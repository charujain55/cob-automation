import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

import java.util.List;
import java.util.Random;

public class WithTestNg {

    public WebDriver driver;
    public WebDriverWait wait;
    public JavascriptExecutor executor;
    public Actions action;
    public Random rand;
    public String text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris id elementum enim, sed egestas " +
            "tortor. Phasellus vitae mauris eget leo ultricies pellentesque hendrerit sit amet augue. Maecenas at hendrerit " +
            "felis, at vulputate enim. Curabitur rhoncus mauris ac ligula elementum finibus. Pellentesque a " +
            "pellentesque metus. In porta quam aliquam, molestie lorem in, suscipit nisi. Morbi congue aliquam orci " +
            "eu sollicitudin.";

    @BeforeClass
    public void setUp()
    {
        //Launch chrome browser
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

        //Go to COB application
        driver.get("https://cob-advisor-nordfk-uat.nord-b5t-dev.nordfk.se/nordfk-cob");

        //Maximize browser window
        driver.manage().window().maximize();

        //Wait till Legitimera button appears
        wait = new WebDriverWait(driver, 30);
        WebElement Legitimera = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("Button-container")));

        //Click on Legitimera button
        Legitimera.click();
    }

    @Test(priority = 0)
    public void bankIdAuth() throws IOException, InterruptedException {
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
        WebElement Category_Body = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("text")));

        //----- PAGE-1 ----- ----- PAGE-1 ----- ----- PAGE-1 -----

        //Provide Advisor SSN
        Category_Body.sendKeys("199008217821");

        //Click on Forts\u00E4tt button
        driver.findElement(By.className("action-alternate")).click();
        Thread.sleep(3000);

        //Click on 'Open BankID security application' button over alert
        Runtime.getRuntime().exec("./src/main/resources/bank_id_alert.exe");
    }

    @Test(priority = 1)
    public void personLogin(){
        WebElement personnummer=  wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("TextField-input")));
        //Provide Person SSN
        personnummer.sendKeys("194506172222");

        WebElement new_session = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
                "//div[text()='Skapa ett nytt r\u00E5dgivningstillf\u00E4lle']")));
        //Start New Session
        new_session.click();

        WebElement get_started = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("" +
                "//div[@class='Button-container']//following-sibling::div[.='P\u00E5b\u00F6rja']")));
        executor = (JavascriptExecutor)driver;
        //Click on Påbörja button
        executor.executeScript("arguments[0].click()", get_started);
    }

    @Test(priority = 2)
    public void page2(){
        WebElement FATCA_no = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Nej']")));
        //Select NO for FATCA
        FATCA_no.click();

        //executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        WebElement move_on = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("" +
                "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']")));
        //Click on Gå vidare button
        executor.executeScript("arguments[0].click()", move_on);
    }

    @Test(priority = 3)
    public void page3(){
        WebElement Discretionary = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("" +
                "//div[text()='Diskretion\u00E4r f\u00F6rvaltning']")));
        executor.executeScript("arguments[0].scrollIntoView();", Discretionary);
        //Click on checkbox 'Diskretion\u00E4r förvaltning'
        Discretionary.click();
        //Click on checkbox 'Tryggande & Pensionssparande'
        driver.findElement(By.xpath("//div[text()='Tryggande & Pensionssparande']")).click();
        //Click on checkbox 'Genomgång av grundskydd'
        driver.findElement(By.xpath("//div[text()='Genomg\u00E5ng av grundskydd']")).click();
        //Click on checkbox 'Sjukvårdsförs\u00E4kring'
        driver.findElement(By.xpath("//div[text()='Sjukv\u00E5rdsf\u00F6rs\u00E4kring']")).click();
        //Click on checkbox 'Rådgivning'
        driver.findElement(By.xpath("//div[text()='R\u00E5dgivning']")).click();
        //Click on checkbox 'Rådgivning till anst\u00E4llda'
        driver.findElement(By.xpath("//div[text()='R\u00E5dgivning till anst\u00E4llda']")).click();
        //Click on checkbox 'Administration'
        driver.findElement(By.xpath("//div[text()='Administration']")).click();
        //Click on checkbox 'Onlinetj\u00E4nst'
        driver.findElement(By.xpath("//div[text()='Onlinetj\u00E4nst']")).click();
        //Click on checkbox 'Placeringspolicy'
        driver.findElement(By.xpath("//div[text()='Placeringspolicy']")).click();
        //Click on checkbox 'Vilande bolag/Succession'
        driver.findElement(By.xpath("//div[text()='Vilande bolag/Succession']")).click();
        //Click on checkbox 'Överskottslikviditet i bolag'
        driver.findElement(By.xpath("//div[text()='\u00D6verskottslikviditet i bolag']")).click();
        //Click on checkbox 'Investeringsrådgivning'
        driver.findElement(By.xpath("//div[text()='Investeringsr\u00E5dgivning']")).click();
        //Scroll window to bottom
        executor.executeScript("window.scrollTo(0,document.body.scrollHeight)");
        //Click on checkbox 'Ers\u00E4ttning rådgivning'
        driver.findElement(By.xpath("//div[text()='Ers\u00E4ttning r\u00E5dgivning']")).click();
        //Click on checkbox 'Annan ers\u00E4ttning'
        driver.findElement(By.xpath("//div[text()='Annan ers\u00E4ttning']")).click();
        //Fill data in textbox 'Var god beskriv ers\u00E4ttningsformen'
        driver.findElement(By.name("uppdragsavtal_annaners_text")).sendKeys("Test Data");
        WebElement move_on = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("" +
                "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']")));
        //Click on move on button
        executor.executeScript("arguments[0].click()", move_on);
    }

    @Test(priority = 4)
    public void page4(){
        WebElement Arrange_life_insurance = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("" +
                "//div[text()='F\u00F6rmedla livf\u00F6rs\u00E4kringar']")));
        executor.executeScript("arguments[0].scrollIntoView();", Arrange_life_insurance);
        //Click on checkbox 'Förmedla livförs\u00E4kringar'
        Arrange_life_insurance.click();
        //Click on checkbox 'Förmedlingsbolaget \u00E4r anknutet ombud till...'
        driver.findElement(By.xpath("//div[contains(text(),'F\u00F6rmedlingsbolaget \u00E4r anknutet ombud till')]")).
                click();
        //Fill data in textbox 'Registreringen avser endast följande förs\u00E4kringsklasser'
        driver.findElement(By.name("forkopsinfo_radgivbolag_forsakringsklasser")).sendKeys("Test Data");
        //Click on radio button '... \u00E4r anst\u00E4lld i ovanstående bolag (anknutet ombud)'
        driver.findElement(By.xpath("//div[text()='... \u00E4r anst\u00E4lld i ovanst\u00E5ende bolag (anknutet ombud)']")).
                click();
        //Click on radio button '... har eget tillstånd hos Finansinspektionen'
        //driver.findElement(By.xpath("//div[text()='... har eget tillst\u00E5nd hos Finansinspektionen']")).click();
        //Click on checkbox 'Förs\u00E4kringsförmedlingslicens saknas och rådgivning om förs\u00E4kringslösningar kan ej l\u00E4mnas'
        //driver.findElement(By.xpath("//div[text()='F\u00F6rs\u00E4kringsf\u00F6rmedlingslicens saknas och r\u00E5dgivning om
        // f\u00F6rs\u00E4kringsl\u00F6sningar kan ej l\u00E4mnas']")).click();
        //Click on checkbox 'Utföra investeringsrådgivning'
        driver.findElement(By.xpath("//div[text()='Utf\u00F6ra investeringsr\u00E5dgivning']")).click();
        //Click on checkbox 'Förmedla livförs\u00E4kringar'
        driver.findElement(By.xpath("(//div[text()='F\u00F6rmedla livf\u00F6rs\u00E4kringar'])[2]")).click();
        //Fill data in textbox 'Registreringen avser endast följande förs\u00E4kringsklasser'
        driver.findElement(By.name("forkopsinfo_forsformedl_forsklasser")).sendKeys("Test Data");
        //Click on checkbox 'Förmedlingsbolaget har kvalificerat innehav i produktbolag'
        driver.findElement(By.xpath("//div[text()='F\u00F6rmedlingsbolaget har kvalificerat innehav i produktbolag']")).
                click();
        //Fill data in textbox 'Ange produktbolag'
        driver.findElement(By.name("forkopsinfo_forsformedl_kvalinn_prodbolag")).sendKeys("Test Data");
        //Click on checkbox 'V\u00E4rdepappersbolag har kvalificerat innehav i förmedlingsbolaget'
        driver.findElement(By.xpath("//div[text()='V\u00E4rdepappersbolag har kvalificerat innehav i " +
                "f\u00F6rmedlingsbolaget']")).click();
        //Fill data in textbox 'Ange v\u00E4rdepappersbolag'
        driver.findElement(By.name("forkopsinfo_vardepapperbolag_kvalinn_vardepapperbol")).sendKeys("Test Data");

        //Click on checkbox 'Produkter &/eller förs\u00E4kringar förmedlas uteslutande för ett eller flera bolags r\u00E4kning med
        // vilka avtal ingåtts.'
        WebElement with_agreement_company = driver.findElement(By.xpath("//div[contains(text(), 'med vilka avtal')]"));
        executor.executeScript("arguments[0].scrollIntoView();", with_agreement_company);
        with_agreement_company.click();
        //Fill data in textbox 'Avtal har slutits med följande bolag'
        driver.findElement(By.name("forkopsinfo_investradgiv_avtal_bolag")).sendKeys("Test Data");
        //Click on checkbox 'Produkter &/eller förs\u00E4kringar förmedlas för ett eller flera bolags r\u00E4kning utan avtal med
        // produktbolagen.'
        driver.findElement(By.xpath("//div[contains(text(), 'utan avtal med produktbolagen')]")).click();
        //Fill data in textbox 'Förs\u00E4kringar förmedlas huvudsakligen utan avtal till följande bolag'
        driver.findElement(By.name("forkopsinfo_investradgiv_avtal_forbolag")).sendKeys("Test Data");
        //Click on checkbox 'Bolaget arbetar med oberoende investeringsrådgivning.'
        driver.findElement(By.xpath("//div[contains(text(), 'Bolaget arbetar med oberoende')]")).click();
        WebElement move_on = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("" +
                "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']")));
        //Click on move on button
        executor.executeScript("arguments[0].click()", move_on);
    }

    @Test(priority = 5)
    public void page5(){
        WebElement Saving = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Sparande']")));
        //Click on checkbox 'Sparande'
        Saving.click();
        //Click on checkbox 'Kapitalförvaltning'
        driver.findElement(By.xpath("//div[text()='Kapitalf\u00F6rvaltning']")).click();
        //Click on checkbox 'Annat (beskriv)'
        driver.findElement(By.xpath("//div[text()='Annat (beskriv)']")).click();
        //Fill data in textbox 'V\u00E4nligen beskriv'
        driver.findElement(By.xpath("(//input[@class='TextField-input'])[1]")).sendKeys("Test data");

        //Click on checkbox 'Pension'
        driver.findElement(By.xpath("//div[text()='Pension']")).click();
        //Click on checkbox 'Privat konsumtion'
        driver.findElement(By.xpath("//div[text()='Privat konsumtion']")).click();
        //Click on checkbox 'Ekonomisk trygghet'
        driver.findElement(By.xpath("//div[text()='Ekonomisk trygghet']")).click();
        //Click on checkbox 'Till n\u00E4rstående'
        driver.findElement(By.xpath("//div[text()='Till n\u00E4rst\u00E5ende']")).click();
        //Click on checkbox 'Jag \u00E4r förmånstagare'
        driver.findElement(By.xpath("//div[text()='Jag \u00E4r f\u00F6rm\u00E5nstagare']")).click();
        //Click on checkbox 'Sparande/placering'
        driver.findElement(By.xpath("//div[text()='Sparande/placering']")).click();
        //Click on checkbox 'Annat (beskriv)'
        driver.findElement(By.xpath("(//div[text()='Annat (beskriv)'])[2]")).click();
        //Fill data in textbox 'V\u00E4nligen beskriv'
        driver.findElement(By.xpath("(//input[@class='TextField-input'])[2]")).sendKeys("Test data");

        //Click on checkbox 'För egen del (eller av mig hel\u00E4gt företag)'
        driver.findElement(By.xpath("//div[text()='F\u00F6r egen del (eller av mig hel\u00E4gt f\u00F6retag)']")).click();
        //Click on checkbox 'För någon annan genom fullmakt'
        driver.findElement(By.xpath("//div[text()='F\u00F6r n\u00E5gon annan genom fullmakt']")).click();
        //Click on checkbox 'Annat (beskriv)'
        driver.findElement(By.xpath("(//div[text()='Annat (beskriv)'])[3]")).click();
        //Fill data in textbox 'V\u00E4nligen beskriv'
        driver.findElement(By.xpath("(//input[@class='TextField-input'])[3]")).sendKeys("Test data");

        //Select yes for '\u00E4r syftet med att investera i finansiella instrument annat \u00E4n att nå avkastning?'
        driver.findElement(By.xpath("//div[text()='Ja']")).click();

        WebElement describe_textbox = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("" +
                "(//input[@class='TextField-input'])[4]")));
        //Fill data in textbox 'V\u00E4nligen beskriv'
        describe_textbox.sendKeys("Test data");
        //Fill data in textbox 'Ange belopp'
        driver.findElement(By.xpath("(//input[@class='TextField-input'])[5]")).sendKeys("567000");

        //Click on checkbox 'Sparande'
        driver.findElement(By.xpath("(//div[text()='Sparande'])[2]")).click();
        //Click on checkbox 'Arv/gåva'
        driver.findElement(By.xpath("//div[text()='Arv/g\u00E5va']")).click();
        //Click on checkbox 'Lön/pension'
        driver.findElement(By.xpath("//div[text()='L\u00F6n/pension']")).click();
        //Click on checkbox 'Fastighetsförs\u00E4ljning'
        driver.findElement(By.xpath("//div[text()='Fastighetsf\u00F6rs\u00E4ljning']")).click();
        //Click on checkbox 'Kapitalavkastning'
        driver.findElement(By.xpath("//div[text()='Kapitalavkastning']")).click();
        //Click on checkbox 'Annat (beskriv)'
        driver.findElement(By.xpath("(//div[text()='Annat (beskriv)'])[4]")).click();
        //Fill data in textbox 'V\u00E4nligen beskriv'
        driver.findElement(By.xpath("(//input[@class='TextField-input'])[6]")).sendKeys("Test data");

        //Click on checkbox 'Svensk bank'
        driver.findElement(By.xpath("//div[text()='Svensk bank']")).click();
        //Click on checkbox 'Svenskt förs\u00E4kringsbolag'
        driver.findElement(By.xpath("//div[text()='Svenskt f\u00F6rs\u00E4kringsbolag']")).click();
        //Click on checkbox 'Utl\u00E4ndsk bank'
        driver.findElement(By.xpath("//div[text()='Utl\u00E4ndsk bank']")).click();
        //Fill data in textbox 'Ange utl\u00E4ndsk bank'
        driver.findElement(By.xpath("(//input[@class='TextField-input'])[7]")).sendKeys("Test data");

        WebElement salary_dropdown = driver.findElement(By.className("Select-info"));
        executor.executeScript("arguments[0].scrollIntoView()", salary_dropdown);
        salary_dropdown.click();
        List<WebElement> salary_ele = driver.findElements(By.className("Select-dropdown-option"));
        rand = new Random();
        int salary = rand.nextInt(salary_ele.size());
        salary_ele.get(salary).click();
        driver.findElement(By.xpath("(//div[text()='Ja'])[2]")).click();
        //driver.findElement(By.xpath("//div[text()='Koppling']")).sendKeys("Test data");
        WebElement textbox3 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("" +
                "//label[text()='Koppling']/following-sibling::input")));
        textbox3.sendKeys("Test data");
        driver.findElement(By.xpath("//label[text()='Namn']/following-sibling::input")).sendKeys(
                "Test data 1");
        driver.findElement(By.xpath("//label[text()='Befattning']/following-sibling::input")).sendKeys(
                "Test data 2");
        driver.findElement(By.className("Select-info")).click();
        driver.findElement(By.xpath("//input[@placeholder='S\u00F6k']")).sendKeys("argentina");
        WebElement country = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Argentina']")));
        country.click();
        driver.findElement(By.xpath("//label[text()='Period']/following-sibling::input")).sendKeys(
                "Test data 3");
        WebElement move_on = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("" +
                "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']")));
        executor.executeScript("arguments[0].click();", move_on);
    }

    @Test(priority = 6)
    public void page6(){
        WebElement page6 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
                "//label[text()='F\u00F6rs\u00F6rjningsplikt antal barn']/following-sibling::input")));
        int no = rand.nextInt(4);
        page6.sendKeys(String.valueOf(no));
        driver.findElement(By.className("Select-info")).click();
        List<WebElement> status = driver.findElements(By.xpath("(//div[@class='Select-dropdown'])[1]/div[@class=" +
                "'Select-dropdown-option']"));
        int randnum  = rand.nextInt(status.size());
        status.get(randnum).click();
        driver.findElement(By.xpath("(//div[@class='Select-info'])[2]")).click();
        List<WebElement> status1 = driver.findElements(By.xpath("(//div[@class='Select-dropdown'])[2]/div[@class=" +
                "'Select-dropdown-option']"));
        int randnum1  = rand.nextInt(status1.size());
        status1.get(randnum1).click();
        driver.findElement(By.xpath("//label[text()='M\u00E5nadsinkomst efter skatt']/following-sibling::input")).
                sendKeys("20000");
        driver.findElement(By.xpath("//label[text()='M\u00E5nadskostnad']/following-sibling::input")).
                sendKeys("30000");
        driver.findElement(By.xpath("//label[text()='Fastigheter']/following-sibling::input")).
                sendKeys("40000");
        driver.findElement(By.xpath("//label[text()='Skulder']/following-sibling::input")).
                sendKeys("50000");

        List<WebElement> investment = driver.findElements(By.xpath("//td//input[@type='text']"));
        for (WebElement i:investment) {
            int random_investment = rand.nextInt(999999);
            i.sendKeys(String.valueOf(random_investment));
        }
        WebElement move_on = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("" +
                "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']")));
        executor.executeScript("arguments[0].click();", move_on);
    }

    @Test(priority = 7)
    public void page7(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Nej']")));
        List<WebElement> radio_button1 = driver.findElements(By.xpath("//div[@class='Cob-OptionsList-checkboxes']" +
                "//div[@class='Checkbox-label']"));
        int radio_button = rand.nextInt(radio_button1.size());
        radio_button1.get(radio_button).click();

        int years = rand.nextInt(99);
        driver.findElement(By.className("Cob-SimpleTextField-input")).sendKeys(String.valueOf(years));

        driver.findElement(By.xpath("(//div[@class='Checkbox-checkbox'])[3]")).click();
        executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");

        List<WebElement> text_fields = driver.findElements(By.xpath("//td//input[@type='number']"));
        int j =1;
        for (WebElement i:text_fields) {
            int shares = rand.nextInt(99);
            if (shares > 20) {
                i.sendKeys(String.valueOf(shares));
                driver.findElement(By.xpath("(//div[@class='Select-info'])["+j+"]")).click();
                WebElement Omfattande = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
                        "(//div[@class='Select-dropdown']//div[text()='Omfattande'])["+j+"]")));
                Omfattande.click();
                driver.findElement(By.xpath("(//td[4]//div[@class='Checkbox-checkbox'])["+j+"]")).click();
            }
            else
                i.sendKeys(String.valueOf(shares));
            j++;
        }
        WebElement move_on = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("" +
                "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']")));
        executor.executeScript("arguments[0].click();", move_on);
    }

    @Test(priority = 8)
    public void page8(){
        WebElement risk_option = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
                "//h3[text()='V\u00E4lj din \u00F6nskade riskniv\u00E5']")));
        executor.executeScript("arguments[0].scrollIntoView();", risk_option);
        List<WebElement> risk = driver.findElements(By.xpath("//div[@class='Checkbox-checkbox']"));
        int rand_risk = rand.nextInt(risk.size());
        risk.get(rand_risk).click();
        WebElement move_on = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("" +
                "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']")));
        executor.executeScript("arguments[0].click();", move_on);
    }

    @Test(priority = 9)
    public void page9() throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Sparprofil']")));
        //List all dropdowns
        List<WebElement> reason = driver.findElements(By.xpath("//div[@class='Select-info']"));
        int ja = 1;
        int jb = 1;
        for (WebElement i :reason) {
            executor.executeScript("arguments[0].scrollIntoView();", i);
            //Click on each dropdown
            i.click();
            //List all options in dropdown
            List<WebElement> options = driver.findElements(By.xpath("(//div[@class='Select-dropdown'])" +
                    "["+ja+"]/div[@class='Select-dropdown-option']"));
            int rand_reason = rand.nextInt(options.size());
            ja++;
            //If 'Other' option is selected from dropdown
            if (rand_reason == 6){
                options.get(rand_reason).click();
                //Fill data in textbox
                WebElement other = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
                        "(//label[text()='V\u00E4nligen beskriv'])["+jb+"]/following-sibling::input")));
                other.sendKeys("Test data");
                jb++;
            }
            else
                //Select & click any random option from dropdown
                options.get(rand_reason).click();
        }

        WebElement riskprofil = driver.findElement(By.xpath("(//font/span)[1]"));
       // WebElement total_risk = driver.findElement(By.xpath("(//font/span)[2]"));
        int riskprofil1 = Integer.parseInt(riskprofil.getText());
      //  int total_risk1 = Integer.parseInt(total_risk.getText());

        //Select risk for each horizon
        for (int i =1; i <= 7; i+=2) {
            List<WebElement> all_risk = driver.findElements(By.xpath(
                    "(//div[@class='Cob-OptionsList'])["+i+"]//div[@class='Checkbox-checkbox']"));
            int size = all_risk.size();
            //If risk selection is greater than actual risk
            if (size > riskprofil1)
                //Then select risk with in range of actual risk only
                size = riskprofil1;
            int rand_risk1 = rand.nextInt(size);
            all_risk.get(rand_risk1).click();
        }

        int je =1;
        for (int i =2; i <= 8; i+=2) {
            List<WebElement> all_risk = driver.findElements(By.xpath(
                    "(//div[@class='Cob-OptionsList'])[" + i + "]//div[@class='Checkbox-checkbox']"));
            int rand_risk2 = rand.nextInt(all_risk.size());
            if (rand_risk2 == 0) {
                all_risk.get(rand_risk2).click();
                Thread.sleep(300);
                int month = rand.nextInt(24);
                driver.findElement(By.xpath("(//label[text()='Ange antal m\u00E5nader'])["+je+"]/following-sibling::input")).
                        sendKeys(String.valueOf(month));
                je++;
            }
            else
                all_risk.get(rand_risk2).click();
        }

        //executor.executeScript("window.scrollTo(document.body.scrollHeight, 0)");
        String summa_placeringsbara_tillgångar = driver.findElement(By.xpath("//h4[contains(text(),'Summa " +
                "placeringsbara tillg\u00E5ngar: ')]")).getText();
        String totalt_placerat_kapital = driver.findElement(By.xpath("//h4[contains(text(), 'Totalt placerat " +
                "kapital')]")).getText();
        System.out.println(summa_placeringsbara_tillgångar + totalt_placerat_kapital);
        String[] summa_placeringsbara_tillgångar1 = summa_placeringsbara_tillgångar.split(":");
        summa_placeringsbara_tillgångar = summa_placeringsbara_tillgångar1[1].trim();
        String[] totalt_placerat_kapital1 = totalt_placerat_kapital.split(":");
        totalt_placerat_kapital = totalt_placerat_kapital1[1].trim();
        System.out.println(summa_placeringsbara_tillgångar + totalt_placerat_kapital);
        //If slider is not fully selected & total sum percentage is not 100%
        if (!summa_placeringsbara_tillgångar.equals(totalt_placerat_kapital)) {
            System.out.println("true");
            WebElement slider = driver.findElement(By.xpath("(//div[@role='slider'])[4]"));
            //Move slider to 100%
            action.moveToElement(slider).clickAndHold().moveByOffset(50, 0).release().build().perform();
        }
        System.out.println("false");
        WebElement move_on = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("" +
                "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']")));
        // Click on move on button
        executor.executeScript("arguments[0].click();", move_on);
    }

    @Test(priority = 10)
    public void page10() throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='R\u00E5det']")));
        Thread.sleep(1000);
        driver.findElement(By.xpath("//textarea[@name='advice_notes']")).sendKeys(text);
        driver.findElement(By.name("advice_notes2")).sendKeys(text);
        driver.findElement(By.name("advice_notes3")).sendKeys(text);
        driver.findElement(By.name("advice_notes4")).sendKeys(text);
        driver.findElement(By.name("advice_notes5")).sendKeys(text);
        WebElement move_on = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("" +
                "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']")));
        executor.executeScript("arguments[0].click();", move_on);
    }

    @Test(priority = 11)
    public void page11(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Kontoval']")));
        driver.findElement(By.xpath("//div[text()='Ny dep\u00E5 hos Nord']")).click();
        driver.findElement(By.xpath("//div[text()='ISK']")).click();
        driver.findElement(By.xpath("//div[text()='Brutto']")).click();
        driver.findElement(By.xpath("(//div[@class='Checkbox-checkbox'])[10]")).click();
        driver.findElement(By.xpath("(//div[@class='Checkbox-checkbox'])[11]")).click();
        driver.findElement(By.xpath("//div[text()='Insynsfullmakt']")).click();
        driver.findElement(By.xpath("(//div[@class='Checkbox-checkbox'])[14]")).click();
        driver.findElement(By.className("Select-info")).click();
        driver.findElement(By.xpath("//input[@placeholder='S\u00F6k']")).sendKeys("Citibank");
        driver.findElement(By.xpath("//div[@class='Select-dropdown-option highlight']")).click();
        driver.findElement(By.xpath("//label[text()='Clearingnummer']/following-sibling::input")).sendKeys("9040");
        driver.findElement(By.xpath("//label[text()='Kontonummer']/following-sibling::input")).sendKeys("1234567");
        driver.findElement(By.xpath("(//div[@class='Checkbox-checkbox'])[16]")).click();
        driver.findElement(By.xpath("(//div[@class='Checkbox-checkbox'])[18]")).click();
        WebElement move_on = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("" +
                "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']")));
        executor.executeScript("arguments[0].click();", move_on);
    }

    @Test(priority = 12)
    public void page12(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Placeringsf\u00F6rslag']")));
        WebElement right_slider = driver.findElement(By.xpath("//div[@role='slider'][2]"));
        executor.executeScript("arguments[0].scrollIntoView()", right_slider);
        WebElement target_drop = driver.findElement(By.xpath("//span[@class='Step-mark-class'][7]"));

        action = new Actions(driver);
        //Move risk slider and select range 1 to 7
        action.dragAndDrop(right_slider, target_drop).build().perform();
        //Click product type dropdown
        driver.findElement(By.xpath("(//div[@class='Select-info'])[1]")).click();
        String product_type = "ETF:er";

        switch (product_type){
            case "Strukturerade produkter":
                //Select 'Structure Product' from dropdown
                driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+product_type+"']")).click();
                //Un-check checkbox for selecting secondary market
                driver.findElement(By.xpath("//div[text()='Aktuella instrument f\u00F6r teckning']")).click();
                //Click on product name dropdown
                driver.findElement(By.xpath("(//div[@class='Select-info'])[2]")).click();
                String product_name = "1053 Buffertbevis Sverige | SE0005991213";
                //Select any product from dropdown
                driver.findElement(By.xpath("//div[@title='"+product_name+"']")).click();
                //Provide Quantity
                driver.findElement(By.xpath("//label[text()='Antal']/following-sibling::input")).sendKeys("110");
                WebElement courtage = driver.findElement(By.xpath("(//div[@role='slider'])[1]"));
                executor.executeScript("arguments[0].scrollIntoView()", courtage);
                //Move courtage slider
                action.moveToElement(courtage).clickAndHold().moveByOffset(50, 0).release().build().perform();
                //Click on button to add product to the listing
                driver.findElement(By.xpath("//div[@class='field']/button")).click();
            case "Obligationer":
                //Select 'Bonds' from dropdown
                driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+product_type+"']")).click();
                //Un-check checkbox for selecting secondary market
                driver.findElement(By.xpath("//div[text()='Aktuella instrument f\u00F6r teckning']")).click();
                //Click on product name dropdown
                driver.findElement(By.xpath("(//div[@class='Select-info'])[2]")).click();
                String product_name1 = "Brødrene Greger RB 14/19 | NO0010715246";
                //Select any product from dropdown
                driver.findElement(By.xpath("//div[@title='"+product_name1+"']")).click();
                //Provide Quantity
                driver.findElement(By.xpath("//label[text()='Antal']/following-sibling::input")).sendKeys("110");
                WebElement courtage1 = driver.findElement(By.xpath("(//div[@role='slider'])[1]"));
                executor.executeScript("arguments[0].scrollIntoView()", courtage1);
                //Move courtage slider
                action.moveToElement(courtage1).clickAndHold().moveByOffset(50, 0).release().build().perform();
                //Click on button to add product to the listing
                driver.findElement(By.xpath("//div[@class='field']/button")).click();
            case "Aktier":
                //Select 'Shares' from dropdown
                driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+product_type+"']")).click();
                //Click on product name dropdown
                driver.findElement(By.xpath("(//div[@class='Select-info'])[2]")).click();
                String product_name2 = "Akelius Residential Pref | SE0005936713";
                //Select any product from dropdown
                driver.findElement(By.xpath("//div[@title='"+product_name2+"']")).click();
                //Provide Quantity
                driver.findElement(By.xpath("//label[text()='Antal']/following-sibling::input")).sendKeys("110");
                WebElement courtage2 = driver.findElement(By.xpath("(//div[@role='slider'])[1]"));
                executor.executeScript("arguments[0].scrollIntoView()", courtage2);
                //Move courtage slider
                action.moveToElement(courtage2).clickAndHold().moveByOffset(50, 0).release().build().perform();
                //Click on button to add product to the listing
                driver.findElement(By.xpath("//div[@class='field']/button")).click();
            case "Fonder":
                //Select 'Funds' from dropdown
                driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+product_type+"']")).click();
                //Click on product name dropdown
                driver.findElement(By.xpath("(//div[@class='Select-info'])[2]")).click();
                String product_name3 = "Aktie-Ansvar Avkastningsfond | SE0000735771";
                //Select any product from dropdown
                driver.findElement(By.xpath("//div[@title='"+product_name3+"']")).click();
                //Provide Amount to invest
                driver.findElement(By.xpath("//label[text()='Belopp att investera']/following-sibling::input")).sendKeys("110");
                WebElement courtage3 = driver.findElement(By.xpath("(//div[@role='slider'])[1]"));
                executor.executeScript("arguments[0].scrollIntoView()", courtage3);
                //Move courtage slider
                action.moveToElement(courtage3).clickAndHold().moveByOffset(50, 0).release().build().perform();
                //Click on button to add product to the listing
                driver.findElement(By.xpath("//div[@class='field']/button")).click();
            case "ETF:er":
                //Select 'ETF' from dropdown
                driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+product_type+"']")).click();
                //Click on product name dropdown
                driver.findElement(By.xpath("(//div[@class='Select-info'])[2]")).click();
                String product_name4 = "AC Amerikanska Bolag M\u00E5nadsvis 1505GF | SE0007048632";
                //Select any product from dropdown
                driver.findElement(By.xpath("//div[@title='"+product_name4+"']")).click();
                //Provide Quantity
                driver.findElement(By.xpath("//label[text()='Antal']/following-sibling::input")).sendKeys("110");
                WebElement courtage4 = driver.findElement(By.xpath("(//div[@role='slider'])[1]"));
                executor.executeScript("arguments[0].scrollIntoView()", courtage4);
                //Move courtage slider
                action.moveToElement(courtage4).clickAndHold().moveByOffset(50, 0).release().build().perform();
                //Click on button to add product to the listing
                driver.findElement(By.xpath("//div[@class='field']/button")).click();
        }
        WebElement move_on = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("" +
                "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']")));
        //Click on move on button
        executor.executeScript("arguments[0].click()", move_on);
    }

    @Test(priority = 13)
    public void page13(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='R\u00E5dgivarens bed\u00F6mning']")));
        //Select yes
        driver.findElement(By.xpath("(//div[@class='Checkbox-checkbox'])[1]")).click();
        //Select No
        driver.findElement(By.xpath("(//div[@class='Checkbox-checkbox'])[4]")).click();
        //Select Yes
        driver.findElement(By.xpath("(//div[@class='Checkbox-checkbox'])[5]")).click();
        //Fill data in textbox
        driver.findElement(By.xpath("(//textarea)[1]")).sendKeys(text);
        //Fill data in textbox
        driver.findElement(By.xpath("(//textarea)[2]")).sendKeys(text);
        WebElement move_on = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("" +
                "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']")));
        //Click on move on button
        executor.executeScript("arguments[0].click()", move_on);
    }

    @Test(priority = 14)
    public void page14(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Placeringsf\u00F6rslag']")));
        executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        WebElement fee1 = driver.findElement(By.xpath("//label[text()='Administrationsavgift']/following-sibling::input"));
        //Clear 'Administrationsavgift' field
        fee1.sendKeys(Keys.BACK_SPACE);
        //Fill fee data
        fee1.sendKeys("100");
        WebElement fee2 = driver.findElement(By.xpath("//label[text()='Flyttavgift']/following-sibling::input"));
        //Clear 'Flyttavgift' field
        fee2.sendKeys(Keys.BACK_SPACE);
        //Fill fee data
        fee2.sendKeys("110");
        WebElement fee3 = driver.findElement(By.xpath("//label[text()='\u00D6vriga avgifter']/following-sibling::input"));
        //Clear 'Övriga avgifter' field
        fee3.sendKeys(Keys.BACK_SPACE);
        //Fill fee data
        fee3.sendKeys("120");
        WebElement move_on = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("" +
                "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']")));
        //Click on move on button
        executor.executeScript("arguments[0].click()", move_on);
    }

    @Test(priority = 15)
    public void page15(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Finansiella instrument']")));
        //Click on move on button
        WebElement move_on = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("" +
                "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']")));
        executor.executeScript("arguments[0].click()", move_on);
    }

    @Test(priority = 16)
    public void page16() throws InterruptedException, IOException {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='\u00D6vriga dokument']")));
        //Click on drag & drop area
        driver.findElement(By.className("FileDrop-dropbox")).click();
        Thread.sleep(1000);
        //Upload pdf file
        Runtime.getRuntime().exec("C:\\Users\\jyothi\\Downloads\\AutoIT\\fileUpload.exe");
        Thread.sleep(1000);
        WebElement move_on = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("" +
                "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']")));
        //Click on move on button
        executor.executeScript("arguments[0].click()", move_on);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Signering och kundens " +
                "underskrift']")));
        List<WebElement> sign_checkboxes = driver.findElements(By.xpath("//div[@class='Cob-SignAgreement-fields']/div"));

        for (WebElement checkbox : sign_checkboxes) {
            String class_name = checkbox.getAttribute("class");
            //if agreement checkboxes are not checked then mark them checked
            if (!class_name.contains("checked"))
                checkbox.click();
            else
                continue;
        }
        WebElement signera_avtal = driver.findElement(By.xpath("//div[text()='Signera avtal']"));
        //Click on sign agreement button
        executor.executeScript("arguments[0].click();", signera_avtal);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Signera avtal']")));
        //Click in 'Skicka till signering' button
        driver.findElement(By.xpath("//button[@class='Button small filled success']")).click();
    }

    @AfterClass
    public void tearDown() throws InterruptedException {
        driver.close();
    }
}
