import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

import java.util.*;

public class BrowserTest {

	public static void main(String[] args) throws InterruptedException, IOException {
		//Launch chrome browser
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();

		//Go to COB application
		driver.get("https://cob-advisor-nordfk-uat.nord-b5t-dev.nordfk.se/nordfk-cob");

		//Maximize browser window
		driver.manage().window().maximize();

		//Wait till Legitimera button appears
		WebDriverWait wait = new WebDriverWait(driver, 30);
		WebElement Legitimera = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("Button-container")));

		//Click on Legitimera button
		Legitimera.click();

	    wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		WebElement Category_Body = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("text")));

		//----- PAGE-1 ----- ----- PAGE-1 ----- ----- PAGE-1 -----

		//Provide Advisor SSN
        Category_Body.sendKeys("199008217821");

        //Click on Forts�tt button
        driver.findElement(By.className("action-alternate")).click();
		Thread.sleep(3000);

		//Click on 'Open BankID security application' button over alert
		Runtime.getRuntime().exec("./src/main/resources/bank_id_alert.exe");

		WebElement personnummer=  wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("TextField-input")));
		//Provide Person SSN
		personnummer.sendKeys("194506172222");

		WebElement new_session = wait.until(ExpectedConditions.visibilityOfElementLocated
				  (By.xpath("//div[text()='Skapa ett nytt r�dgivningstillf�lle']")));
		//Start New Session
		new_session.click();

		WebElement get_started = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("" +
				"//div[@class='Button-container']//following-sibling::div[.='P�b�rja']")));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		//Click on P�b�rja button
		executor.executeScript("arguments[0].click()", get_started);

		//----- PAGE-2 ----- ----- PAGE-2 ----- ----- PAGE-2 -----

		WebElement FATCA_no = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Nej']")));
		//Select NO for FATCA
		FATCA_no.click();

		//executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		WebElement move_on = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("" +
				"//div[@class='Button-container']//following-sibling::div[.='Gå vidare']")));
		//Click on G� vidare button
		executor.executeScript("arguments[0].click()", move_on);

		//----- PAGE-3 ----- ----- PAGE-3 ----- ----- PAGE-3 -----

		WebElement Discretionary = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("" +
				"//div[text()='Diskretion�r f�rvaltning']")));
		executor.executeScript("arguments[0].scrollIntoView();", Discretionary);
		//Click on checkbox 'Diskretion�r f�rvaltning'
		Discretionary.click();
		//Click on checkbox 'Tryggande & Pensionssparande'
		driver.findElement(By.xpath("//div[text()='Tryggande & Pensionssparande']")).click();
		//Click on checkbox 'Genomg�ng av grundskydd'
		driver.findElement(By.xpath("//div[text()='Genomg�ng av grundskydd']")).click();
		//Click on checkbox 'Sjukv�rdsf�rs�kring'
		driver.findElement(By.xpath("//div[text()='Sjukv�rdsf�rs�kring']")).click();
		//Click on checkbox 'R�dgivning'
		driver.findElement(By.xpath("//div[text()='R�dgivning']")).click();
		//Click on checkbox 'R�dgivning till anst�llda'
		driver.findElement(By.xpath("//div[text()='R�dgivning till anst�llda']")).click();
		//Click on checkbox 'Administration'
		driver.findElement(By.xpath("//div[text()='Administration']")).click();
		//Click on checkbox 'Onlinetj�nst'
		driver.findElement(By.xpath("//div[text()='Onlinetj�nst']")).click();
		//Click on checkbox 'Placeringspolicy'
		driver.findElement(By.xpath("//div[text()='Placeringspolicy']")).click();
		//Click on checkbox 'Vilande bolag/Succession'
		driver.findElement(By.xpath("//div[text()='Vilande bolag/Succession']")).click();
		//Click on checkbox '�verskottslikviditet i bolag'
		driver.findElement(By.xpath("//div[text()='�verskottslikviditet i bolag']")).click();
		//Click on checkbox 'Investeringsr�dgivning'
		driver.findElement(By.xpath("//div[text()='Investeringsr�dgivning']")).click();
	    //Scroll window to bottom
		executor.executeScript("window.scrollTo(0,document.body.scrollHeight)");
	    //Click on checkbox 'Ers�ttning r�dgivning'
		driver.findElement(By.xpath("//div[text()='Ers�ttning r�dgivning']")).click();
	    //Click on checkbox 'Annan ers�ttning'
		driver.findElement(By.xpath("//div[text()='Annan ers�ttning']")).click();
		//Fill data in textbox 'Var god beskriv ers�ttningsformen'
		driver.findElement(By.name("uppdragsavtal_annaners_text")).sendKeys("Test Data");
		//Click on move on button
		executor.executeScript("arguments[0].click()", move_on);

		//----- PAGE-4 ----- ----- PAGE-4 ----- ----- PAGE-4 -----

		WebElement Arrange_life_insurance = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("" +
				"//div[text()='F�rmedla livf�rs�kringar']")));
		executor.executeScript("arguments[0].scrollIntoView();", Arrange_life_insurance);
		//Click on checkbox 'F�rmedla livf�rs�kringar'
		Arrange_life_insurance.click();
		//Click on checkbox 'F�rmedlingsbolaget �r anknutet ombud till...'
		driver.findElement(By.xpath("//div[contains(text(),'F�rmedlingsbolaget �r anknutet ombud till')]")).click();
		//Fill data in textbox 'Registreringen avser endast f�ljande f�rs�kringsklasser'
		driver.findElement(By.name("forkopsinfo_radgivbolag_forsakringsklasser")).sendKeys("Test Data");
		//Click on radio button '... �r anst�lld i ovanst�ende bolag (anknutet ombud)'
		driver.findElement(By.xpath("//div[text()='... �r anst�lld i ovanst�ende bolag (anknutet ombud)']")).click();
		//Click on radio button '... har eget tillst�nd hos Finansinspektionen'
		//driver.findElement(By.xpath("//div[text()='... har eget tillst�nd hos Finansinspektionen']")).click();
		//Click on checkbox 'F�rs�kringsf�rmedlingslicens saknas och r�dgivning om f�rs�kringsl�sningar kan ej l�mnas'
		//driver.findElement(By.xpath("//div[text()='F�rs�kringsf�rmedlingslicens saknas och r�dgivning om
		// f�rs�kringsl�sningar kan ej l�mnas']")).click();
		//Click on checkbox 'Utf�ra investeringsr�dgivning'
		driver.findElement(By.xpath("//div[text()='Utf�ra investeringsr�dgivning']")).click();
		//Click on checkbox 'F�rmedla livf�rs�kringar'
		driver.findElement(By.xpath("(//div[text()='F�rmedla livf�rs�kringar'])[2]")).click();
		//Fill data in textbox 'Registreringen avser endast f�ljande f�rs�kringsklasser'
		driver.findElement(By.name("forkopsinfo_forsformedl_forsklasser")).sendKeys("Test Data");
		//Click on checkbox 'F�rmedlingsbolaget har kvalificerat innehav i produktbolag'
		driver.findElement(By.xpath("//div[text()='F�rmedlingsbolaget har kvalificerat innehav i produktbolag']")).click();
		//Fill data in textbox 'Ange produktbolag'
		driver.findElement(By.name("forkopsinfo_forsformedl_kvalinn_prodbolag")).sendKeys("Test Data");
		//Click on checkbox 'V�rdepappersbolag har kvalificerat innehav i f�rmedlingsbolaget'
		driver.findElement(By.xpath("//div[text()='V�rdepappersbolag har kvalificerat innehav i f�rmedlingsbolaget']")).click();
		//Fill data in textbox 'Ange v�rdepappersbolag'
		driver.findElement(By.name("forkopsinfo_vardepapperbolag_kvalinn_vardepapperbol")).sendKeys("Test Data");

		//Click on checkbox 'Produkter &/eller f�rs�kringar f�rmedlas uteslutande f�r ett eller flera bolags r�kning med
		// vilka avtal ing�tts.'
		WebElement with_agreement_company = driver.findElement(By.xpath("//div[contains(text(), 'med vilka avtal')]"));
		executor.executeScript("arguments[0].scrollIntoView();", with_agreement_company);
		with_agreement_company.click();
		//Fill data in textbox 'Avtal har slutits med f�ljande bolag'
		driver.findElement(By.name("forkopsinfo_investradgiv_avtal_bolag")).sendKeys("Test Data");
		//Click on checkbox 'Produkter &/eller f�rs�kringar f�rmedlas f�r ett eller flera bolags r�kning utan avtal med
		// produktbolagen.'
		driver.findElement(By.xpath("//div[contains(text(), 'utan avtal med produktbolagen')]")).click();
		//Fill data in textbox 'F�rs�kringar f�rmedlas huvudsakligen utan avtal till f�ljande bolag'
		driver.findElement(By.name("forkopsinfo_investradgiv_avtal_forbolag")).sendKeys("Test Data");
		//Click on checkbox 'Bolaget arbetar med oberoende investeringsr�dgivning.'
		driver.findElement(By.xpath("//div[contains(text(), 'Bolaget arbetar med oberoende')]")).click();
		//Click on move on button
		executor.executeScript("arguments[0].click()", move_on);

		//----- PAGE-5 ----- ----- PAGE-5 ----- ----- PAGE-5 -----

		WebElement Saving = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Sparande']")));
		//Click on checkbox 'Sparande'
		Saving.click();
		//Click on checkbox 'Kapitalf�rvaltning'
		driver.findElement(By.xpath("//div[text()='Kapitalf�rvaltning']")).click();
		//Click on checkbox 'Annat (beskriv)'
		driver.findElement(By.xpath("//div[text()='Annat (beskriv)']")).click();
		//Fill data in textbox 'V�nligen beskriv'
		driver.findElement(By.xpath("(//input[@class='TextField-input'])[1]")).sendKeys("Test data");

		//Click on checkbox 'Pension'
		driver.findElement(By.xpath("//div[text()='Pension']")).click();
		//Click on checkbox 'Privat konsumtion'
		driver.findElement(By.xpath("//div[text()='Privat konsumtion']")).click();
		//Click on checkbox 'Ekonomisk trygghet'
		driver.findElement(By.xpath("//div[text()='Ekonomisk trygghet']")).click();
		//Click on checkbox 'Till n�rst�ende'
		driver.findElement(By.xpath("//div[text()='Till n�rst�ende']")).click();
		//Click on checkbox 'Jag �r f�rm�nstagare'
		driver.findElement(By.xpath("//div[text()='Jag �r f�rm�nstagare']")).click();
		//Click on checkbox 'Sparande/placering'
		driver.findElement(By.xpath("//div[text()='Sparande/placering']")).click();
		//Click on checkbox 'Annat (beskriv)'
		driver.findElement(By.xpath("(//div[text()='Annat (beskriv)'])[2]")).click();
		//Fill data in textbox 'V�nligen beskriv'
		driver.findElement(By.xpath("(//input[@class='TextField-input'])[2]")).sendKeys("Test data");

		//Click on checkbox 'F�r egen del (eller av mig hel�gt f�retag)'
		driver.findElement(By.xpath("//div[text()='F�r egen del (eller av mig hel�gt f�retag)']")).click();
		//Click on checkbox 'F�r n�gon annan genom fullmakt'
		driver.findElement(By.xpath("//div[text()='F�r n�gon annan genom fullmakt']")).click();
		//Click on checkbox 'Annat (beskriv)'
		driver.findElement(By.xpath("(//div[text()='Annat (beskriv)'])[3]")).click();
		//Fill data in textbox 'V�nligen beskriv'
		driver.findElement(By.xpath("(//input[@class='TextField-input'])[3]")).sendKeys("Test data");

		//Select yes for '�r syftet med att investera i finansiella instrument annat �n att n� avkastning?'
		driver.findElement(By.xpath("//div[text()='Ja']")).click();

		WebElement describe_textbox = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("" +
				"(//input[@class='TextField-input'])[4]")));
		//Fill data in textbox 'V�nligen beskriv'
		describe_textbox.sendKeys("Test data");
		//Fill data in textbox 'Ange belopp'
		driver.findElement(By.xpath("(//input[@class='TextField-input'])[5]")).sendKeys("567000");

		//Click on checkbox 'Sparande'
		driver.findElement(By.xpath("(//div[text()='Sparande'])[2]")).click();
		//Click on checkbox 'Arv/g�va'
		driver.findElement(By.xpath("//div[text()='Arv/g�va']")).click();
		//Click on checkbox 'L�n/pension'
		driver.findElement(By.xpath("//div[text()='L�n/pension']")).click();
		//Click on checkbox 'Fastighetsf�rs�ljning'
		driver.findElement(By.xpath("//div[text()='Fastighetsf�rs�ljning']")).click();
		//Click on checkbox 'Kapitalavkastning'
		driver.findElement(By.xpath("//div[text()='Kapitalavkastning']")).click();
		//Click on checkbox 'Annat (beskriv)'
		driver.findElement(By.xpath("(//div[text()='Annat (beskriv)'])[4]")).click();
		//Fill data in textbox 'V�nligen beskriv'
		driver.findElement(By.xpath("(//input[@class='TextField-input'])[6]")).sendKeys("Test data");

		//Click on checkbox 'Svensk bank'
		driver.findElement(By.xpath("//div[text()='Svensk bank']")).click();
		//Click on checkbox 'Svenskt f�rs�kringsbolag'
		driver.findElement(By.xpath("//div[text()='Svenskt f�rs�kringsbolag']")).click();
		//Click on checkbox 'Utl�ndsk bank'
		driver.findElement(By.xpath("//div[text()='Utl�ndsk bank']")).click();
		//Fill data in textbox 'Ange utl�ndsk bank'
		driver.findElement(By.xpath("(//input[@class='TextField-input'])[7]")).sendKeys("Test data");

		WebElement salary_dropdown = driver.findElement(By.className("Select-info"));
		executor.executeScript("arguments[0].scrollIntoView()", salary_dropdown);
		salary_dropdown.click();
		List<WebElement> salary_ele = driver.findElements(By.className("Select-dropdown-option"));
		Random rand = new Random();
		int salary = rand.nextInt(salary_ele.size());
		salary_ele.get(salary).click();
		driver.findElement(By.xpath("(//div[text()='Ja'])[2]")).click();
		//driver.findElement(By.xpath("//div[text()='Koppling']")).sendKeys("Test data");
		WebElement textbox3 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("" +
				"//label[text()='Koppling']/following-sibling::input")));
		textbox3.sendKeys("Test data");
		driver.findElement(By.xpath("//label[text()='Namn']/following-sibling::input")).sendKeys(
				"Test data 1");
		driver.findElement(By.xpath("//label[text()='Befattning']/following-sibling::input")).sendKeys(
				"Test data 2");
		driver.findElement(By.className("Select-info")).click();
		driver.findElement(By.xpath("//input[@placeholder='S�k']")).sendKeys("argentina");
		WebElement country = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Argentina']")));
		country.click();
		driver.findElement(By.xpath("//label[text()='Period']/following-sibling::input")).sendKeys("Test data 3");
		executor.executeScript("arguments[0].click();", move_on);

		//----- PAGE-6 ----- ----- PAGE-6 ----- ----- PAGE-6 -----

		WebElement page6 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//label[text()='F�rs�rjningsplikt antal barn']/following-sibling::input")));
		int no = rand.nextInt(4);
		page6.sendKeys(String.valueOf(no));
		driver.findElement(By.className("Select-info")).click();
		List<WebElement> status = driver.findElements(By.xpath("(//div[@class='Select-dropdown'])[1]/div[@class=" +
				"'Select-dropdown-option']"));
		int randnum  = rand.nextInt(status.size());
		status.get(randnum).click();
		driver.findElement(By.xpath("(//div[@class='Select-info'])[2]")).click();
		List<WebElement> status1 = driver.findElements(By.xpath("(//div[@class='Select-dropdown'])[2]/div[@class=" +
				"'Select-dropdown-option']"));
		int randnum1  = rand.nextInt(status1.size());
		status1.get(randnum1).click();
		driver.findElement(By.xpath("//label[text()='M�nadsinkomst efter skatt']/following-sibling::input")).
				sendKeys("20000");
		driver.findElement(By.xpath("//label[text()='M�nadskostnad']/following-sibling::input")).
				sendKeys("30000");
		driver.findElement(By.xpath("//label[text()='Fastigheter']/following-sibling::input")).
				sendKeys("40000");
		driver.findElement(By.xpath("//label[text()='Skulder']/following-sibling::input")).
				sendKeys("50000");

		List<WebElement> investment = driver.findElements(By.xpath("//td//input[@type='text']"));
		for (WebElement i:investment) {
			int random_investment = rand.nextInt(999999);
			i.sendKeys(String.valueOf(random_investment));
		}
		executor.executeScript("arguments[0].click();", move_on);

		//----- PAGE-7 ----- ----- PAGE-7 ----- ----- PAGE-7 -----

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Nej']")));
		List<WebElement> radio_button1 = driver.findElements(By.xpath("//div[@class='Cob-OptionsList-checkboxes']" +
				"//div[@class='Checkbox-label']"));
		int radio_button = rand.nextInt(radio_button1.size());
		radio_button1.get(radio_button).click();

		int years = rand.nextInt(99);
		driver.findElement(By.className("Cob-SimpleTextField-input")).sendKeys(String.valueOf(years));

		driver.findElement(By.xpath("(//div[@class='Checkbox-checkbox'])[3]")).click();
		executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");

		List<WebElement> text_fields = driver.findElements(By.xpath("//td//input[@type='number']"));
		int j =1;
		for (WebElement i:text_fields) {
			int shares = rand.nextInt(99);
			if (shares > 20) {
				i.sendKeys(String.valueOf(shares));
				driver.findElement(By.xpath("(//div[@class='Select-info'])["+j+"]")).click();
				WebElement Omfattande = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"(//div[@class='Select-dropdown']//div[text()='Omfattande'])["+j+"]")));
				Omfattande.click();
				driver.findElement(By.xpath("(//td[4]//div[@class='Checkbox-checkbox'])["+j+"]")).click();
			}
			else
				i.sendKeys(String.valueOf(shares));
			j++;
		}
		executor.executeScript("arguments[0].click();", move_on);

		//----- PAGE-8 ----- ----- PAGE-8 ----- ----- PAGE-8 -----

		WebElement risk_option = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//h3[text()='V�lj din �nskade riskniv�']")));
		executor.executeScript("arguments[0].scrollIntoView();", risk_option);
		List<WebElement> risk = driver.findElements(By.xpath("//div[@class='Checkbox-checkbox']"));
		int rand_risk = rand.nextInt(risk.size());
		risk.get(rand_risk).click();
		executor.executeScript("arguments[0].click();", move_on);

		//----- PAGE-9 ----- ----- PAGE-9 ----- ----- PAGE-9 -----

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Sparprofil']")));
		List<WebElement> reason = driver.findElements(By.xpath("//div[@class='Select-info']"));
		int ja = 1;
		int jb = 1;
		for (WebElement i :reason) {
			executor.executeScript("arguments[0].scrollIntoView();", i);
			i.click();
			List<WebElement> options = driver.findElements(By.xpath("(//div[@class='Select-dropdown'])" +
					"["+ja+"]/div[@class='Select-dropdown-option']"));
			int rand_reason = rand.nextInt(options.size());
			ja++;
			if (rand_reason == 6){
				options.get(rand_reason).click();
				WebElement other = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"(//label[text()='V�nligen beskriv'])["+jb+"]/following-sibling::input")));
				other.sendKeys("Test data");
				jb++;
			}
			else
				options.get(rand_reason).click();
		}

		WebElement riskprofil = driver.findElement(By.xpath("(//font/span)[1]"));
		//WebElement total_risk = driver.findElement(By.xpath("(//font/span)[2]"));
		int riskprofil1 = Integer.parseInt(riskprofil.getText());
		//int total_risk1 = Integer.parseInt(total_risk.getText());

		for (int i =1; i <= 7; i+=2) {
			List<WebElement> all_risk = driver.findElements(By.xpath(
					"(//div[@class='Cob-OptionsList'])["+i+"]//div[@class='Checkbox-checkbox']"));
			int size = all_risk.size();
			//If risk selection is greater than actual risk
			if (size > riskprofil1)
				//Then select risk with in range of actual risk only
				size = riskprofil1;
			int rand_risk1 = rand.nextInt(size);
			all_risk.get(rand_risk1).click();
		}

		int je =1;
		for (int i =2; i <= 8; i+=2) {
			List<WebElement> all_risk = driver.findElements(By.xpath(
					"(//div[@class='Cob-OptionsList'])[" + i + "]//div[@class='Checkbox-checkbox']"));
			int rand_risk2 = rand.nextInt(all_risk.size());
			if (rand_risk2 == 0) {
				all_risk.get(rand_risk2).click();
				Thread.sleep(300);
				int month = rand.nextInt(24);
				driver.findElement(By.xpath("(//label[text()='Ange antal m�nader'])["+je+"]/following-sibling::input")).
						sendKeys(String.valueOf(month));
				je++;
			}
			else
				all_risk.get(rand_risk2).click();
		}

		//executor.executeScript("window.scrollTo(document.body.scrollHeight, 0)");
		String summa_placeringsbara_tillgångar = driver.findElement(By.xpath("//h4[contains(text(),'Summa " +
				"placeringsbara tillg�ngar: ')]")).getText();
		String totalt_placerat_kapital = driver.findElement(By.xpath("//h4[contains(text(), 'Totalt placerat " +
				"kapital')]")).getText();
		String[] summa_placeringsbara_tillgångar1 = summa_placeringsbara_tillgångar.split(":");
		summa_placeringsbara_tillgångar = summa_placeringsbara_tillgångar1[1].trim();
		String[] totalt_placerat_kapital1 = totalt_placerat_kapital.split(":");
		totalt_placerat_kapital = totalt_placerat_kapital1[1].trim();
		//If slider is not fully selected & total sum percentage is not 100%
		if (!summa_placeringsbara_tillgångar.equals(totalt_placerat_kapital)) {
			WebElement slider = driver.findElement(By.xpath("(//div[@role='slider'])[4]"));
			//Move slider to 100%
			Actions action = new Actions(driver);
			action.moveToElement(slider).clickAndHold().moveByOffset(50, 0).release().build().perform();
		}
		// Click on move on button
		executor.executeScript("arguments[0].click();", move_on);

		//----- PAGE-10 ----- ----- PAGE-10 ----- ----- PAGE-10 -----

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='R�det']")));
		String text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris id elementum enim, sed egestas tortor. " +
				"Phasellus vitae mauris eget leo ultricies pellentesque hendrerit sit amet augue. Maecenas at hendrerit " +
				"felis, at vulputate enim. Curabitur rhoncus mauris ac ligula elementum finibus. Pellentesque a " +
				"pellentesque metus. In porta quam aliquam, molestie lorem in, suscipit nisi. Morbi congue aliquam orci " +
				"eu sollicitudin.";
		Thread.sleep(1000);
		driver.findElement(By.xpath("//textarea[@name='advice_notes']")).sendKeys(text);
		driver.findElement(By.name("advice_notes2")).sendKeys(text);
		driver.findElement(By.name("advice_notes3")).sendKeys(text);
		driver.findElement(By.name("advice_notes4")).sendKeys(text);
		driver.findElement(By.name("advice_notes5")).sendKeys(text);
		executor.executeScript("arguments[0].click();", move_on);

		//----- PAGE-11 ----- ----- PAGE-11 ----- ----- PAGE-11 -----

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Kontoval']")));
		driver.findElement(By.xpath("//div[text()='Ny dep� hos Nord']")).click();
		driver.findElement(By.xpath("//div[text()='ISK']")).click();
		driver.findElement(By.xpath("//div[text()='Brutto']")).click();
		driver.findElement(By.xpath("(//div[@class='Checkbox-checkbox'])[10]")).click();
		driver.findElement(By.xpath("(//div[@class='Checkbox-checkbox'])[11]")).click();
		driver.findElement(By.xpath("//div[text()='Insynsfullmakt']")).click();
		driver.findElement(By.xpath("(//div[@class='Checkbox-checkbox'])[14]")).click();
		driver.findElement(By.className("Select-info")).click();
		driver.findElement(By.xpath("//input[@placeholder='S�k']")).sendKeys("Citibank");
		driver.findElement(By.xpath("//div[@class='Select-dropdown-option highlight']")).click();
		driver.findElement(By.xpath("//label[text()='Clearingnummer']/following-sibling::input")).sendKeys("9040");
		driver.findElement(By.xpath("//label[text()='Kontonummer']/following-sibling::input")).sendKeys("1234567");
		driver.findElement(By.xpath("(//div[@class='Checkbox-checkbox'])[16]")).click();
		driver.findElement(By.xpath("(//div[@class='Checkbox-checkbox'])[18]")).click();
		executor.executeScript("arguments[0].click();", move_on);

		//----- PAGE-12 ----- ----- PAGE-12 ----- ----- PAGE-12 -----

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Placeringsf�rslag']")));
		WebElement right_slider = driver.findElement(By.xpath("//div[@role='slider'][2]"));
		executor.executeScript("arguments[0].scrollIntoView()", right_slider);
		WebElement target_drop = driver.findElement(By.xpath("//span[@class='Step-mark-class'][7]"));

		Actions action = new Actions(driver);
		//Move risk slider and select range 1 to 7
		action.dragAndDrop(right_slider, target_drop).build().perform();
		//Click product type dropdown
		driver.findElement(By.xpath("(//div[@class='Select-info'])[1]")).click();
		String product_type = "ETF:er";

		switch (product_type){
			case "Strukturerade produkter":
				//Select 'Structure Product' from dropdown
				driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+product_type+"']")).click();
				//Un-check checkbox for selecting secondary market
				driver.findElement(By.xpath("//div[text()='Aktuella instrument f�r teckning']")).click();
				//Click on product name dropdown
				driver.findElement(By.xpath("(//div[@class='Select-info'])[2]")).click();
				String product_name = "1053 Buffertbevis Sverige | SE0005991213";
				//Select any product from dropdown
				driver.findElement(By.xpath("//div[@title='"+product_name+"']")).click();
				//Provide Quantity
				driver.findElement(By.xpath("//label[text()='Antal']/following-sibling::input")).sendKeys("110");
				WebElement courtage = driver.findElement(By.xpath("(//div[@role='slider'])[1]"));
				executor.executeScript("arguments[0].scrollIntoView()", courtage);
				//Move courtage slider
				action.moveToElement(courtage).clickAndHold().moveByOffset(50, 0).release().build().perform();
				//Click on button to add product to the listing
				driver.findElement(By.xpath("//div[@class='field']/button")).click();
			case "Obligationer":
				//Select 'Bonds' from dropdown
				driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+product_type+"']")).click();
				//Un-check checkbox for selecting secondary market
				driver.findElement(By.xpath("//div[text()='Aktuella instrument f�r teckning']")).click();
				//Click on product name dropdown
				driver.findElement(By.xpath("(//div[@class='Select-info'])[2]")).click();
				String product_name1 = "Br�drene Greger RB 14/19 | NO0010715246";
				//Select any product from dropdown
				driver.findElement(By.xpath("//div[@title='"+product_name1+"']")).click();
				//Provide Quantity
				driver.findElement(By.xpath("//label[text()='Antal']/following-sibling::input")).sendKeys("110");
				WebElement courtage1 = driver.findElement(By.xpath("(//div[@role='slider'])[1]"));
				executor.executeScript("arguments[0].scrollIntoView()", courtage1);
				//Move courtage slider
				action.moveToElement(courtage1).clickAndHold().moveByOffset(50, 0).release().build().perform();
				//Click on button to add product to the listing
				driver.findElement(By.xpath("//div[@class='field']/button")).click();
			case "Aktier":
				//Select 'Shares' from dropdown
				driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+product_type+"']")).click();
				//Click on product name dropdown
				driver.findElement(By.xpath("(//div[@class='Select-info'])[2]")).click();
				String product_name2 = "Akelius Residential Pref | SE0005936713";
				//Select any product from dropdown
				driver.findElement(By.xpath("//div[@title='"+product_name2+"']")).click();
				//Provide Quantity
				driver.findElement(By.xpath("//label[text()='Antal']/following-sibling::input")).sendKeys("110");
				WebElement courtage2 = driver.findElement(By.xpath("(//div[@role='slider'])[1]"));
				executor.executeScript("arguments[0].scrollIntoView()", courtage2);
				//Move courtage slider
				action.moveToElement(courtage2).clickAndHold().moveByOffset(50, 0).release().build().perform();
				//Click on button to add product to the listing
				driver.findElement(By.xpath("//div[@class='field']/button")).click();
			case "Fonder":
				//Select 'Funds' from dropdown
				driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+product_type+"']")).click();
				//Click on product name dropdown
				driver.findElement(By.xpath("(//div[@class='Select-info'])[2]")).click();
				String product_name3 = "Aktie-Ansvar Avkastningsfond | SE0000735771";
				//Select any product from dropdown
				driver.findElement(By.xpath("//div[@title='"+product_name3+"']")).click();
				//Provide Amount to invest
				driver.findElement(By.xpath("//label[text()='Belopp att investera']/following-sibling::input")).sendKeys("110");
				WebElement courtage3 = driver.findElement(By.xpath("(//div[@role='slider'])[1]"));
				executor.executeScript("arguments[0].scrollIntoView()", courtage3);
				//Move courtage slider
				action.moveToElement(courtage3).clickAndHold().moveByOffset(50, 0).release().build().perform();
				//Click on button to add product to the listing
				driver.findElement(By.xpath("//div[@class='field']/button")).click();
			case "ETF:er":
				//Select 'ETF' from dropdown
				driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+product_type+"']")).click();
				//Click on product name dropdown
				driver.findElement(By.xpath("(//div[@class='Select-info'])[2]")).click();
				String product_name4 = "AC Amerikanska Bolag M�nadsvis 1505GF | SE0007048632";
				//Select any product from dropdown
				driver.findElement(By.xpath("//div[@title='"+product_name4+"']")).click();
				//Provide Quantity
				driver.findElement(By.xpath("//label[text()='Antal']/following-sibling::input")).sendKeys("110");
				WebElement courtage4 = driver.findElement(By.xpath("(//div[@role='slider'])[1]"));
				executor.executeScript("arguments[0].scrollIntoView()", courtage4);
				//Move courtage slider
				action.moveToElement(courtage4).clickAndHold().moveByOffset(50, 0).release().build().perform();
				//Click on button to add product to the listing
				driver.findElement(By.xpath("//div[@class='field']/button")).click();
		}
		//Click on move on button
		executor.executeScript("arguments[0].click()", move_on);

		//----- PAGE-13 ----- ----- PAGE-13 ----- ----- PAGE-13 -----

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='R�dgivarens bed�mning']")));
		//Select yes
		driver.findElement(By.xpath("(//div[@class='Checkbox-checkbox'])[1]")).click();
		//Select No
		driver.findElement(By.xpath("(//div[@class='Checkbox-checkbox'])[4]")).click();
		//Select Yes
		driver.findElement(By.xpath("(//div[@class='Checkbox-checkbox'])[5]")).click();
		//Fill data in textbox
		driver.findElement(By.xpath("(//textarea)[1]")).sendKeys(text);
		//Fill data in textbox
		driver.findElement(By.xpath("(//textarea)[2]")).sendKeys(text);
		//Click on move on button
		executor.executeScript("arguments[0].click()", move_on);

		//----- PAGE-14 ----- ----- PAGE-14 ----- ----- PAGE-14 -----

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Placeringsf�rslag']")));
		executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		WebElement fee1 = driver.findElement(By.xpath("//label[text()='Administrationsavgift']/following-sibling::input"));
		//Clear 'Administrationsavgift' field
		fee1.sendKeys(Keys.BACK_SPACE);
		//Fill fee data
		fee1.sendKeys("100");
		WebElement fee2 = driver.findElement(By.xpath("//label[text()='Flyttavgift']/following-sibling::input"));
		//Clear 'Flyttavgift' field
		fee2.sendKeys(Keys.BACK_SPACE);
		//Fill fee data
		fee2.sendKeys("110");
		WebElement fee3 = driver.findElement(By.xpath("//label[text()='�vriga avgifter']/following-sibling::input"));
		//Clear '�vriga avgifter' field
		fee3.sendKeys(Keys.BACK_SPACE);
		//Fill fee data
		fee3.sendKeys("120");
		//Click on move on button
		executor.executeScript("arguments[0].click()", move_on);

		//----- PAGE-15 ----- ----- PAGE-15 ----- ----- PAGE-15 -----

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Finansiella instrument']")));
		//Click on move on button
		executor.executeScript("arguments[0].click()", move_on);

		//----- PAGE-16 ----- ----- PAGE-16 ----- ----- PAGE-16 -----

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='�vriga dokument']")));
		//Click on drag & drop area
		driver.findElement(By.className("FileDrop-dropbox")).click();
		Thread.sleep(1000);
		//Upload pdf file
		Runtime.getRuntime().exec("C:\\Users\\jyothi\\Downloads\\AutoIT\\fileUpload.exe");
		Thread.sleep(1000);
		//Click on move on button
		executor.executeScript("arguments[0].click()", move_on);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Signering och kundens " +
				"underskrift']")));
		List<WebElement> sign_checkboxes = driver.findElements(By.xpath("//div[@class='Cob-SignAgreement-fields']/div"));

		for (WebElement checkbox : sign_checkboxes) {
			String class_name = checkbox.getAttribute("class");
			//if agreement checkboxes are not checked then mark them checked
			if (!class_name.contains("checked"))
				checkbox.click();
			else
				continue;
		}
		WebElement signera_avtal = driver.findElement(By.xpath("//div[text()='Signera avtal']"));
		//Click on sign agreement button
		executor.executeScript("arguments[0].click();", signera_avtal);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Signera avtal']")));
		//Click in 'Skicka till signering' button
		driver.findElement(By.xpath("//button[@class='Button small filled success']")).click();

		Thread.sleep(10000);
		//driver.close();
	}
}