package utilities;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class CsvUtils {
    // method to read a CSV file;
    public Object[][] getTestData(String fileName) throws IOException {
        // set the UTF-8 character encoding, using a buffer with a character input stream BufferedReader reading file content;
        BufferedReader file = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
        // Ignore read CSV file header row (first row!);
        file.readLine();
        /*
         * Traversing read CSV files all other content except the first line, and there ArrayList named in the records
         * Each object records stored as a String array;
         */
        List<Object[]> records = new ArrayList<Object[]>();
        String record;
        while ((record = file.readLine()) != null) {
            String fields[] = record.split(",");
            records.add(fields);
        }
        // close the file objects;
        file.close();
        // return value of a first function, i.e., Object [] []
        // Convert the list of test data storage Object a two-dimensional array;
        Object[][] results = new Object[records.size()][];
        for (int i = 0; i < records.size(); i++) {
            results[i] = records.get(i);
        }
        return results;
    }
    
    public static CSVRecord getCSVTestData() throws IOException {
    	BufferedReader reader = Files.newBufferedReader(Paths.get(Constants.Path_TestData+"EndtoEndTestData.csv"));
    	CSVParser parser = new CSVParser(reader, CSVFormat.Builder.create().setSkipHeaderRecord(true).setHeader().build());
    	CSVRecord record = parser.getRecords().get(0);
    	//parser.close();
    	return record;
    }
}
