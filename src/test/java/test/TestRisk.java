package test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import se.nordfk.cob.annotation.dataProvider.DataProviderClass;
import se.nordfk.cob.pages.experiencebackground.Experience;
import se.nordfk.cob.pages.risk.Risk;

public class TestRisk extends BaseTest {

	static WebDriver driver;

	public static Risk riskInformation1() throws InterruptedException, IOException
	{
		Experience exp = TestExperience.experienceInformation1();
		Risk risk1 = exp.moveOnToNextPage();
		risk1.riskProfile(record.get("Risk"));
		return risk1;
	}

	@Test(priority=8, dataProvider = "Risk", dataProviderClass = DataProviderClass.class)
	public void riskInformation(String riskValue) throws InterruptedException, IOException
	{
		Experience exp = TestExperience.experienceInformation1();
		Risk risk1 = exp.moveOnToNextPage();
		risk1.riskProfile(riskValue);
	}
}
