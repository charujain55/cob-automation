package test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import se.nordfk.cob.annotation.dataProvider.DataProviderClass;
import se.nordfk.cob.pages.agreement.Agreement;
import se.nordfk.cob.pages.customerinfo.CustomerInfo;


public class TestAgreement extends BaseTest
{
	static WebDriver driver;

	public static Agreement agreementInformation1() throws InterruptedException, IOException 
 	{   
		CustomerInfo customerInfo = TestCustomerInfo.customerInformation1();
 		Agreement agreement = customerInfo.moveOnToNextPage();
 		agreement.selectCheckBoxes(record.get("Form_of_compensation"));
		return agreement;
	}
	
	@Test(priority=3, dataProvider = "agreement", dataProviderClass = DataProviderClass.class)
	public static void agreementInformation(String formOfCompensation) throws InterruptedException, IOException 
	{   
		CustomerInfo customerInfo = TestCustomerInfo.customerInformation1();
		customerInfo.fatcaButtonSelect();
		Agreement agreement = customerInfo.moveOnToNextPage();
		agreement.selectCheckBoxes(formOfCompensation);
	}
}
