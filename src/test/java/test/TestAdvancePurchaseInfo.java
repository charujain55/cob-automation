package test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import se.nordfk.cob.pages.advancepurchase.AdvancePurchaseInfo;
import se.nordfk.cob.pages.agreement.Agreement;

public class TestAdvancePurchaseInfo extends BaseTest{

	static WebDriver driver;

	public static AdvancePurchaseInfo purchaseInformation1() throws InterruptedException, IOException 
	{   
		Agreement agreement = TestAgreement.agreementInformation1();
		AdvancePurchaseInfo purchase = agreement.moveOnToNextPage();
		purchase.purchaseInfoCheckboxes(record.get("Insurance_class"), record.get("Insurance_classes"), 
				record.get("Product_company"), record.get("Securities_company"), 
				record.get("Companies_with_agreement"), record.get("Companies_without_agreement"));
		return purchase;
	}

	@Test(priority=4, dataProvider = "AdvancePurchaseInfo", dataProviderClass=DataProvider.class)
	public void purchaseInformation(String insuranceClasse, String insuranceClasses, 
			String ProductCompany, String SecuritiesCompany, 
			String CompaniesWithAgreement, String CompaniesWithoutAgreement) throws InterruptedException, IOException 
	{   
		Agreement agreement = TestAgreement.agreementInformation1();
		AdvancePurchaseInfo purchase = agreement.moveOnToNextPage();
		purchase.purchaseInfoCheckboxes(insuranceClasse, insuranceClasses, ProductCompany, 
				SecuritiesCompany, CompaniesWithAgreement, CompaniesWithoutAgreement);
	}
}
