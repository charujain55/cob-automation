package test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import se.nordfk.cob.annotation.dataProvider.DataProviderClass;
import se.nordfk.cob.pages.advancepurchase.AdvancePurchaseInfo;
import se.nordfk.cob.pages.moneylaundering.MoneyLaundering;

public class TestMoneyLaundering extends BaseTest {

	static WebDriver driver;

	public static MoneyLaundering moneyLaunderingInformation1()  throws InterruptedException, IOException{   
		AdvancePurchaseInfo purchaseInfo = TestAdvancePurchaseInfo.purchaseInformation1();
		MoneyLaundering money = purchaseInfo.moveOnToNextPage();
		money.moneyLaundering(record.get("Reason_for_business_with_NordFK"), record.get("Purpose_of_company_investment"), 
				record.get("Investment_on_behalf_of"), record.get("Purpose_of_investing_other_than_achieving_returns"), 
				record.get("Amount_invested_in_securities"), record.get("Resource_of_capital"), 
				record.get("Foreign_bank"), record.get("Foreign_insurance_company"), record.get("Coupling"), 
				record.get("Name"), record.get("Position"), record.get("Country"), record.get("Period"));
		return money;
	}

	@Test(priority=5, dataProvider = "MoneyLaundering", dataProviderClass = DataProviderClass.class)
	public void moneyLaunderingInformation(String reasonForBusinessWithNordFK, String purposeOfCompanyInvestment, 
			String investmentOnBehalfOf, String purposeOfInvestingOtherThanAchievingReturns, String amount, 
			String resourceOfCapital, String foreignBank, String foreignInsuranceCompany, String coupling, 
			String name,String position, String country, String period)  throws InterruptedException, IOException{   
		AdvancePurchaseInfo purchaseInfo = TestAdvancePurchaseInfo.purchaseInformation1();
		MoneyLaundering money = purchaseInfo.moveOnToNextPage();
		money.moneyLaundering(reasonForBusinessWithNordFK, purposeOfCompanyInvestment, investmentOnBehalfOf, 
				purposeOfInvestingOtherThanAchievingReturns, amount, resourceOfCapital, foreignBank, foreignInsuranceCompany, 
				coupling, name, position, country, period);
	}
}
