package test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import se.nordfk.cob.pages.documents.Documents;
import se.nordfk.cob.pages.placement.Placement;

public class TestDocuments extends BaseTest{

	static WebDriver driver;

	public static Documents placementInformation1() throws InterruptedException, IOException{
		Placement placement = TestPlacement.placementInformation1();		
		placement.proposal();

		Documents documents = placement.moveOnToNextPage();
		documents.otherDocuments();
		return documents;
	}

	@Test(priority=17)
	public void placementInformation() throws InterruptedException, IOException{
		Placement placement = TestPlacement.placementInformation1();		
		Documents documents = placement.moveOnToNextPage();
		documents.otherDocuments();
	}
}
