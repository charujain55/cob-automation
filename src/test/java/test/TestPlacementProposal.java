package test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import se.nordfk.cob.annotation.dataProvider.DataProviderClass;
import se.nordfk.cob.pages.placementproposal.PlacementProposal;
import se.nordfk.cob.pages.preregisteredaccount.PreRegisteredAccount;

public class TestPlacementProposal extends BaseTest{

	static WebDriver driver;

	public static PlacementProposal proposalInformation1() throws InterruptedException, IOException{
		PreRegisteredAccount registeraccount = TestPreRegisteredAccount.registeredAccountInformation1();
		PlacementProposal placement = registeraccount.moveOnToNextPage();
		placement.placementProposal();
		placement.addStructuredProduct(record.get("Buy_structure_product"), record.get("Buy_structure_product_quantity"));
		placement.addObligationer(record.get("Buy_bond"), record.get("Buy_bond_quantity"));
		placement.addAktier(record.get("Buy_shares"), record.get("Buy_shares_quantity"));
		placement.addFonder(record.get("Buy_fund"), record.get("Buy_fund_belopp"));
		placement.addEtf(record.get("Buy_Etf"), record.get("Buy_Etf_quantity"));
		placement.placementProposalSell();
		placement.addStructuredProductForSell(record.get("Sell_structure_product"), record.get("Sell_structure_product_quantity"));
		placement.addObligationerForSell(record.get("Sell_bond"), record.get("Sell_bond_quantity"));
		placement.addAktierForSell(record.get("Sell_shares"), record.get("Sell_shares_quantity"));
		placement.addFonderForSell(record.get("Sell_fund"));
		placement.addEtfForSell(record.get("Sell_Etf"), record.get("Sell_Etf_quantity"));
		return placement;
	}

	@Test(priority=13, dataProvider = "PlacementProposal", dataProviderClass = DataProviderClass.class)
	public void proposalInformation(String productNameSP, String quantitySP, String productNameBond, String quantityBond, 
			String productNameAktier, String quantityAktier, String productNameFund, String quantityFund, String productNameEtf, 
			String quantityEtf, String SellProductNameSP, String SellQuantitySP, String SellProductNameBond, String SellQuantityInvestBond, 
			String SellProductNameAktier, String SellQuantityInvestAktier, String SellProductNameFund, String SellProductNameEtf, 
			String SellQuantityInvestEtf) throws InterruptedException, IOException{
		PreRegisteredAccount registeraccount = TestPreRegisteredAccount.registeredAccountInformation1();
		PlacementProposal placement = registeraccount.moveOnToNextPage();
		placement.placementProposal();
		placement.addStructuredProduct(productNameSP, quantitySP);
		placement.addObligationer(productNameBond, quantityBond);
		placement.addAktier(productNameAktier, quantityAktier);
		placement.addFonder(productNameFund, quantityFund);
		placement.addEtf(productNameEtf, quantityEtf);
		placement.placementProposalSell();
		placement.addStructuredProductForSell(SellProductNameSP, SellQuantitySP);
		placement.addObligationerForSell(SellProductNameBond, SellQuantityInvestBond);
		placement.addAktierForSell(SellProductNameAktier, SellQuantityInvestAktier);
		placement.addFonderForSell(SellProductNameFund);
		placement.addEtfForSell(SellProductNameEtf, SellQuantityInvestEtf);
	}
}
