package test;
import java.io.IOException;
import org.testng.annotations.Test;
import se.nordfk.cob.annotation.dataProvider.*;
import se.nordfk.cob.pages.advisorlogin.CustomerSsnLogin;

public class TestLoginSSN extends BaseTest 
{
	
	@Test(priority = 0, dataProvider = "bankIDAdvisorSSN", dataProviderClass = DataProviderClass.class)
	public void ssnAuthentication(String advisorSSN, String customerSSN) throws InterruptedException, IOException
	{   
		CustomerSsnLogin ssnlogin = cobLoginPage.legitimeraButtonClick(advisorSSN);
		ssnlogin.enterCustomerSSN(customerSSN);
	}

}

