package test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import se.nordfk.cob.annotation.dataProvider.DataProviderClass;
import se.nordfk.cob.pages.moneylaundering.MoneyLaundering;
import se.nordfk.cob.pages.savingprofile.SavingProfile;

public class TestSavingProfile extends BaseTest{

	static WebDriver driver;

	public static SavingProfile incomeInformation1() throws InterruptedException, IOException 
	{
		MoneyLaundering money = TestMoneyLaundering.moneyLaunderingInformation1();
		SavingProfile savingProfile = money.moveOnToNextPage();
		savingProfile.customerProfile(record.get("No_of_children"), record.get("Marital_status"), record.get("Type_of_employment"), 
				record.get("Monthly_income"), record.get("Monthly_cost"), record.get("Real_estate"), record.get("Liabilities"));
		savingProfile.existingSavings(record.get("Risk_1_horizon_1"), record.get("Risk_1_horizon_2"), record.get("Risk_1_horizon_3"), 
				record.get("Risk_1_horizon_4"), record.get("Risk_2_horizon_1"), record.get("Risk_2_horizon_2"), 
				record.get("Risk_2_horizon_3"), record.get("Risk_2_horizon_4"), record.get("Risk_3_horizon_1"), 
				record.get("Risk_3_horizon_2"), record.get("Risk_3_horizon_3"), record.get("Risk_3_horizon_4"), 
				record.get("Risk_4_horizon_1"), record.get("Risk_4_horizon_2"), record.get("Risk_4_horizon_3"), 
				record.get("Risk_4_horizon_4"), record.get("Risk_5_horizon_1"), record.get("Risk_5_horizon_2"), 
				record.get("Risk_5_horizon_3"), record.get("Risk_5_horizon_4"), record.get("Risk_6_horizon_1"), 
				record.get("Risk_6_horizon_2"), record.get("Risk_6_horizon_3"), record.get("Risk_6_horizon_4"), 
				record.get("Risk_7_horizon_1"), record.get("Risk_7_horizon_2"), record.get("Risk_7_horizon_3"), 
				record.get("Risk_7_horizon_4"));
		return savingProfile;
	}

	@Test(priority=6, dataProvider = "SavingProfile", dataProviderClass = DataProviderClass.class)
	public void incomeInformation(String noOfChildren, String statusMarital, String typeEmployment, 
			String monthlyIncomeAmount, String monthlyCostAmount, String realEstateAmount, String liability,
			String risk1horizon1, String risk1horizon2, String risk1horizon3, String risk1horizon4, 
			String risk2horizon1, String risk2horizon2, String risk2horizon3, String risk2horizon4, 
			String risk3horizon1, String risk3horizon2, String risk3horizon3, String risk3horizon4, 
			String risk4horizon1, String risk4horizon2, String risk4horizon3, String risk4horizon4, 
			String risk5horizon1, String risk5horizon2, String risk5horizon3, String risk5horizon4, 
			String risk6horizon1, String risk6horizon2, String risk6horizon3, String risk6horizon4, 
			String risk7horizon1, String risk7horizon2, String risk7horizon3, String risk7horizon4) 
					throws InterruptedException, IOException {   
		MoneyLaundering money = TestMoneyLaundering.moneyLaunderingInformation1();
		SavingProfile savingProfile = money.moveOnToNextPage();
		savingProfile.customerProfile(noOfChildren, statusMarital, typeEmployment, monthlyIncomeAmount, 
				monthlyCostAmount, realEstateAmount, liability);
		savingProfile.existingSavings(risk1horizon1, risk1horizon2, risk1horizon3, risk1horizon4, 
				risk2horizon1, risk2horizon2, risk2horizon3, risk2horizon4, 
				risk3horizon1, risk3horizon2, risk3horizon3, risk3horizon4, 
				risk4horizon1, risk4horizon2, risk4horizon3, risk4horizon4, 
				risk5horizon1, risk5horizon2, risk5horizon3, risk5horizon4, 
				risk6horizon1, risk6horizon2, risk6horizon3, risk6horizon4, 
				risk7horizon1, risk7horizon2, risk7horizon3, risk7horizon4);
	}
}
