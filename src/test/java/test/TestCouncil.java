package test;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import se.nordfk.cob.annotation.dataProvider.DataProviderClass;
import se.nordfk.cob.pages.council.Council;
import se.nordfk.cob.pages.riskprofile.RiskProfile;

public class TestCouncil extends BaseTest {

	static WebDriver driver;

	public static Council meetingInformation1() throws InterruptedException, IOException{
		RiskProfile riskProfile = TestRiskProfile.capitalSavingProfile1();
		Council council = riskProfile.moveOnToNextPage();
		council.meetingNotes(record.get("Background_and_condition"), record.get("Things_discussed_in_meeting"), 
				record.get("Information_about_portfolio"), record.get("Third_party_compensation"), record.get("Other"));
		return council;
	}

	@Test (priority=10, dataProvider = "MeetingNotes", dataProviderClass = DataProviderClass.class)
	public void meetingInformation(String backgroundAndCondition, String thingsDiscussedInMeeting, 
			String informationAboutPortfolio, String thirdPartyCompensation, String other) throws InterruptedException, IOException{
		RiskProfile riskProfile = TestRiskProfile.capitalSavingProfile1();
		Council council = riskProfile.moveOnToNextPage();
		council.meetingNotes(backgroundAndCondition, thingsDiscussedInMeeting, informationAboutPortfolio, 
				thirdPartyCompensation, other);
	}
}
