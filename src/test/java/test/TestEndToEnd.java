package test;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.github.bonigarcia.wdm.WebDriverManager;
import se.nordfk.cob.pages.advancepurchase.AdvancePurchaseInfo;
import se.nordfk.cob.pages.advisorassessment.AdvisorAssessment;
import se.nordfk.cob.pages.advisorlogin.CustomerSsnLogin;
import se.nordfk.cob.pages.agreement.Agreement;
import se.nordfk.cob.pages.coblogin.CobLoginPage;
import se.nordfk.cob.pages.council.Council;
import se.nordfk.cob.pages.customerinfo.CustomerInfo;
import se.nordfk.cob.pages.documents.Documents;
import se.nordfk.cob.pages.experiencebackground.Experience;
import se.nordfk.cob.pages.moneylaundering.MoneyLaundering;
import se.nordfk.cob.pages.placement.Placement;
import se.nordfk.cob.pages.placementproposal.PlacementProposal;
import se.nordfk.cob.pages.proposal.Proposal;
import se.nordfk.cob.pages.risk.Risk;
import se.nordfk.cob.pages.riskprofile.RiskProfile;
import se.nordfk.cob.pages.savingprofile.SavingProfile;


public class TestEndToEnd{

	static WebDriver driver;

	@BeforeClass
	public WebDriver setUpDriver() throws Exception{
		//Initialize Driver
		WebDriverManager.chromedriver().setup();

		driver= new ChromeDriver();   		

		//Go to COB application
		driver.get("https://cob-advisor-nordfk-uat.nord-b5t-dev.nordfk.se/nordfk-cob");

		//Maximize browser window
		driver.manage().window().maximize();

		return driver;
	}

	//*****cobLoginPage*****

	@Test(priority=0)
	public void bankidAuthentication() throws InterruptedException, IOException{
		//Create object of Advisor Login page
		CobLoginPage cobLogin = new CobLoginPage(driver);

		//Click on Legitimera button provide Advisor SSN and Login
		cobLogin.legitimeraButtonClick("199008217821");
	}


	//*****Customer_SSN_Login*****

	@Test(priority=1)
	public void ssnAuthentication() throws InterruptedException, IOException{   
		//Create object of Person Login page 
		CustomerSsnLogin testLogin = new CustomerSsnLogin(driver);
		//Enter Customer SSN
		testLogin.enterCustomerSSN("194506172222");
	}

	//*****Customer_Info*****

	@Test(priority=2)
	public static void customerInformation() throws InterruptedException, IOException{

		//Create object of Customer Info page
		CustomerInfo customerInfo = new CustomerInfo(driver);	
		customerInfo.fatcaButtonSelect();
	}

	//*****Agreement*****

	@Test(priority=3)

	public static void agreementInformation() throws InterruptedException, IOException {
		//Create object of Agreement page
		Agreement agreement = new Agreement(driver);
		agreement.selectCheckBoxes("Test data");
	}

	//*****Purchase_Info*****

	@Test(priority=4)

	public static void purchaseInformation() throws InterruptedException, IOException{
		//Create object of Purchase Information page	
		AdvancePurchaseInfo purchase = new  AdvancePurchaseInfo(driver);
		purchase.purchaseInfoCheckboxes("Test data","Test data","Test data","Test data","Test data","Test data");
	}

	//*****Money_Laundering*****

	@Test(priority=5)

	public static void moneyLaunderingInformation() throws InterruptedException, IOException{
		////Create object of Money laundering page
		MoneyLaundering money = new MoneyLaundering(driver);
		money.moneyLaundering("Test data","Test data","Test data","Test data","1250000000000","Test data",
				"Test data","Test data","Test data","Richard","Senator","argentina","Test data 3");
	}

	//*****Saving_Profile*****

	@Test(priority=6)

	public static void incomeInformation() throws InterruptedException, IOException{
		//Create object of Saving Profile page	
		SavingProfile income = new SavingProfile(driver);
		income.customerProfile("0","Skild","Pension�r","100000","50000","200000","50000");
		income.existingSavings("10000","20000","30000","40000","10000","20000","30000","40000","10000","20000","30000",
				"40000","10000","20000","30000","40000","10000","20000","30000","40000","10000","20000","30000","40000",
				"10000","20000","30000","40000");
	}

	//*****Experience*****

	@Test(priority=7)

	public static void experienceInformation(){
		//Create object of Experience background page
		Experience exp = new Experience(driver);
		exp.experience("Ja","5");
		exp.tradingExperience("0","8","11","16","0","0","0","0","0","0","0");		
	}

	//*****Risk-*****

	@Test(priority=8)

	public static void riskInformation(){
		//Create object of Risk page
		Risk risk = new Risk(driver);
		risk.riskProfile("3");

	}

	//*****Risk_Profile*****

	@Test(priority=9)

	public static void riskCalculationInformation() throws InterruptedException{
		//Create object of Risk Profile page
		RiskProfile riskpro = new RiskProfile(driver);
		riskpro.goalofSaving("Annat","Test Other goal of saving1","Privat konsumtion","Test Other goal of saving2",
				"Ekonomisk trygghet","Test Other goal of saving3","Pension","Test Other goal of saving4");
		riskpro.horizonRiskLevel("1","2","3","4");
		riskpro.earlySaving("Ja","Ja","Ja","Nej","11","12","6","5");
	}

	//*****Council*****

	@Test(priority=10)

	public static void meetingInformation() throws InterruptedException{
		//Create object of Council page
		Council council = new Council(driver);
		council.meetingNotes("Test Background and condition","Test Things discussed in meeting","Test Information about portfolio",
				"Test Third party compensation","Test Other");
	}

	//*****Account_Selection*****

	@Test(priority=11)

	public static void accountInformation(){
		//Create object of Account Selection page	
		//AccountSelection account = new AccountSelection(driver);		
		//account.accountSelect("Danske Bank","1234","9876543210");
	}

	//*****Placement_Proposal*****

	@Test(priority=12)

	public static void proposalInformation() throws InterruptedException{	
		//Create object of Placement Proposal page
		PlacementProposal placement = new PlacementProposal(driver);	
		placement.placementProposal();
		placement.addStructuredProduct("1053 Buffertbevis Sverige | SE0005991213","3000");
		placement.addObligationer("Concent Utveckling Holding 3 AB | SE0008374797","2000");
		placement.addAktier("Akelius Residential Pref | SE0005936713","2000");
		placement.addFonder("Aktie-Ansvar Avkastningsfond | SE0000735771","1000");
		placement.addEtf("AC Amerikanska Bolag M�nadsvis 1505GF | SE0007048632","1000");
		placement.addStructuredProductForSell("1053 Buffertbevis Sverige | SE0005991213","2000");
		placement.addObligationerForSell("Concent Utveckling Holding 3 AB | SE0008374797","2000");
		placement.addAktierForSell("Akelius Residential Pref | SE0005936713","2000");
		placement.addFonderForSell("Aktie-Ansvar Avkastningsfond | SE0000735771");
		placement.addEtfForSell("AC Amerikanska Bolag M�nadsvis 1505GF | SE0007048632","2000");
	}

	//*****Assessment*****

	@Test(priority=13)

	public static void  assessmentInformation(){
		//Create object of Assessment page
		AdvisorAssessment assess = new AdvisorAssessment(driver);		
		assess.assessment("Nej","Ja","Ja","Test feedback about customer investment","Nothing for now");
	}

	//*****Proposal*****

	@Test(priority=14)

	public static void proposalInfo(){	
		//Create object of Proposal page
		Proposal proposal = new Proposal(driver);	
		proposal.productProposal("100", "200","300");				
	}

	//*****Placement*****

	@Test(priority=15)

	public static void placementInformation(){
		//Create object of Placement page
		Placement placement = new  Placement(driver);		
		placement.proposal();

	}

	//*****Documents*****

	@Test (priority=16)

	public static void signingInformation() throws InterruptedException, IOException{
		//Create object of Documents page
		Documents documents = new Documents(driver);
		documents.otherDocuments();	

	}


}

