package test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import se.nordfk.cob.annotation.dataProvider.DataProviderClass;
import se.nordfk.cob.pages.accountselection.AccountSelection;
import se.nordfk.cob.pages.preregisteredaccount.PreRegisteredAccount;

public class TestPreRegisteredAccount extends BaseTest{

	static WebDriver driver;

	public static PreRegisteredAccount registeredAccountInformation1() throws InterruptedException, IOException{
		AccountSelection account = TestAccountSelection.accountInformation1();	
		PreRegisteredAccount registeraccount = account.moveOnToNextPage();
		registeraccount.accountSelection(record.get("Do_you_want_to_add_new_bank_account"), record.get("Bank_name"), 
				record.get("Clearing_number"), record.get("Account_number"));
		return registeraccount;
	}

	@Test(priority=12, dataProvider = "PreRegisteredAccount", dataProviderClass = DataProviderClass.class)
	public void registeredAccountInformation(String doYouWantToAddNewBankAccount, String bankName, String clearingNumber, 
			String accountNumber) throws InterruptedException, IOException{
		AccountSelection account = TestAccountSelection.accountInformation1();	
		PreRegisteredAccount registeraccount = account.moveOnToNextPage();
		registeraccount.accountSelection(doYouWantToAddNewBankAccount,  bankName,  clearingNumber, accountNumber);
	}
}
