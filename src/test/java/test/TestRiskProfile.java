package test;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import se.nordfk.cob.annotation.dataProvider.DataProviderClass;
import se.nordfk.cob.pages.risk.Risk;
import se.nordfk.cob.pages.riskprofile.RiskProfile;

public class TestRiskProfile extends BaseTest {

	static WebDriver driver;

	public static RiskProfile capitalSavingProfile1() throws InterruptedException, IOException{		
		Risk portfolioRisk = TestRisk.riskInformation1();
		RiskProfile savingProfile = portfolioRisk.moveOnToNextPage();
		savingProfile.goalofSaving(record.get("Horizon1_goal_of_saving"), record.get("Other_goal_of_saving1"), 
				record.get("Horizon2_goal_of_saving"), record.get("Other_goal_of_saving2"), record.get("Horizon3_goal_of_saving"), 
				record.get("Other_goal_of_saving3"), record.get("Horizon4_goal_of_saving"), record.get("Other_goal_of_saving4"));
		savingProfile.horizonRiskLevel(record.get("Horizon1_risk_value"), record.get("Horizon2_risk_value"), 
				record.get("Horizon3_risk_value"), record.get("Horizon4_risk_value"));
		savingProfile.earlySaving(record.get("Do_you_need_to_save_early_horizon1"), record.get("Do_you_need_to_save_early_horizon2"), 
				record.get("Do_you_need_to_save_early_horizon3"), record.get("Do_you_need_to_save_early_horizon4"), 
				record.get("No_of_months_horizon1"), record.get("No_of_months_horizon2"), record.get("No_of_months_horizon3"), 
				record.get("No_of_months_horizon4"));
		return savingProfile;
	}

	@Test(priority=9, dataProvider = "RiskProfile", dataProviderClass = DataProviderClass.class)
	public void capitalSavingProfile(String horizon1GoalOfSaving, String otherGoalOfSaving1, String horizon2GoalOfSaving, 
			String otherGoalOfSaving2, String horizon3GoalOfSaving, String otherGoalOfSaving3, 
			String horizon4GoalOfSaving, String otherGoalOfSaving4, String horizon1RiskValue, 
			String horizon2RiskValue, String horizon3RiskValue, String horizon4RiskValue, String doYouNeedToSaveEarlyHorizon1, 
			String doYouNeedToSaveEarlyHorizon2, String doYouNeedToSaveEarlyHorizon3, String doYouNeedToSaveEarlyHorizon4, 
			String noOfMonthsHorizon1, String noOfMonthsHorizon2, String noOfMonthsHorizon3, String noOfMonthsHorizon4) 
					throws InterruptedException, IOException{
		Risk portfolioRisk = TestRisk.riskInformation1();
		RiskProfile savingProfile = portfolioRisk.moveOnToNextPage();
		savingProfile.goalofSaving(horizon1GoalOfSaving, otherGoalOfSaving1, horizon2GoalOfSaving, otherGoalOfSaving2, 
				horizon3GoalOfSaving, otherGoalOfSaving3, horizon4GoalOfSaving, otherGoalOfSaving4);
		savingProfile.horizonRiskLevel(horizon1RiskValue, horizon2RiskValue, horizon3RiskValue, horizon4RiskValue);
		savingProfile.earlySaving(doYouNeedToSaveEarlyHorizon1, doYouNeedToSaveEarlyHorizon2, doYouNeedToSaveEarlyHorizon3, 
				doYouNeedToSaveEarlyHorizon4, noOfMonthsHorizon1, noOfMonthsHorizon2, noOfMonthsHorizon3, noOfMonthsHorizon4);
	}
}
