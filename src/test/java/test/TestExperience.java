package test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import se.nordfk.cob.annotation.dataProvider.DataProviderClass;
import se.nordfk.cob.pages.experiencebackground.Experience;
import se.nordfk.cob.pages.savingprofile.SavingProfile;

public class TestExperience extends BaseTest {

	static WebDriver driver;

	public static Experience experienceInformation1() throws InterruptedException, IOException{
		SavingProfile savingProfile = TestSavingProfile.incomeInformation1(); 
		Experience exp = savingProfile.moveOnToNextPage();
		exp.experience(record.get("Do_you_have_knowledge_of_financial_investment"), record.get("Years_trading_in_securities"));
		exp.tradingExperience(record.get("No_of_transaction_Shares_Listed"), record.get("No_of_transaction_Shares_UnListed"), 
				record.get("No_of_transaction_Fonder_UCITS"), record.get("No_of_transaction_Fund_AIF"), 
				record.get("No_of_transaction_Bonds_Listed"), record.get("No_of_transaction_Bonds_UnListed"), 
				record.get("No_of_transaction_Stock_Index_Bonds"), record.get("No_of_transaction_Sprinter"), 
				record.get("No_of_transaction_Credit_Certificate"), record.get("No_of_transaction_Autocalls"), 
				record.get("No_of_transaction_Derivatives"));
		return exp;
	}

	@Test(priority=7, dataProvider = "Experience", dataProviderClass = DataProviderClass.class)
	public void experienceInformation(String radioButtonSelection, String tradingNoOfYears, String noOfSharesListed, 
			String noOfSharesUnListed, String noOfFonderUCITS, String noOfFundAIF, String noOfBondsListed, 
			String noOfBondsUnListed, String noOfStockIndexBonds, String noOfSprinter, String noOfCreditCertificate, 
			String noOfAutocalls, String noOfDerivatives) throws InterruptedException, IOException{  
		SavingProfile savingProfile = TestSavingProfile.incomeInformation1(); 
		Experience exp = savingProfile.moveOnToNextPage();
		exp.experience(radioButtonSelection, tradingNoOfYears);
		exp.tradingExperience(noOfSharesListed, noOfSharesUnListed, noOfFonderUCITS, noOfFundAIF, noOfBondsListed, 
				noOfBondsUnListed, noOfStockIndexBonds, noOfSprinter, noOfCreditCertificate, noOfAutocalls, noOfDerivatives);
	}
}
