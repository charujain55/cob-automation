/*package test1;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.github.bonigarcia.wdm.WebDriverManager;
//import pages.account_selection.Account_Selection;
import pages.advisor_login.Customer_SSN_Login;
import pages.agreement.Agreement;
import pages.assessment.Assessment;
import pages.advance_purchase.Purchase_Info;
import pages.cob_login.cobLoginPage;
import pages.council.Council;
import pages.customer_info.Customer_Info;
import pages.documents.Documents;
import pages.experience_background.Experience;
import pages.money_laundering.Money_Laundering;
import pages.placement.Placement;
import pages.placement_proposal.Placement_Proposal;
import pages.proposal.Proposal;
import pages.risk.Risk;
import pages.risk_profile.Risk_Profile;
import pages.saving_profile.Saving_Profile;


public class Test_End_to_End{

	static WebDriver driver;

	@BeforeClass
	public WebDriver setUpDriver() throws Exception{
		//Initialize Driver
		WebDriverManager.chromedriver().setup();

		driver= new ChromeDriver();   		

		//Go to COB application
		driver.get("https://cob-advisor-nordfk-uat.nord-b5t-dev.nordfk.se/nordfk-cob");

		//Maximize browser window
		driver.manage().window().maximize();

		return driver;
	}

	//*****cobLoginPage*****

	@Test(priority=0)
	public void bankid_authentication() throws InterruptedException, IOException{
		//Create object of Advisor Login page
		cobLoginPage cob_login = new cobLoginPage(driver);

		//Click on Legitimera button provide Advisor SSN and Login
		cob_login.legitimera_button_click("199008217821");
	}


	//*****Customer_SSN_Login*****

	@Test(priority=1)
	public void ssn_authentication() throws InterruptedException, IOException{   
		//Create object of Person Login page 
		Customer_SSN_Login testlogin = new Customer_SSN_Login(driver);
		//Enter Customer SSN
		testlogin.enterCustomerSSN("194506172222");
	}

	//*****Customer_Info*****

	@Test(priority=2)
	public static void customer_information() throws InterruptedException, IOException{

		//Create object of Customer Info page
		Customer_Info customer_info = new Customer_Info(driver);	
		customer_info.FatcaButton_select();
	}

	//*****Agreement*****

	@Test(priority=3)

	public static void agreement_information() throws InterruptedException, IOException {
		//Create object of Agreement page
		Agreement agreement = new Agreement(driver);
		agreement.checkboxes("Test data");
	}

	//*****Purchase_Info*****

	@Test(priority=4)

	public static void purchase_information() throws InterruptedException, IOException{
		//Create object of Purchase Information page	
		Purchase_Info purchase = new Purchase_Info(driver);
		purchase.checkbox1("Test data","Test data","Test data","Test data","Test data","Test data");
	}

	//*****Money_Laundering*****

	@Test(priority=5)

	public static void moneylaundering_information() throws InterruptedException, IOException{
		////Create object of Money laundering page
		Money_Laundering money = new Money_Laundering(driver);
		money.money_laundering("Test data","Test data","Test data","Test data","1250000000000","Test data",
				"Test data","Test data","Test data","Richard","Senator","argentina","Test data 3");
	}

	//*****Saving_Profile*****

	@Test(priority=6)

	public static void income_information() throws InterruptedException, IOException{
		//Create object of Saving Profile page	
		Saving_Profile income = new Saving_Profile(driver);
		income.customer_profile("50000","2000","10000","2000");
		income.existingsavings();
	}

	//*****Experience*****

	@Test(priority=7)

	public static void experience_information(){
		//Create object of Experience background page
		Experience exp = new Experience(driver);
		exp.experience();
		exp.tradingexperience();		
	}

	//*****Risk-*****

	@Test(priority=8)

	public static void risk_information(){
		//Create object of Risk page
		Risk risk = new Risk(driver);
		risk.riskprofile();

	}

	//*****Risk_Profile*****

	@Test(priority=9)

	public static void risk_calculation_information() throws InterruptedException{
		//Create object of Risk Profile page
		Risk_Profile riskpro = new Risk_Profile(driver);
		riskpro.risk_profile("Test data");
		riskpro.risk_calculation();
	}

	//*****Council*****

	@Test(priority=10)

	public static void meeting_information() throws InterruptedException{
		//Create object of Council page
		Council council = new Council(driver);
		council.meeting_notes(" Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris id elementum enim, "
				+ "sed egestas tortor. Phasellus vitae mauris eget leo ultricies pellentesque hendrerit sit amet augue."
				+ " Maecenas at hendrerit felis, at vulputate enim. Curabitur rhoncus mauris ac ligula elementum finibus."
				+ " Pellentesque a pellentesque metus. In porta quam aliquam, molestie lorem in, suscipit nisi. "
				+ "Morbi congue aliquam orci eu sollicitudin.");
	}

	//*****Account_Selection*****

	@Test(priority=11)

	public static void account_information(){
		//Create object of Account Selection page	
		Account_Selection account = new Account_Selection(driver);		
		account.account_select("Danske Bank","1234","9876543210");
	}

	//*****Placement_Proposal*****

	@Test(priority=12)

	public static void proposal_information() throws InterruptedException{	
		//Create object of Placement Proposal page
		Placement_Proposal placement = new Placement_Proposal(driver);	
		placement.placement_proposal("1000", "1000", "1000", "1000", "1000");	
	}

	//*****Assessment*****

	@Test(priority=13)

	public static void  assessment_information(){
		//Create object of Assessment page
		Assessment assess = new Assessment(driver);		
		assess.assessment("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris id elementum enim, "
				+ "sed egestas tortor. Phasellus vitae mauris eget leo ultricies pellentesque hendrerit sit amet augue."
				+ " Maecenas at hendrerit felis, at vulputate enim. Curabitur rhoncus mauris ac ligula elementum finibus."
				+ " Pellentesque a pellentesque metus. In porta quam aliquam, molestie lorem in, suscipit nisi. "
				+ "Morbi congue aliquam orci eu sollicitudin.");
	}

	//*****Proposal*****

	@Test(priority=14)

	public static void proposal_info(){	
		//Create object of Proposal page
		Proposal proposal = new Proposal(driver);	
		proposal.product_proposal("100", "200","300");				
	}

	//*****Placement*****

	@Test(priority=15)

	public static void placement_information(){
		//Create object of Placement page
		Placement placement = new  Placement(driver);		
		placement.proposal();

	}

	//*****Documents*****

	@Test (priority=16)

	public static void signing_information() throws InterruptedException, IOException{
		//Create object of Documents page
		Documents documents = new Documents(driver);
		documents.other_documents();	

	}


}

*/