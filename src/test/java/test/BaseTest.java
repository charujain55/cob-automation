package test;

import org.apache.commons.csv.CSVRecord;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.BeforeClass;
import io.github.bonigarcia.wdm.WebDriverManager;
import se.nordfk.cob.pages.coblogin.CobLoginPage;
import utilities.CsvUtils;


public class BaseTest 
{
	protected ChromeDriver driver;
	private final String URL = "https://cob-advisor-nordfk-dev-1.nord-b5t-dev.nordfk.se";
	protected static CobLoginPage cobLoginPage;
	protected static CSVRecord record;

	@BeforeClass
    public void setUp () throws Exception {

		//Initialise Driver
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();

		//Maximize browser window
		driver.manage().window().maximize();

		//Go to COB application
		driver.get(URL);
		cobLoginPage = new CobLoginPage(driver);
		
		//Get csv data
		record = CsvUtils.getCSVTestData();
	}
}
