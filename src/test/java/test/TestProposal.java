package test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import se.nordfk.cob.annotation.dataProvider.DataProviderClass;
import se.nordfk.cob.pages.advisorassessment.AdvisorAssessment;
import se.nordfk.cob.pages.proposal.Proposal;

public class TestProposal extends BaseTest{

	static WebDriver driver;

	public static Proposal proposalInfo1() throws InterruptedException, IOException{
		AdvisorAssessment assess = TestAssessment.assessmentInformation1();
		Proposal proposal = assess.moveOnToNextPage();	
		proposal.productProposal(record.get("Admin_fee"), record.get("Relocation_fee"), record.get("Other_fee"));
		return proposal;
	}

	@Test(priority=15, dataProvider = "Proposal", dataProviderClass = DataProviderClass.class)
	public void proposalInfo(String adminfee, String relocationfee, String otherfee) throws InterruptedException, IOException{
		AdvisorAssessment assess = TestAssessment.assessmentInformation1();
		Proposal proposal = assess.moveOnToNextPage();	
		proposal.productProposal(adminfee, relocationfee, otherfee);
	}
}
