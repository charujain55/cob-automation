package test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import se.nordfk.cob.pages.placement.Placement;
import se.nordfk.cob.pages.proposal.Proposal;

public class TestPlacement extends BaseTest{

	static WebDriver driver;

	public static Placement placementInformation1() throws InterruptedException, IOException{
		Proposal proposal = TestProposal.proposalInfo1();
		Placement placement = proposal.moveOnToNextPage();		
		placement.proposal();
		return placement;
	}

	@Test(priority=16)
	public void placementInformation() throws InterruptedException, IOException{
		Proposal proposal = TestProposal.proposalInfo1();	
		Placement placement = proposal.moveOnToNextPage();		
		placement.proposal();
	}
}
