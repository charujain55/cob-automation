package test;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import se.nordfk.cob.annotation.dataProvider.DataProviderClass;
import se.nordfk.cob.pages.accountselection.AccountSelection;
import se.nordfk.cob.pages.council.Council;

public class TestAccountSelection extends BaseTest {

	static WebDriver driver;

	public static AccountSelection accountInformation1() throws InterruptedException, IOException{
		Council council = TestCouncil.meetingInformation1();
		AccountSelection account = council.moveOnToNextPage();
		account.accountSelect(record.get("Type_of_account"), record.get("Type_of_depot"), record.get("Power_of_attorny"), 
				record.get("Do_you_understand_risk_in_product_class_A"), record.get("Do_you_understand_risk_in_product_class_B"));
		return account;
	}

	@Test (priority=11, dataProvider = "AccountSelection", dataProviderClass = DataProviderClass.class)
	public void accountInformation(String typeOfAccount, String typeOfDepot, String powerOfAttorny, 
			String DoYouUnderstandRiskInProductClassA, String DoYouUnderstandRiskInProductClassB) 
					throws InterruptedException, IOException{
		Council council = TestCouncil.meetingInformation1();
		AccountSelection account = council.moveOnToNextPage();
		account.accountSelect(typeOfAccount, typeOfDepot, powerOfAttorny, DoYouUnderstandRiskInProductClassA, 
				DoYouUnderstandRiskInProductClassB);
	}
}
