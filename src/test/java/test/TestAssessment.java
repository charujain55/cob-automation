package test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import se.nordfk.cob.annotation.dataProvider.DataProviderClass;
import se.nordfk.cob.pages.advisorassessment.AdvisorAssessment;
import se.nordfk.cob.pages.placementproposal.PlacementProposal;

public class TestAssessment extends BaseTest {

	static WebDriver driver;

	public static AdvisorAssessment assessmentInformation1() throws InterruptedException, IOException{
		PlacementProposal placement = TestPlacementProposal.proposalInformation1();
		AdvisorAssessment advisorAssessment = placement.moveOnToNextPage();
		advisorAssessment.assessment(record.get("Is_customer_able_to_bear_risk"), record.get("Is_customer_judged_for_risk_knowledge"), 
				record.get("Is_customer_advised_against_placement"), record.get("Feedback_about_customer_investment"), 
				record.get("Feedback_discouragement"));
		return advisorAssessment;
	}

	@Test(priority=14, dataProvider = "AdvisorAssessment", dataProviderClass = DataProviderClass.class)
	public void assessmentInformation(String isCustomerAbleToBearRisk, String isCustomerJudgedForRiskKnowledge, 
			String isCustomerAdvisedAgainstPlacement, String feedbackAboutCustomerInvestment, 
			String feedbackDiscouragement) throws InterruptedException, IOException{
		PlacementProposal placement = TestPlacementProposal.proposalInformation1();
		AdvisorAssessment advisorAssessment = placement.moveOnToNextPage();
		advisorAssessment.assessment(isCustomerAbleToBearRisk, isCustomerJudgedForRiskKnowledge, isCustomerAdvisedAgainstPlacement, 
				feedbackAboutCustomerInvestment, feedbackDiscouragement);
	}
}
