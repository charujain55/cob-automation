package test;

import java.io.IOException;

import org.testng.annotations.Test;

import se.nordfk.cob.annotation.dataProvider.DataProviderClass;
import se.nordfk.cob.pages.advisorlogin.CustomerSsnLogin;
import se.nordfk.cob.pages.customerinfo.CustomerInfo;

public class TestCustomerInfo extends BaseTest
{
	public static CustomerInfo customerInformation1() throws InterruptedException, IOException
	{	
		CustomerSsnLogin ssnlogin = cobLoginPage.legitimeraButtonClick(record.get("Advisor_SSN"));
		ssnlogin.enterCustomerSSN(record.get("Customer_SSN"));
		CustomerInfo customerInfo = ssnlogin.moveOnToNextPage();
		customerInfo.fatcaButtonSelect();
		return customerInfo;
	}
	
	@Test(priority=2, dataProvider = "bankIDAdvisorSSN", dataProviderClass = DataProviderClass.class)
	public void customerInformation(String advisorSSN, String customerSSN) throws InterruptedException, IOException
	{
		CustomerSsnLogin ssnlogin = cobLoginPage.legitimeraButtonClick(advisorSSN);
		ssnlogin.enterCustomerSSN(customerSSN);
		CustomerInfo customerInfo = ssnlogin.moveOnToNextPage();
		customerInfo.fatcaButtonSelect();
	}
}