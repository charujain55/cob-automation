package se.nordfk.cob.annotation.dataProvider;
import org.testng.annotations.DataProvider;
import utilities.Constants;
import utilities.CsvUtils;

import java.io.IOException;

public class DataProviderClass {

	//creating object of ExcelUtils class
    static CsvUtils csvUtils = new CsvUtils();
	
    // use the data binding annotation DataProvider
    @DataProvider(name = "bankIDAdvisorSSN")
    public static Object[][] bankId() throws IOException {
        // call the class static methods getTestData, acquiring test data CSV file;
        return csvUtils.getTestData(Constants.Path_TestData+"bankIDAdvisorSSN.csv");
    }

    @DataProvider(name = "customerSSN")
    public static Object[][] customerLogin() throws IOException {
        // call the class static methods getTestData, acquiring test data CSV file;
        return csvUtils.getTestData(Constants.Path_TestData+"customerSSN.csv");
    }

    @DataProvider(name = "agreement")
    public static Object[][] agreement() throws IOException {
        // call the class static method getTestData, acquiring test data CSV file;
        return csvUtils.getTestData(Constants.Path_TestData+"meetingNotes.csv");
    }
    
    @DataProvider(name = "SavingProfile")
    public static Object[][] savingProfile() throws IOException {
        // call the class static method getTestData, acquiring test data CSV file;
        return csvUtils.getTestData(Constants.Path_TestData+"SavingProfile.csv");
    }

    @DataProvider(name = "Experience")
    public static Object[][] experience() throws IOException {
        // call the class static method getTestData, acquiring test data CSV file;
        return csvUtils.getTestData(Constants.Path_TestData+"Experience.csv");
    }

    @DataProvider(name = "Risk")
    public static Object[][] risk() throws IOException {
        // call the class static method getTestData, acquiring test data CSV file;
        return csvUtils.getTestData(Constants.Path_TestData+"Risk.csv");
    }

    @DataProvider(name = "RiskProfile")
    public static Object[][] riskProfile() throws IOException {
        // call the class static method getTestData, acquiring test data CSV file;
        return csvUtils.getTestData(Constants.Path_TestData+"RiskProfile.csv");
    }

    @DataProvider(name = "MeetingNotes")
    public static Object[][] meetingNotes() throws IOException {
        // call the class static method getTestData, acquiring test data CSV file;
        return csvUtils.getTestData(Constants.Path_TestData+"meetingNotes.csv");
    }

    @DataProvider(name = "AccountSelection")
    public static Object[][] accountSelection() throws IOException {
        // call the class static method getTestData, acquiring test data CSV file;
        return csvUtils.getTestData(Constants.Path_TestData+"AccountSelection.csv");
    }

    @DataProvider(name = "PreRegisteredAccount")
    public static Object[][] preRegisteredAccount() throws IOException {
        // call the class static method getTestData, acquiring test data CSV file;
        return csvUtils.getTestData(Constants.Path_TestData+"PreRegisteredAccount.csv");
    }

    @DataProvider(name = "PlacementProposal")
    public static Object[][] placementProposal() throws IOException {
        // call the class static method getTestData, acquiring test data CSV file;
        return csvUtils.getTestData(Constants.Path_TestData+"PlacementProposal.csv");
    }

    @DataProvider(name = "AdvisorAssessment")
    public static Object[][] advisorAssessment() throws IOException {
        // call the class static method getTestData, acquiring test data CSV file;
        return csvUtils.getTestData(Constants.Path_TestData+"AdvisorAssessment.csv");
    }

    @DataProvider(name = "Proposal")
    public static Object[][] proposal() throws IOException {
        // call the class static method getTestData, acquiring test data CSV file;
        return csvUtils.getTestData(Constants.Path_TestData+"Proposal.csv");
    }
}