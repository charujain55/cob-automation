package se.nordfk.cob.pages.customerinfo;

public interface CustomerInfoLocators {

	String locatorForFatcaButton = "//div[text()='Nej']";
	String moveOnButton = "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']";
}