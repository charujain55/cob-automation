package se.nordfk.cob.pages.customerinfo;



import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.nordfk.cob.pages.agreement.Agreement;
import se.nordfk.cob.pages.base.BasePage;


/*Refers to Basic customer information such as Name,Address,Postal address,
 * ZIP Code,City,Email,Contact.COB does not allow US person to proceed.
 */

public class CustomerInfo extends BasePage implements CustomerInfoLocators{
	WebDriverWait wait = new WebDriverWait(driver, 30);

	JavascriptExecutor executor = (JavascriptExecutor)driver;

	//calls super-driver and the constructor from BasePage class
	public CustomerInfo(WebDriver driver){
		super(driver);
	}


	By fatcaButton = By.xpath(locatorForFatcaButton);
	By gavidareButton = By.xpath(moveOnButton);


	//Method to select FATCA and move on

	public void fatcaButtonSelect(){
		WebElement fatcaNumber = wait.until(ExpectedConditions.visibilityOfElementLocated(fatcaButton));

		//Select NO for FATCA
		fatcaNumber.click();
	}
	
	public Agreement moveOnToNextPage()
	{
		//Click on Gå vidare button
		WebElement  moveOn = wait.until(ExpectedConditions.elementToBeClickable(gavidareButton));
		executor.executeScript("arguments[0].click()", moveOn);
		return new Agreement(driver);

	}		

}