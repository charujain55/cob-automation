package se.nordfk.cob.pages.base;

import org.openqa.selenium.WebDriver;

//Base Class
public class BasePage{

	public WebDriver driver;

	//constructor designed to initiate WebDriver and accept driver parameter
	public BasePage(WebDriver driver){

		this.driver = driver;
	}


}

