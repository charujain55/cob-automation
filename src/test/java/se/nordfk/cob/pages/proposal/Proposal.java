package se.nordfk.cob.pages.proposal;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.nordfk.cob.pages.base.BasePage;
import se.nordfk.cob.pages.placement.Placement;

//Product-related fees and reimbursements
public class Proposal extends BasePage implements ProposalLocators{
	WebDriverWait wait = new WebDriverWait(driver, 30);
	JavascriptExecutor executor = (JavascriptExecutor)driver;

	//calls super-driver and the constructor from BasePage class
	public Proposal(WebDriver driver){
		super(driver);
	}

	By titlePlacementProposal = By.xpath(placementProposal);
	By administrationFees = By.xpath(administrationFee);
	By relocationFees = By.xpath(relocationFee);
	By otherFees = By.xpath(otherFee);
	By gavidareButton = By.xpath(moveOnButton);


	//Method for providing fees
	public void productProposal(String adminfee, String relocationfee, String otherfee){

		//wait till page loads
		wait.until(ExpectedConditions.visibilityOfElementLocated(titlePlacementProposal));
		executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");

		WebElement fee1 = driver.findElement(administrationFees);
		//Clear 'Administrationsavgift' field
		fee1.sendKeys(Keys.BACK_SPACE);
		//Fill fee data
		fee1.sendKeys(adminfee);

		WebElement fee2 = driver.findElement(relocationFees);
		//Clear 'Flyttavgift' field
		fee2.sendKeys(Keys.BACK_SPACE);
		//Fill fee data
		fee2.sendKeys(relocationfee);

		WebElement fee3 = driver.findElement(otherFees);
		//Clear 'Övriga avgifter' field
		fee3.sendKeys(Keys.BACK_SPACE);
		//Fill fee data
		fee3.sendKeys(otherfee);
	}

	public Placement moveOnToNextPage(){

		//Click on Gavidare button
		WebElement moveOn = wait.until(ExpectedConditions.elementToBeClickable(gavidareButton));
		executor.executeScript("arguments[0].click()", moveOn);
		return new Placement(driver);
	}	

}
