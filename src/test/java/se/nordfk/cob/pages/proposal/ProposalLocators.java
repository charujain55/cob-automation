package se.nordfk.cob.pages.proposal;

public interface ProposalLocators {
	String placementProposal = "//h2[text()='Placeringsf\u00F6rslag']";
	String administrationFee = "//label[text()='Administrationsavgift']/following-sibling::input";
	String relocationFee = "//label[text()='Flyttavgift']/following-sibling::input";
	String otherFee = "//label[text()='\u00D6vriga avgifter']/following-sibling::input";
	String moveOnButton = "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']";
}
