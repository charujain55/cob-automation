package se.nordfk.cob.pages.coblogin;

import java.io.IOException;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.nordfk.cob.pages.advisorlogin.CustomerSsnLogin;
import se.nordfk.cob.pages.base.BasePage;

//COB person flow, create a new session

public class CobLoginPage extends BasePage implements CobloginLocators{
	WebDriverWait wait = new WebDriverWait(driver, 30);


	//calls super-driver and the constructor from BasePage class
	public CobLoginPage(WebDriver driver){
		super(driver);
	}

	
	By legitimeraButton = By.className(locatorForlegitimeraButton);
	By advisorSSN = By.className(locatorForAdvisorSsnField);
	By loginButton = By.className(locatorForRestart);

	//Method to click on legitimera button, enter advisor SSN and Login

	public CustomerSsnLogin legitimeraButtonClick(String ssn) throws InterruptedException, IOException{
		WebElement legitimera = wait.until(ExpectedConditions.visibilityOfElementLocated(legitimeraButton));

		//locate Legitimera button
		driver.findElement(legitimeraButton); 

		//click on Legitimera button
		legitimera.click();

		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		WebElement categoryBody = wait.until(ExpectedConditions.visibilityOfElementLocated(advisorSSN));

		//After locating Advisor SSN field provide Advisor SSN 
		categoryBody.sendKeys(ssn);

		// Click on Forts�tt button 
		driver.findElement(loginButton).click();

		//Click on 'Open BankID security application' button over alert
//		Runtime.getRuntime().exec("./src/main/resources/bank_id_alert.exe");
		Thread.sleep(10000);
		return new CustomerSsnLogin(driver);
	}

}

