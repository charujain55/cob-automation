package se.nordfk.cob.pages.coblogin;

public interface CobloginLocators{

	String locatorForlegitimeraButton = "Button-container";
	String locatorForAdvisorSsnField = "text";
	String locatorForRestart = "action-alternate";

}