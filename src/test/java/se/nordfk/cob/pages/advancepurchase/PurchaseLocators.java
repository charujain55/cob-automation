package se.nordfk.cob.pages.advancepurchase;

public interface PurchaseLocators{

	String moveOnButton = "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']";
	String arrangeLifeInsurance = "//div[text()='F\u00F6rmedla livf\u00F6rs\u00E4kringar']";
	String actsAsAgentForNordFondkommissionAB =
			"//div[contains(text(),'F\u00F6rmedlingsbolaget \u00E4r anknutet ombud till')]";
	String registrationRefersToFollowingInsuranceClasses = 
			"forkopsinfo_radgivbolag_forsakringsklasser";
	String advisorIsAffiliatedAgent = 
			"//div[text()='... \u00E4r anst\u00E4lld i ovanst\u00E5ende bolag (anknutet ombud)']";
	String performInvestmentAdvice = "//div[text()='Utf\u00F6ra investeringsr\u00E5dgivning']";
	String arrangeLifeInsurances = "(//div[text()='F\u00F6rmedla livf\u00F6rs\u00E4kringar'])[2]";
	String registrationRefersToInsuranceClasses = "forkopsinfo_forsformedl_forsklasser";
	String brokerageCompanyHasHoldingsInProductCompanies =
			"//div[text()='F\u00F6rmedlingsbolaget har kvalificerat innehav i produktbolag']";
	String enterProductCompany = "forkopsinfo_forsformedl_kvalinn_prodbolag";
	String securitiesCompaniesHaveHoldingsInBrokerageCompany =
			"//div[text()='V\u00E4rdepappersbolag har kvalificerat innehav i f\u00F6rmedlingsbolaget']";
	String enterSecuritiesCompanies = "forkopsinfo_vardepapperbolag_kvalinn_vardepapperbol";
	String productsAndInsuranceAreMediatedOnBehalfOfCompanies = 
			"//div[contains(text(), 'med vilka avtal')]";
	String agreementsConcludedWithFollowingCompanies = "forkopsinfo_investradgiv_avtal_bolag";
	String productsAndInsuranceAreMediatedOnBehalfOfCompaniesWithoutAgreement =
			"//div[contains(text(), 'utan avtal med produktbolagen')]" ;
	String insuranceProvidedWithoutAgreement = "forkopsinfo_investradgiv_avtal_forbolag";
	String companyWorksWithIndependentInvestmentAdvice = "//div[contains(text(), 'Bolaget arbetar med oberoende')]";
}
