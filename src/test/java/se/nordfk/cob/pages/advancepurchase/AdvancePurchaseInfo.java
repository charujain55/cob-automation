package se.nordfk.cob.pages.advancepurchase;



import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.nordfk.cob.pages.base.BasePage;
import se.nordfk.cob.pages.moneylaundering.MoneyLaundering;

/* Nord FK provides a range of financial services that include various activities.
 * Nord FK's guidelines for conflicts of interest describe the potential conflicts 
 * of interest that have been identified and how they should be handled and avoided 
 * in order to prevent customers' interests from being adversely affected. The 
 * employees must always take care of the customer's interests in the first place.
 * At the customer's request, Nord FK provides further information on the guidelines.
 */

public class AdvancePurchaseInfo extends BasePage implements PurchaseLocators{

	WebDriverWait wait = new WebDriverWait(driver, 30);    
	JavascriptExecutor executor = (JavascriptExecutor)driver;

	//calls super-driver and the constructor from BasePage class
	public AdvancePurchaseInfo(WebDriver driver){
		super(driver);
	}

	By arrangelifeInsurance = By.xpath(arrangeLifeInsurance);
	By agentOfNord = By.xpath(actsAsAgentForNordFondkommissionAB);
	By textdata1 = By.name(registrationRefersToFollowingInsuranceClasses);
	By affiliatedAgent = By.xpath(advisorIsAffiliatedAgent);
	By investmentAdvice = By.xpath(performInvestmentAdvice);
	By lifeInsurance = By.xpath(arrangeLifeInsurances);
	By textdata2 = By.name(registrationRefersToInsuranceClasses);
	By brokerageCompany = By.xpath(brokerageCompanyHasHoldingsInProductCompanies);
	By textdata3 = By.name(enterProductCompany);
	By securitiesCompanies = By.xpath(securitiesCompaniesHaveHoldingsInBrokerageCompany);
	By textdata4 = By.name(enterSecuritiesCompanies);
	By productsInsurance = By.xpath(productsAndInsuranceAreMediatedOnBehalfOfCompanies);
	By textdata5 = By.name(agreementsConcludedWithFollowingCompanies);
	By products =  By.xpath(productsAndInsuranceAreMediatedOnBehalfOfCompaniesWithoutAgreement);
	By textdata6 = By.name(insuranceProvidedWithoutAgreement);
	By independentInvestmentAdvice = By.xpath(companyWorksWithIndependentInvestmentAdvice);
	By gavidareButtton = By.xpath(moveOnButton);


	//Method to select Purchase info

	public void purchaseInfoCheckboxes(String insuranceClasses,String insuranceClass,
			String enterProductCompany,String enterSecuritiesCompanies,
			String agreementsConcludedWithCompanies, String insuranceProvidedWithoutAgreementToCompanies){
		
		WebElement arrangedLifeInsurance = wait.until(ExpectedConditions.visibilityOfElementLocated(arrangelifeInsurance));
		executor.executeScript("arguments[0].scrollIntoView();", arrangedLifeInsurance);

		//Click on checkbox 'F�rmedla livf�rs�kringar'
		arrangedLifeInsurance.click();

		//Click on checkbox 'F�rmedlingsbolaget �r anknutet ombud till...'
		driver.findElement(agentOfNord).click();

		//Fill data in textbox 'Registreringen avser endast f�ljande f�rs�kringsklasser'
		driver.findElement(textdata1).sendKeys(insuranceClasses);

		//Click on radio button '...�r anst�lld i ovanst�ende bolag (anknutet ombud)'
		driver.findElement(affiliatedAgent).click();

		/*Click on radio button '... har eget tillst�nd hos Finansinspektionen'
		driver.findElement(supervisoryAuthority).click();

		Click on checkbox 'F�rs�kringsf�rmedlingslicens saknas och r�dgivning 
		om f�rs�kringsl�sningar kan ej l�mnas'
		driver.findElement(brokerageLicense).click();*/

		//Click on checkbox 'Utf�ra investeringsr�dgivning'
		driver.findElement(investmentAdvice).click();

		//Click on checkbox 'F�rmedla livf�rs�kringar'
		driver.findElement(lifeInsurance).click();

		//Fill data in textbox 'Registreringen avser endast f�ljande f�rs�kringsklasser'
		driver.findElement(textdata2).sendKeys(insuranceClass);

		//Click on checkbox 'F�rmedlingsbolaget har kvalificerat innehav i produktbolag'
		driver.findElement(brokerageCompany).click();

		//Fill data in textbox 'Ange produktbolag'
		driver.findElement(textdata3).sendKeys(enterProductCompany);

		//Click on checkbox 'V�rdepappersbolag har kvalificerat innehav i f�rmedlingsbolaget'
		driver.findElement(securitiesCompanies).click();

		//Fill data in textbox 'Ange v�rdepappersbolag'
		driver.findElement(textdata4).sendKeys(enterSecuritiesCompanies);

		//Click on checkbox 'Produkter &/eller f�rs�kringar f�rmedlas uteslutande f�r...'
		WebElement withAgreementCompany = driver.findElement(productsInsurance);
		executor.executeScript("arguments[0].scrollIntoView();", withAgreementCompany);
		withAgreementCompany.click();

		//Fill data in textbox 'Avtal har slutits med f�ljande bolag'
		driver.findElement(textdata5).sendKeys(agreementsConcludedWithCompanies);

		//Click on checkbox 'Produkter &/eller f�rs�kringar f�rmedlas f�r ett eller flera...'
		driver.findElement(products).click();

		//Fill data in textbox 'F�rs�kringar f�rmedlas huvudsakligen utan avtal till f�ljande bolag'
		driver.findElement(textdata6).sendKeys(insuranceProvidedWithoutAgreementToCompanies);

		//Click on checkbox 'Bolaget arbetar med oberoende investeringsr�dgivning.'
		driver.findElement(independentInvestmentAdvice).click();
	}
	
	public MoneyLaundering moveOnToNextPage()
	{
		//Click on Gavidare button
		WebElement moveOn = wait.until(ExpectedConditions.elementToBeClickable(gavidareButtton));
		executor.executeScript("arguments[0].click()", moveOn);
		return new MoneyLaundering(driver);
	}

}
