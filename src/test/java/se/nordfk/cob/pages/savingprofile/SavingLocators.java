package se.nordfk.cob.pages.savingprofile;

public interface SavingLocators {
	String titleSavingProfilePage = "//h2[text()='Sparprofil']";
	String NumberOfChildren ="//label[text()='F\u00F6rs\u00F6rjningsplikt antal barn']/following-sibling::input";
	String martialStatusDropdown = "(//div[@class='Select-info'])[1]";
	String employmentTypeDropdown = "(//div[@class='Select-info'])[2]";
	String enterMonthlyIncome = "//label[text()='M\u00E5nadsinkomst efter skatt']/following-sibling::input";
	String enterMonthlyCost ="//label[text()='M\u00E5nadskostnad']/following-sibling::input" ;
	String enterRealEstateFunds = "//label[text()='Fastigheter']/following-sibling::input";
	String enterLiabilities = "//label[text()='Skulder']/following-sibling::input";
	
	String risk1Checkbox = "(//td//label/parent::div)[1]";
	String risk2Checkbox = "(//td//label/parent::div)[2]";
	String risk3Checkbox = "(//td//label/parent::div)[3]";
	String risk4Checkbox = "(//td//label/parent::div)[4]";
	String risk5Checkbox = "(//td//label/parent::div)[5]";
	String risk6Checkbox = "(//td//label/parent::div)[6]";
	String risk7Checkbox = "(//td//label/parent::div)[7]";

	String horizon1Risk1 = "(//td//input[@type='text'])[1]";
	String horizon2Risk1 = "(//td//input[@type='text'])[2]";
	String horizon3Risk1 = "(//td//input[@type='text'])[3]";
	String horizon4Risk1 = "(//td//input[@type='text'])[4]";
	String horizon1Risk2 = "(//td//input[@type='text'])[5]";
	String horizon2Risk2 = "(//td//input[@type='text'])[6]";
	String horizon3Risk2 = "(//td//input[@type='text'])[7]";
	String horizon4Risk2 = "(//td//input[@type='text'])[8]";
	String horizon1Risk3 = "(//td//input[@type='text'])[9]";
	String horizon2Risk3 = "(//td//input[@type='text'])[10]";
	String horizon3Risk3 = "(//td//input[@type='text'])[11]";
	String horizon4Risk3 = "(//td//input[@type='text'])[12]";
	String horizon1Risk4 = "(//td//input[@type='text'])[13]";
	String horizon2Risk4 = "(//td//input[@type='text'])[14]";
	String horizon3Risk4 = "(//td//input[@type='text'])[15]";
	String horizon4Risk4 = "(//td//input[@type='text'])[16]";
	String horizon1Risk5 = "(//td//input[@type='text'])[17]";
	String horizon2Risk5 = "(//td//input[@type='text'])[18]";
	String horizon3Risk5 = "(//td//input[@type='text'])[19]";
	String horizon4Risk5 = "(//td//input[@type='text'])[20]";
	String horizon1Risk6 = "(//td//input[@type='text'])[21]";
	String horizon2Risk6 = "(//td//input[@type='text'])[22]";
	String horizon3Risk6 = "(//td//input[@type='text'])[23]";
	String horizon4Risk6 = "(//td//input[@type='text'])[24]";
	String horizon1Risk7 = "(//td//input[@type='text'])[25]";
	String horizon2Risk7 = "(//td//input[@type='text'])[26]";
	String horizon3Risk7 = "(//td//input[@type='text'])[27]";
	String horizon4Risk7 = "(//td//input[@type='text'])[28]";
	
	String moveOnButton = "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']";
}