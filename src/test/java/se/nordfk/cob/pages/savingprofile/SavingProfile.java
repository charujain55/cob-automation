package se.nordfk.cob.pages.savingprofile;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.nordfk.cob.pages.base.BasePage;
import se.nordfk.cob.pages.experiencebackground.Experience;

import java.util.Random;

public class SavingProfile extends BasePage implements SavingLocators{
	WebDriverWait wait = new WebDriverWait(driver, 30);    
	JavascriptExecutor executor = (JavascriptExecutor)driver;
	Random randomNumbers = new Random();

	//calls super-driver and the constructor from BasePage class
	public SavingProfile(WebDriver driver){
		super(driver);
	}


	By titleSavingProfile = By.xpath(titleSavingProfilePage);
	By numberOfChildren = By.xpath(NumberOfChildren);
	By martialStatus = By.xpath(martialStatusDropdown);
	By employment = By.xpath(employmentTypeDropdown);
	By monthlyIncome = By.xpath(enterMonthlyIncome);
	By monthlyCost = By.xpath(enterMonthlyCost);
	By realEstate = By.xpath(enterRealEstateFunds);
	By liabilities = By.xpath(enterLiabilities);

	By riskCheckbox1 = By.xpath(risk1Checkbox);
	By riskCheckbox2 = By.xpath(risk2Checkbox);
	By riskCheckbox3 = By.xpath(risk3Checkbox);
	By riskCheckbox4 = By.xpath(risk4Checkbox);
	By riskCheckbox5 = By.xpath(risk5Checkbox);
	By riskCheckbox6 = By.xpath(risk6Checkbox);
	By riskCheckbox7 = By.xpath(risk7Checkbox);

	By horizonRisk1_1 = By.xpath(horizon1Risk1);
	By horizonRisk2_1 = By.xpath(horizon2Risk1);
	By horizonRisk3_1 = By.xpath(horizon3Risk1);
	By horizonRisk4_1 = By.xpath(horizon4Risk1);
	By horizonRisk1_2 = By.xpath(horizon1Risk2);
	By horizonRisk2_2 = By.xpath(horizon2Risk2);
	By horizonRisk3_2 = By.xpath(horizon3Risk2);
	By horizonRisk4_2 = By.xpath(horizon4Risk2);
	By horizonRisk1_3 = By.xpath(horizon1Risk3);
	By horizonRisk2_3 = By.xpath(horizon2Risk3);
	By horizonRisk3_3 = By.xpath(horizon3Risk3);
	By horizonRisk4_3 = By.xpath(horizon4Risk3);
	By horizonRisk1_4 = By.xpath(horizon1Risk4);
	By horizonRisk2_4 = By.xpath(horizon2Risk4);
	By horizonRisk3_4 = By.xpath(horizon3Risk4);
	By horizonRisk4_4 = By.xpath(horizon4Risk4);
	By horizonRisk1_5 = By.xpath(horizon1Risk5);
	By horizonRisk2_5 = By.xpath(horizon2Risk5);
	By horizonRisk3_5 = By.xpath(horizon3Risk5);
	By horizonRisk4_5 = By.xpath(horizon4Risk5);
	By horizonRisk1_6 = By.xpath(horizon1Risk6);
	By horizonRisk2_6 = By.xpath(horizon2Risk6);
	By horizonRisk3_6 = By.xpath(horizon3Risk6);
	By horizonRisk4_6 = By.xpath(horizon4Risk6);
	By horizonRisk1_7 = By.xpath(horizon1Risk7);
	By horizonRisk2_7 = By.xpath(horizon2Risk7);
	By horizonRisk3_7 = By.xpath(horizon3Risk7);
	By horizonRisk4_7 = By.xpath(horizon4Risk7);

	By gavidareButton = By.xpath(moveOnButton);


	//Method to fill personal info & investment

	public void customerProfile(String noOfChildren, String statusMarital, String typeEmployment, 
			String monthlyIncomeAmount, String monthlyCostAmount, String realEstateAmount, String liability){
		//wait until page is loaded
		wait.until(ExpectedConditions.visibilityOfElementLocated(titleSavingProfile));

		//Send value for no of children
		driver.findElement(numberOfChildren).sendKeys(noOfChildren);;

		//Select martial status from dropdown
		driver.findElement(martialStatus).click();
		driver.findElement(By.xpath("//div[@title='"+statusMarital+"']")).click();

		//Select type of employment from dropdown
		driver.findElement(employment).click();
		driver.findElement(By.xpath("//div[@title='"+typeEmployment+"']")).click();

		//Provide monthly income
		driver.findElement(monthlyIncome).sendKeys(monthlyIncomeAmount);
		//Provide monthly cost
		driver.findElement(monthlyCost).sendKeys(monthlyCostAmount);
		//Provide investment in real estate
		driver.findElement(realEstate).sendKeys(realEstateAmount);
		//Provide investment in liabilities
		driver.findElement(liabilities).sendKeys(liability);

	}


	//Method to enter customer's existing savings

	public void existingSavings(String risk1horizon1, String risk1horizon2, String risk1horizon3, String risk1horizon4, 
			String risk2horizon1, String risk2horizon2, String risk2horizon3, String risk2horizon4, 
			String risk3horizon1, String risk3horizon2, String risk3horizon3, String risk3horizon4, 
			String risk4horizon1, String risk4horizon2, String risk4horizon3, String risk4horizon4, 
			String risk5horizon1, String risk5horizon2, String risk5horizon3, String risk5horizon4, 
			String risk6horizon1, String risk6horizon2, String risk6horizon3, String risk6horizon4, 
			String risk7horizon1, String risk7horizon2, String risk7horizon3, String risk7horizon4){
		// Provide investment in risk class 1 at all 4 horizons
		WebElement riskCheckbox_1 = driver.findElement(riskCheckbox1);
//		riskCheckbox_1.click();
		String classNameCheckbox1 = riskCheckbox_1.getAttribute("class");
		// if 'No investment' checkbox is not checked then provide investment at all 4 horizons
		if(!classNameCheckbox1.contains("checked")) {
			driver.findElement(horizonRisk1_1).sendKeys(risk1horizon1);
			driver.findElement(horizonRisk2_1).sendKeys(risk1horizon2);
			driver.findElement(horizonRisk3_1).sendKeys(risk1horizon3);
			driver.findElement(horizonRisk4_1).sendKeys(risk1horizon4);
		}
		// Provide investment in risk class 2 at all 4 horizons
		WebElement riskCheckbox_2 = driver.findElement(riskCheckbox2);
		riskCheckbox_2.click();
		String classNameCheckbox2 = riskCheckbox_2.getAttribute("class");
		// if 'No investment' checkbox is not checked then provide investment at all 4 horizons
		if(!classNameCheckbox2.contains("checked")) {
			driver.findElement(horizonRisk1_2).sendKeys(risk2horizon1);
			driver.findElement(horizonRisk2_2).sendKeys(risk2horizon2);
			driver.findElement(horizonRisk3_2).sendKeys(risk2horizon3);
			driver.findElement(horizonRisk4_2).sendKeys(risk2horizon4);
		}
		// Provide investment in risk class 3 at all 4 horizons
		WebElement riskCheckbox_3 = driver.findElement(riskCheckbox3);
		riskCheckbox_3.click();
		String classNameCheckbox3 = riskCheckbox_3.getAttribute("class");
		// if 'No investment' checkbox is not checked then provide investment at all 4 horizons
		if(!classNameCheckbox3.contains("checked")){
			driver.findElement(horizonRisk1_3).sendKeys(risk3horizon1);
			driver.findElement(horizonRisk2_3).sendKeys(risk3horizon2);
			driver.findElement(horizonRisk3_3).sendKeys(risk3horizon3);
			driver.findElement(horizonRisk4_3).sendKeys(risk3horizon4);
		}
		// Provide investment in risk class 4 at all 4 horizons
		WebElement riskCheckbox_4 = driver.findElement(riskCheckbox4);
		riskCheckbox_4.click();
		String classNameCheckbox4 = riskCheckbox_4.getAttribute("class");
		// if 'No investment' checkbox is not checked then provide investment at all 4 horizons
		if(!classNameCheckbox4.contains("checked")) {
			driver.findElement(horizonRisk1_4).sendKeys(risk4horizon1);
			driver.findElement(horizonRisk2_4).sendKeys(risk4horizon2);
			driver.findElement(horizonRisk3_4).sendKeys(risk4horizon3);
			driver.findElement(horizonRisk4_4).sendKeys(risk4horizon4);
		}
		// Provide investment in risk class 5 at all 4 horizons
		WebElement riskCheckbox_5 = driver.findElement(riskCheckbox5);
//		riskCheckbox_5.click();
		String classNameCheckbox5 = riskCheckbox_5.getAttribute("class");
		// if 'No investment' checkbox is not checked then provide investment at all 4 horizons
		if(!classNameCheckbox5.contains("checked")) {
			driver.findElement(horizonRisk1_5).sendKeys(risk5horizon1);
			driver.findElement(horizonRisk2_5).sendKeys(risk5horizon2);
			driver.findElement(horizonRisk3_5).sendKeys(risk5horizon3);
			driver.findElement(horizonRisk4_5).sendKeys(risk5horizon4);
		}
		// Provide investment in risk class 6 at all 4 horizons
		WebElement riskCheckbox_6 = driver.findElement(riskCheckbox6);
		riskCheckbox_6.click();
		String classNameCheckbox6 = riskCheckbox_6.getAttribute("class");
		// if 'No investment' checkbox is not checked then provide investment at all 4 horizons
		if(!classNameCheckbox6.contains("checked")) {
			driver.findElement(horizonRisk1_6).sendKeys(risk6horizon1);
			driver.findElement(horizonRisk2_6).sendKeys(risk6horizon2);
			driver.findElement(horizonRisk3_6).sendKeys(risk6horizon3);
			driver.findElement(horizonRisk4_6).sendKeys(risk6horizon4);
		}
		// Provide investment in risk class 7 at all 4 horizons
		WebElement riskCheckbox_7 = driver.findElement(riskCheckbox7);
//		riskCheckbox_7.click();
		String classNameCheckbox7 = riskCheckbox_7.getAttribute("class");
		// if 'No investment' checkbox is not checked then provide investment at all 4 horizons
		if(!classNameCheckbox7.contains("checked")) {
			driver.findElement(horizonRisk1_7).sendKeys(risk7horizon1);
			driver.findElement(horizonRisk2_7).sendKeys(risk7horizon2);
			driver.findElement(horizonRisk3_7).sendKeys(risk7horizon3);
			driver.findElement(horizonRisk4_7).sendKeys(risk7horizon4);
		}

		/*List<WebElement> investment = driver.findElements(horizons);
		for (WebElement i:investment){
			int randomInvestment =randomNumbers.nextInt(999999);
			i.sendKeys(String.valueOf(randomInvestment));
		}*/
	}

	public Experience moveOnToNextPage(){

		//Click on Gavidare button
		WebElement moveOn = wait.until(ExpectedConditions.elementToBeClickable(gavidareButton));
		executor.executeScript("arguments[0].click();", moveOn);
		return new Experience(driver);
	}

}     

