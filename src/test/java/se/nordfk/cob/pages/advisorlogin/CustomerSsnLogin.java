package se.nordfk.cob.pages.advisorlogin;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.nordfk.cob.pages.base.BasePage;
import se.nordfk.cob.pages.customerinfo.CustomerInfo;


//Page1-Enter the customer's personal

public class CustomerSsnLogin extends BasePage implements CustomerSsnLocators {
	WebDriverWait wait = new WebDriverWait(driver, 30);
    

	//calls super-driver and the constructor from BasePage class
	public CustomerSsnLogin(WebDriver driver){
		super(driver);
	}

	By person = By.className(locatorForPersonNumber);
	By newSession = By.xpath(locatorForSelectingNewSession);
	By gavidareButton = By.xpath(moveOnButton);


	//Method to enter Customer SSN, select new session & move on


	public void enterCustomerSSN(String ssnNumber) {
		WebElement personnummer = wait.until(ExpectedConditions.visibilityOfElementLocated(person));

		// Provide Person SSN
		personnummer.sendKeys(ssnNumber);

		WebElement newSession1 = wait.until(ExpectedConditions.visibilityOfElementLocated(newSession));

		// Start New Session
		newSession1.click();
	}
		public CustomerInfo moveOnToNextPage()
		{
			
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		WebElement getStarted = wait.until(ExpectedConditions.elementToBeClickable(gavidareButton));

		// Click on P�b�rja button
		executor.executeScript("arguments[0].click()", getStarted);
		
		return new CustomerInfo(driver);
	}

}
