package se.nordfk.cob.pages.advisorlogin;

public interface CustomerSsnLocators {

	String locatorForPersonNumber = "TextField-input";
	String locatorForSelectingNewSession = "//div[text()='Skapa ett nytt r\u00E5dgivningstillf\u00E4lle']";
	String moveOnButton = "//div[@class='Button-container']//following-sibling::div[.='P\u00E5b\u00F6rja']";

}
