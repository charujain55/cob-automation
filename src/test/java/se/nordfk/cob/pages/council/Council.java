package se.nordfk.cob.pages.council;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.nordfk.cob.pages.accountselection.AccountSelection;
import se.nordfk.cob.pages.base.BasePage;

//Information about the product / portfolio, background and conditions
public class Council extends BasePage implements CouncilLocators{

	WebDriverWait wait = new WebDriverWait(driver, 30);
	JavascriptExecutor executor = (JavascriptExecutor)driver;
    
	//calls super-driver and the constructor from BasePage class
	public Council(WebDriver driver){
		super(driver);
	}

	By councils =  By.xpath(council);
	By gavidareButton = By.xpath(moveOnButton);
	By backgroundAndConditions  = By.xpath(backgroundAndConditionTextArea);
	By thingsDiscussedInMeetings = By.name(thingsDiscussedInMeetingTextArea);
	By informationAboutProducts = By.name(informationAboutProductTextArea);
	By thirdPartyCompensations = By.name(thirdPartyCompensationTextArea);
	By otherReason = By.name(othersTextArea);

	//Method to enter Meeting Notes

	public void meetingNotes(String backgroundAndCondition, String thingsDiscussedInMeeting, String informationAboutPortfolio, 
			String thirdPartyCompensation, String other) throws InterruptedException{
		wait.until(ExpectedConditions.visibilityOfElementLocated(councils));

		//Details of background and conditions
		driver.findElement(backgroundAndConditions).sendKeys(backgroundAndCondition);

		//Things that have been discussed during the meeting
		driver.findElement(thingsDiscussedInMeetings).sendKeys(thingsDiscussedInMeeting);

		//Information about the product / portfolio
		driver.findElement(informationAboutProducts).sendKeys(informationAboutPortfolio);

		//Motivate any third-party compensation to the adviser 
		driver.findElement(thirdPartyCompensations).sendKeys(thirdPartyCompensation);

		//Any conflicts of interest / other issues taken into account
		driver.findElement(otherReason).sendKeys(other);

	}
	
	public AccountSelection moveOnToNextPage()
	{
		//Click on Gavidare button
		WebElement moveOn = wait.until(ExpectedConditions.elementToBeClickable(gavidareButton));
		executor.executeScript("arguments[0].click();", moveOn);
		return new AccountSelection(driver);
	}	
}
