package se.nordfk.cob.pages.council;

public interface CouncilLocators {
	String council ="//h2[text()='R\u00E5det']";
	String moveOnButton = "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']";
	String backgroundAndConditionTextArea = "//textarea[@name='advice_notes']";
	String thingsDiscussedInMeetingTextArea = "advice_notes2";
	String informationAboutProductTextArea ="advice_notes3";
	String thirdPartyCompensationTextArea = "advice_notes4";
	String othersTextArea = "advice_notes5";
}
