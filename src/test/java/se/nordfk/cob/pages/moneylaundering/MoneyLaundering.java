package se.nordfk.cob.pages.moneylaundering;


import java.util.List;
import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.nordfk.cob.pages.base.BasePage;
import se.nordfk.cob.pages.savingprofile.SavingProfile;

//Issues under the Money Laundering Act
public class MoneyLaundering extends BasePage implements MoneyLaunderingLocators{

	WebDriverWait wait = new WebDriverWait(driver, 30);    
	JavascriptExecutor executor = (JavascriptExecutor)driver;
	public Random rand;

	//calls super-driver and the constructor from BasePage class
	public MoneyLaundering (WebDriver driver){
		super(driver);
	}

	//primary reason for a business relationship with Nord FK
		By savings = By.xpath(saving);
		By assetsManagement = By.xpath(assetManagement);
		By others1 = By.xpath(primaryReasonForBusinessRelationship);
		By textdata1 = By.xpath(provideDescription);

		// purpose of your or your company's investment
		By pension  = By.xpath(purposeOfYourCompanyInvestmentPension);
		By privateConsumption = By.xpath(purposeOfYourCompanyInvestmentPrivateConsumption);
		By financialSecurity = By.xpath(purposeOfYourCompanyInvestmentFinancialSecurity);
		By toRelatives = By.xpath(purposeOfYourCompanyInvestmentToRelatives);
		By beneficiary = By.xpath(purposeOfYourCompanyInvestmentBeneficiary);
		By placement = By.xpath(purposeOfYourCompanyInvestmentPlacement);
		By others2 = By.xpath(purposeOfYourCompanyInvestment);
		By textdata2 = By.xpath(provideDescription1);

		//On whose behalf are you investing
		By companyOwned = By.xpath(companyWhollyOwned);
		By byProxy = By.xpath(forSomeoneElseByProxy);
		By others3 = By.xpath(whosBehalfYouAreInvesting);
		By textdata3 = By.xpath(provideDescription2);

		//purpose of investing in financial instruments 
		By purposeOfInvestment = By.xpath(purposeOfInvesting);
		By textdata4 = By.xpath(provideDescription3);

		//How much have you / the company you represent invested in securities
		By investedInSecurities = By.xpath(investmentInSecurities);

		//Where does the capital you invest mainly come from?
		By savings1 = By.xpath(savingsCapital);
		By inheritance =  By.xpath(inheritedCapital);
		By salary = By.xpath(salaryCapital);
		By realEstate = By.xpath(realestateCapital);
		By returnsOnCapital = By.xpath(returnOnCapital);
		By others4 = By.xpath(sourceOfCapital);
		By textdata5 = By.xpath(provideDescription4);

		//From which bank / insurance company do you take capital for investments
		By swedishBanks = By.xpath(swedishBank);
		By swedishInsurances = By.xpath(swedishInsurance);
		By foreignBanks = By.xpath(foreignBank);
		By textdata6 = By.xpath(provideDescription5);
		By foreignInsuranceCompanies = By.xpath(foreignInsuranceCompany);
		By textdata7 = By.xpath(provideDescription6);

		//How large individual transactions in financial instruments do you make?
		By individualTransactions = By.className(transactionsInFinancialInstruments);
		By transactionOption = By.className(transactionOptions);

		//Family member or a close associate have political or public position in Sweden or abroad
		By couplings = By.xpath(coupling);
		By personName = By.xpath(name);
		By personPosition = By.xpath(position);
		By personCountry = By.className(country);
		By selectCountries = By.xpath(countries);
		By argentina = By.xpath(countryOptions);
		By timePeriod = By.xpath(period);
		By gavidareButton = By.xpath(moveOnButton);

	//Method to select Money Laundering options

		public void moneyLaundering(String description1, String description2, String description3, 
				String description4, String enterAmount,String description6, String foreignBankNames, 
				String foreignInsuranceCompanynames,String coupling1, String name1,String position1, 
				String countries1, String period1  ){

			WebElement save = wait.until(ExpectedConditions.visibilityOfElementLocated(savings));
			//Click on checkbox 'Sparande'
			save.click();

			//Click on checkbox 'Kapitalf�rvaltning'
			driver.findElement(assetsManagement).click();

			//Click on checkbox 'Annat (beskriv)'
			driver.findElement(others1).click();

			//Fill data in textbox 'V�nligen beskriv'
			driver.findElement(textdata1).sendKeys(description1);

			//Click on checkbox 'Pension'
			driver.findElement(pension).click();

			//Click on checkbox 'Privat konsumtion'
			driver.findElement(privateConsumption).click();

			//Click on checkbox 'Ekonomisk trygghet'
			driver.findElement(financialSecurity).click();

			//Click on checkbox 'Till n�rst�ende'
			driver.findElement(toRelatives).click();

			//Click on checkbox 'Jag �r f�rm�nstagare'
			driver.findElement(beneficiary).click();

			//Click on checkbox 'Sparande/placering'
			driver.findElement(placement).click();

			//Click on checkbox 'Annat (beskriv)'
			driver.findElement(others2).click();

			//Fill data in textbox 'V�nligen beskriv'
			driver.findElement(textdata2).sendKeys(description2);

			//Click on checkbox 'F�r egen del (eller av mig hel�gt f�retag)'
			driver.findElement(companyOwned).click();

			//Click on checkbox 'F�r n�gon annan genom fullmakt'
			driver.findElement(byProxy).click();

			//Click on checkbox 'Annat (beskriv)'
			driver.findElement(others3).click();

			//Fill data in textbox 'V�nligen beskriv'
			driver.findElement(textdata3).sendKeys(description3);

			//Select yes for '�r syftet med att investera i finansiella instrument...?'
			driver.findElement(purposeOfInvestment ).click();
			WebElement describeTextbox = wait.until(ExpectedConditions.visibilityOfElementLocated(textdata4));
			
			//Fill data in textbox 'V�nligen beskriv'
			describeTextbox.sendKeys(description4);

			//Fill data in textbox 'Ange belopp'
			driver.findElement(investedInSecurities ).sendKeys(enterAmount);

			//Click on checkbox 'Sparande'
			driver.findElement(savings1).click();

			//Click on checkbox 'Arv/g�va'
			driver.findElement(inheritance).click();

			//Click on checkbox 'L�n/pension'
			driver.findElement(salary).click();

			//Click on checkbox 'Fastighetsf�rs�ljning'
			driver.findElement(realEstate).click();

			//Click on checkbox 'Kapitalavkastning'
			driver.findElement(returnsOnCapital).click();

			//Click on checkbox 'Annat (beskriv)'
			driver.findElement(others4).click();

			//Fill data in textbox 'V�nligen beskriv'
			driver.findElement(textdata5).sendKeys(description6);

			//Click on checkbox 'Svensk bank'
			driver.findElement(swedishBanks).click();

			//Click on checkbox 'Svenskt f�rs�kringsbolag'
			driver.findElement(swedishInsurances).click();

			//Click on checkbox 'Utl�ndsk bank'
			driver.findElement(foreignBanks).click();

			//Fill data in textbox 'Ange utl�ndsk bank'
			driver.findElement(textdata6).sendKeys(foreignBankNames);

			//Click on checkbox 'Utl�ndskt f�rs�kringsbolag'
			driver.findElement(foreignInsuranceCompanies).click();

			//Fill data in textbox 'Ange utl�ndskt f�rs�kringsbolag'
			driver.findElement(textdata7).sendKeys(foreignInsuranceCompanynames);

			//Select from drop-down for individual transactions in financial instruments
			WebElement salaryDropdown = driver.findElement(individualTransactions);
			executor.executeScript("arguments[0].scrollIntoView()", salaryDropdown);

			//Click on drop-down option for individual transactions
			salaryDropdown.click();

			List<WebElement> salaryTransaction = driver.findElements(transactionOption);
			rand = new Random();
			int salary = rand.nextInt(salaryTransaction.size());
			salaryTransaction.get(salary).click();

			/* Select yes if close family member or a associate had high political or 
			 * other public position in Sweden or abroad,*/

			driver.findElement(By.xpath("(//div[text()='Ja'])[2]")).click();
			WebElement textbox3 = wait.until(ExpectedConditions.visibilityOfElementLocated(couplings));
			textbox3.sendKeys(coupling1);

			//Enter Name
			driver.findElement(personName).sendKeys(name1);

			//Enter position held
			driver.findElement(personPosition).sendKeys(position1);

			//Select Country from drop-down options
			driver.findElement(personCountry).click();
			driver.findElement(selectCountries).sendKeys(countries1);

			WebElement personcountry = wait.until(ExpectedConditions.visibilityOfElementLocated(argentina));
			personcountry.click();

			//Enter Period
			driver.findElement(timePeriod).sendKeys(period1);
		
	}
	
	public SavingProfile moveOnToNextPage(){
		WebElement moveOn = wait.until(ExpectedConditions.elementToBeClickable(gavidareButton));
		executor.executeScript("arguments[0].click();", moveOn);
		return new SavingProfile(driver);
	}

}
