package se.nordfk.cob.pages.moneylaundering;

public interface MoneyLaunderingLocators {
	String saving = "//div[text()='Sparande']";
	String assetManagement ="//div[text()='Kapitalf\u00F6rvaltning']";
	String primaryReasonForBusinessRelationship = "//div[text()='Annat (beskriv)']";
	String provideDescription = "(//input[@class='TextField-input'])[1]";
	String purposeOfYourCompanyInvestmentPension = "//div[text()='Pension']";
	String purposeOfYourCompanyInvestmentPrivateConsumption = "//div[text()='Privat konsumtion']";
	String purposeOfYourCompanyInvestmentFinancialSecurity = "//div[text()='Ekonomisk trygghet']";
	String purposeOfYourCompanyInvestmentToRelatives = "//div[text()='Till n\u00E4rst\u00E5ende']";
	String purposeOfYourCompanyInvestmentBeneficiary = "//div[text()='Jag \u00E4r f\u00F6rm\u00E5nstagare']";
	String purposeOfYourCompanyInvestmentPlacement = "//div[text()='Sparande/placering']";
	String purposeOfYourCompanyInvestment = "(//div[text()='Annat (beskriv)'])[2]";
	String provideDescription1 = "(//input[@class='TextField-input'])[2]";
	String companyWhollyOwned = "//div[text()='F\u00F6r egen del (eller av mig hel\u00E4gt f\u00F6retag)']";
	String forSomeoneElseByProxy = "//div[text()='F\u00F6r n\u00E5gon annan genom fullmakt']";
	String whosBehalfYouAreInvesting = "(//div[text()='Annat (beskriv)'])[3]";
	String provideDescription2 = "(//input[@class='TextField-input'])[3]";
	String purposeOfInvesting = "//div[text()='Ja']";
	String provideDescription3 = "(//input[@class='TextField-input'])[4]";
	String investmentInSecurities = "(//input[@class='TextField-input'])[5]";
	String savingsCapital = "(//div[text()='Sparande'])[2]";
	String inheritedCapital = "//div[text()='Arv/g\u00E5va']";
	String salaryCapital = "//div[text()='L\u00F6n/pension']";
	String realestateCapital = "//div[text()='Fastighetsf\u00F6rs\u00E4ljning']";
	String returnOnCapital = "//div[text()='Kapitalavkastning']";
	String sourceOfCapital = "(//div[text()='Annat (beskriv)'])[4]";
	String provideDescription4 = "(//input[@class='TextField-input'])[6]";
	String swedishBank = "//div[text()='Svensk bank']";
	String swedishInsurance = "//div[text()='Svenskt f\u00F6rs\u00E4kringsbolag']";
	String foreignBank = "//div[text()='Utl\u00E4ndsk bank']";
	String provideDescription5 = "(//input[@class='TextField-input'])[7]";
	String foreignInsuranceCompany = "//div[text()='Utl\u00E4ndskt f\u00F6rs\u00E4kringsbolag']";
	String provideDescription6 = "(//input[@class='TextField-input'])[8]";
	String transactionsInFinancialInstruments = "Select-info";
	String transactionOptions = "Select-dropdown-option";
	String coupling = "//label[text()='Koppling']/following-sibling::input";
	String name = "//label[text()='Namn']/following-sibling::input";
	String position = "//label[text()='Befattning']/following-sibling::input";
	String country = "Select-info";
	String countries = "//input[@placeholder='S\u00F6k']";
	String countryOptions = "//div[text()='Argentina']";
	String period = "//label[text()='Period']/following-sibling::input";
	String moveOnButton = "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']";

}
