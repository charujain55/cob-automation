package se.nordfk.cob.pages.experiencebackground;

public interface ExperienceLocators {
	String experienceBackgroundPage = "//h2[text()='Erfarenhetsbakgrund']";
	String beenTradingInSecurities = "Cob-SimpleTextField-input";
	String checkboxProductRiskUnderstanding ="(//div[@class='Checkbox-checkbox'])[3]";
	String investmentsSharesListed = "(//td//input[@type='number'])[1]";
	String investmentsSharesUnListed = "(//td//input[@type='number'])[2]";
	String investmentsFonderUCITS = "(//td//input[@type='number'])[3]";
	String investmentsFundAIF= "(//td//input[@type='number'])[4]";
	String investmentsBondsListed = "(//td//input[@type='number'])[5]";
	String investmentsBondsUnListed = "(//td//input[@type='number'])[6]";
	String investmentsStockIndexBonds = "(//td//input[@type='number'])[7]";
	String investmentsSprinter = "(//td//input[@type='number'])[8]";
	String investmentsCreditCertificate = "(//td//input[@type='number'])[9]";
	String investmentsAutocalls = "(//td//input[@type='number'])[10]";
	String investmentsDerivatives = "(//td//input[@type='number'])[11]";
	String investments = "//td//input[@type='number']";
	String customerHasBeenTrainedCheckbox = "//td[4]//div[@class='Checkbox-checkbox']";
	String moveOnButton = "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']";
}

