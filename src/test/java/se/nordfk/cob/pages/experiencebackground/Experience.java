package se.nordfk.cob.pages.experiencebackground;


import java.util.List;
import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.nordfk.cob.pages.base.BasePage;
import se.nordfk.cob.pages.risk.Risk;

/*Description:-Education and an ongoing or previous profession 
 * that has led to knowledge of how financial investments and 
 * markets work?
 */
public class Experience extends BasePage implements ExperienceLocators{

	WebDriverWait wait = new WebDriverWait(driver, 30);    
	JavascriptExecutor executor = (JavascriptExecutor)driver;
	Random randomNumber = new Random();


	//calls super-driver and the constructor from BasePage class
	public Experience(WebDriver driver){
		super(driver);
	}


	By experienceBackgroundTitle = By.xpath(experienceBackgroundPage);
	By tradingInSecurities = By.className(beenTradingInSecurities);
	By productRiskUnderstandingCheckbox = By.xpath(checkboxProductRiskUnderstanding);
	By SharesListed = By.xpath(investmentsSharesListed);
	By SharesUnListed = By.xpath(investmentsSharesUnListed);
	By FonderUCITS = By.xpath(investmentsFonderUCITS);
	By FundAIF= By.xpath(investmentsFundAIF);
	By BondsListed = By.xpath(investmentsBondsListed);
	By BondsUnListed = By.xpath(investmentsBondsUnListed);
	By StockIndexBonds = By.xpath(investmentsStockIndexBonds);
	By Sprinter = By.xpath(investmentsSprinter);
	By CreditCertificate = By.xpath(investmentsCreditCertificate);
	By Autocalls = By.xpath(investmentsAutocalls);
	By Derivatives = By.xpath(investmentsDerivatives);
	By investment = By.xpath(investments);
	By customerHasBeenTrained = By.xpath(customerHasBeenTrainedCheckbox);
	By gavidareButton = By.xpath(moveOnButton);

	//Method for Experience background

	public void experience(String radioButtonSelection, String tradingNoOfYears){
		//Wait until page loads
		wait.until(ExpectedConditions.visibilityOfElementLocated(experienceBackgroundTitle));

		//select YES/NO for existing knowledge of financial investments
		WebElement radioButton = driver.findElement(By.xpath("//div[@class='Checkbox-label' and text()='"+radioButtonSelection+"']"));
		radioButton.click();
		//Enter years of trading experience
		driver.findElement(tradingInSecurities).sendKeys(tradingNoOfYears);

		//Select checkbox that the customer has understood the risks of all products
		driver.findElement(productRiskUnderstandingCheckbox).click();
		executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}


	//Method for Experience in trading

	public void tradingExperience(String noOfSharesListed, String noOfSharesUnListed, String noOfFonderUCITS, 
			String noOfFundAIF, String noOfBondsListed, String noOfBondsUnListed, String noOfStockIndexBonds, 
			String noOfSprinter, String noOfCreditCertificate, String noOfAutocalls, String noOfDerivatives){
		
		//Fill number of transactions in the last 3 years
		driver.findElement(SharesListed).sendKeys(noOfSharesListed);
		driver.findElement(SharesUnListed).sendKeys(noOfSharesUnListed);
		driver.findElement(FonderUCITS).sendKeys(noOfFonderUCITS);
		driver.findElement(FundAIF).sendKeys(noOfFundAIF);
		driver.findElement(BondsListed).sendKeys(noOfBondsListed);
		driver.findElement(BondsUnListed).sendKeys(noOfBondsUnListed);
		driver.findElement(StockIndexBonds).sendKeys(noOfStockIndexBonds);
		driver.findElement(Sprinter).sendKeys(noOfSprinter);
		driver.findElement(CreditCertificate).sendKeys(noOfCreditCertificate);
		driver.findElement(Autocalls).sendKeys(noOfAutocalls);
		driver.findElement(Derivatives).sendKeys(noOfDerivatives);
		
		//Select level of experience from dropdown ingen = 0, experienceBasic >0<=10, experienceInformed >10<=15, Avancerad>15
		List<WebElement> textFields = driver.findElements(investment);
		int j = 1;
		for (WebElement i:textFields){
			//No. of years in trading (shares)
			Integer yearsInTradingShares = Integer.valueOf(i.getAttribute("value"));
			if(yearsInTradingShares > 0 && yearsInTradingShares <= 10) {
				WebElement experienceDropdown = driver.findElement(By.xpath("(//div[@class='Select-info'])[" + j + "]"));
				executor.executeScript("arguments[0].scrollIntoView(true);", experienceDropdown);
				experienceDropdown.click();

				//CLick on experienceBasic if years in Trading are > 0 <= 10
				WebElement experienceBasic = wait.until(ExpectedConditions.visibilityOfElementLocated
						(By.xpath("(//div[@class='Select-dropdown']//div[text()='Grundl\u00E4ggande'])[" + j + "]")));
				experienceBasic.click();
			}
			else if(yearsInTradingShares > 10 && yearsInTradingShares < 15) {
				WebElement experienceDropdown = driver.findElement(By.xpath("(//div[@class='Select-info'])[" + j + "]"));
				executor.executeScript("arguments[0].scrollIntoView(true);", experienceDropdown);
				experienceDropdown.click();

				//CLick on experienceInformed if years in Trading are > 10 < 15
				WebElement experienceInformed = wait.until(ExpectedConditions.visibilityOfElementLocated
						(By.xpath("(//div[@class='Select-dropdown']//div[text()='Informerad'])[" + j + "]")));
				experienceInformed.click();
			}
			else if (yearsInTradingShares > 15){
				WebElement experienceDropdown = driver.findElement(By.xpath("(//div[@class='Select-info'])[" + j + "]"));
				executor.executeScript("arguments[0].scrollIntoView(true);", experienceDropdown);
				experienceDropdown.click();
				//Select on experienceAdvanced if years in Trading are > 15
				WebElement experienceAdvanced = wait.until(ExpectedConditions.visibilityOfElementLocated
						(By.xpath("(//div[@class='Select-dropdown']//div[text()='Avancerad'])[" + j + "]")));
				experienceAdvanced.click();
			}
			j++;
		}
		
		//Select all checkbox that the customer has been trained
		List<WebElement> checkboxCustomerHasBeenTrainedElements = driver.findElements(customerHasBeenTrained);
		for (WebElement webElement : checkboxCustomerHasBeenTrainedElements) {
			webElement.click();
		}
	}

	public Risk moveOnToNextPage(){
		WebElement moveOn = wait.until(ExpectedConditions.elementToBeClickable(gavidareButton));
		executor.executeScript("arguments[0].click();", moveOn);
		return new Risk(driver);
	}	

}

