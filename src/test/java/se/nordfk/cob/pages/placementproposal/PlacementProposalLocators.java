package se.nordfk.cob.pages.placementproposal;

public interface PlacementProposalLocators {
	String buying = "//div[text()='K\u00F6p']";
	String selling =  "//div[text()='S\u00E4lj']";      
	String monthlySaving = "//div[text()='M\u00E5nadssparande']";
	String newMonthlySaving = "//div[text()='L\u00C4gg till nytt m\u00C5nadssparande']";
	String placementProposal = "//h2[text()='Placeringsf\u00F6rslag']";
	String riskClassFilter = "//div[@role='slider'][2]";
	String highestRiskClass = "//span[@class='Step-mark-class'][7]";
	String labelPlacering = "//h4[text()='2. Placering']";
	String productType  = "(//div[@class='Select-info'])[2]";
	String currentInstruments = "//div[contains(text(),'Aktuella instrument f\u00F6r teckning')]"; 
	String productName= "(//div[@class='Select-info'])[3]";
	String labelInvestering = "//h4[text()='3. Investering']";
	String antal = "//div/label[.='Antal']/following-sibling::input";
	String percentHolding = "//div[normalize-space()='Procent av innehav']";
	String numberOfFunds = "//div[text()='Antal fondandelar']";
	String brokerage = "(//div[@role='slider'])[1]";
	String withdrawCash= "//div[contains(text(),'Dra likvider fr\u00E5n vald dep\u00E5')]";
	String bankKonto =  "//input[@id='bGcghFJHhAJHg']";
	String addToList =    "//div[@class='field']/button";
	String amountToInvest = "//div/label[.='Belopp att investera']/following-sibling::input";
	String productNameFunds = "(//div[@class='Select-info'])[3]";
	String moveOnButton = "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']";
	String buyOrderTypeLocator = "//div[text()='K\u00F6p']";
}
