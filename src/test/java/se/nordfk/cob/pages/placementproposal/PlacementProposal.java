package se.nordfk.cob.pages.placementproposal;

import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.nordfk.cob.pages.advisorassessment.AdvisorAssessment;
import se.nordfk.cob.pages.base.BasePage;

//Add placement suggestions/ add securities for both buying and selling
public class PlacementProposal extends BasePage implements PlacementProposalLocators{

	WebDriverWait wait = new WebDriverWait(driver, 30);
	JavascriptExecutor executor = (JavascriptExecutor)driver;
	Random rand = new Random();
	public Actions action;

	//calls super-driver and the constructor from BasePage class
	public PlacementProposal(WebDriver driver){

		super(driver);
	}

	// For Structured Product
	By buy = By.xpath(buying);
	By sell = By.xpath(selling);
	By monthlySavings = By.xpath(monthlySaving);
	By newMonthlySavings = By.xpath(newMonthlySaving);
	By titlePlacementProposal = By.xpath(placementProposal);
	By riskClassFilters = By.xpath(riskClassFilter);
	By highestRiskClasses = By.xpath(highestRiskClass);
	By labelTitlePlacering = By.xpath(labelPlacering);
	By productTypeDropdown = By.xpath(productType);
	By currentInstrument = By.xpath(currentInstruments);
	By labelTitleInvestering = By.xpath(labelInvestering);
	By productNameDropdown = By.xpath(productName);
	By quantity = By.xpath(antal);  
	By belopp = By.xpath(amountToInvest);  
	By percentHoldings = By.xpath(percentHolding);
	By brokerageSlider = By.xpath(brokerage);
	By withdrawCashFromDepot = By.xpath(withdrawCash);
	By addList = By.xpath(addToList);


	//For Fund products
	By productNameDropdownFund = By.xpath(productNameFunds);
	By amountsToInvest =By.xpath(amountToInvest);

	By gavidareButton = By.xpath(moveOnButton);

	By buyOrderType = By.xpath(buyOrderTypeLocator);

	//Method for Placement proposal

	public void placementProposal() throws InterruptedException{
		//wait until page loads
		wait.until(ExpectedConditions.visibilityOfElementLocated(titlePlacementProposal));

		//Select radio button for K�p
		WebElement buyRadioButton= driver.findElement(buy);  
		buyRadioButton.click();

		//Set filter for risk class
		WebElement riskSlider = driver.findElement(riskClassFilters);
		executor.executeScript("arguments[0].scrollIntoView(true)", riskSlider);

		WebElement targetDrop = driver.findElement(highestRiskClasses);

		action = new Actions(driver);
		//Move risk slider and select range 1 to 7
		action.dragAndDrop(riskSlider, targetDrop).build().perform();

		//Click product type drop-down
		driver.findElement(productTypeDropdown).click();
	}


	public void addStructuredProduct(String productNameSP, String quantityInvest) throws InterruptedException{
		//Select 'Structure Product' from drop-down
		String productTypeName = "Strukturerade produkter";
		driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+productTypeName+"']")).click();

		//Un-check check-box for selecting secondary market
		driver.findElement(currentInstrument).click();

		//Click on product name drop-down
		driver.findElement(productNameDropdown).click();

		//Select any product from drop-down
		driver.findElement(By.xpath("//div[@title='"+productNameSP+"']")).click();

		//Wait until investering section loads
		WebElement inveteringTitle =  wait.until(ExpectedConditions.visibilityOfElementLocated(labelTitleInvestering));
		executor.executeScript("arguments[0].scrollIntoView(true)", inveteringTitle);

		if (inveteringTitle.isDisplayed() == true) {
			//Provide Quantity
			WebElement antal = wait.until(ExpectedConditions.visibilityOfElementLocated(quantity));
			action.sendKeys(antal, quantityInvest);

			WebElement courtageSP = driver.findElement(brokerageSlider);
			executor.executeScript("arguments[0].scrollIntoView(true)", courtageSP);

			//Move courtage slider
			action.moveToElement(courtageSP).clickAndHold().moveByOffset(150, 0).release().build().perform();
		}

		//Click on button to add product to the listing
		WebElement addListbuttonElement = wait.until(ExpectedConditions.elementToBeClickable(addList));
		addListbuttonElement.click();
	}


	public void addObligationer(String productNameBond, String quantityInvest){
		//Click product type drop-down
		driver.findElement(productTypeDropdown).click();

		//Select 'Bonds' from drop-down
		String productTypeName = "Obligationer";
		driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+productTypeName+"']")).click();

		//Un_check check-box for selecting secondary market
		driver.findElement(currentInstrument).click();

		//Click on product name drop-down
		driver.findElement(productNameDropdown).click();

		//Select any product from drop-down
		driver.findElement(By.xpath("//div[@title='"+productNameBond+"']")).click();    

		//Wait until investering section loads
		WebElement inveteringTitle =  wait.until(ExpectedConditions.visibilityOfElementLocated(labelTitleInvestering));
		executor.executeScript("arguments[0].scrollIntoView(true)", inveteringTitle);

		if (inveteringTitle.isDisplayed() == true) {
			//Provide Quantity
			WebElement antal = wait.until(ExpectedConditions.visibilityOfElementLocated(quantity));
			action.sendKeys(antal, quantityInvest);

			WebElement courtageBond = driver.findElement(brokerageSlider);
			executor.executeScript("arguments[0].scrollIntoView(true)", courtageBond);

			//Move courtage slider
			action.moveToElement(courtageBond).clickAndHold().moveByOffset(150, 0).release().build().perform();
		}

		//Click on button to add product to the listing
		WebElement addListbuttonElement = wait.until(ExpectedConditions.elementToBeClickable(addList));
		addListbuttonElement.click();
	}


	public void addAktier(String productNameAktier, String quantityInvest){
		//Click product type drop-down
		driver.findElement(productTypeDropdown).click(); 

		//Select 'Shares' from drop-down
		String productTypeName = "Aktier";
		driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+productTypeName+"']")).click();

		//Click on product name drop-down
		WebElement aktierFund =  driver.findElement(productNameDropdownFund);
		executor.executeScript("arguments[0].scrollIntoView(true);", aktierFund);
		aktierFund.click();

		//Select any product from drop-down
		driver.findElement(By.xpath("//div[@title='"+productNameAktier+"']")).click();

		//Wait until investering section loads
		WebElement inveteringTitle =  wait.until(ExpectedConditions.visibilityOfElementLocated(labelTitleInvestering));
		executor.executeScript("arguments[0].scrollIntoView(true)", inveteringTitle);

		if (inveteringTitle.isDisplayed() == true) {
			//Provide Quantity
			WebElement antal = wait.until(ExpectedConditions.visibilityOfElementLocated(quantity));
			action.sendKeys(antal, quantityInvest);

			WebElement courtageAktier = driver.findElement(brokerageSlider);
			executor.executeScript("arguments[0].scrollIntoView(true)", courtageAktier);

			//Move courtage slider
			action.moveToElement(courtageAktier).clickAndHold().moveByOffset(150, 0).release().build().perform();
		}

		//Click on button to add product to the listing
		WebElement addListbuttonElement = wait.until(ExpectedConditions.elementToBeClickable(addList));
		addListbuttonElement.click();
	}

	public void addFonder(String productNameFund, String quantityInvest){
		//Click product type drop-down
		driver.findElement(productTypeDropdown).click();

		//Select 'Funds' from drop-down
		String productTypeName = "Fonder";
		driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+productTypeName+"']")).click();

		//Click on product name drop-down
		driver.findElement(productNameDropdownFund).click();

		//Select any product from drop-down
		driver.findElement(By.xpath("//div[@title='"+productNameFund+"']")).click();

		//Wait until investering section loads
		WebElement inveteringTitle =  wait.until(ExpectedConditions.visibilityOfElementLocated(labelTitleInvestering));
		executor.executeScript("arguments[0].scrollIntoView(true)", inveteringTitle);

		if (inveteringTitle.isDisplayed() == true) {
			//Provide Amount to invest
			WebElement antal = wait.until(ExpectedConditions.visibilityOfElementLocated(belopp));
			action.sendKeys(antal, quantityInvest);

			WebElement courtageFund = driver.findElement(brokerageSlider);
			executor.executeScript("arguments[0].scrollIntoView(true)", courtageFund);

			//Move courtage slider
			action.moveToElement(courtageFund).clickAndHold().moveByOffset(150, 0).release().build().perform();
		}

		//Click on button to add product to the listing
		WebElement addListbuttonElement = wait.until(ExpectedConditions.elementToBeClickable(addList));
		addListbuttonElement.click();
	}

	public void addEtf(String productNameEtf, String quantityInvest){
		//Click product type drop-down
		driver.findElement(productTypeDropdown).click();

		//Select 'ETF' from drop-down
		String productTypeName = "ETF:er";
		driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+productTypeName+"']")).click();

		//Click on product name drop-down
		driver.findElement(productNameDropdownFund).click();

		//Select any product from drop-down
		driver.findElement(By.xpath("//div[@title='"+productNameEtf+"']")).click();

		//Wait until investering section loads
		WebElement inveteringTitle =  wait.until(ExpectedConditions.visibilityOfElementLocated(labelTitleInvestering));
		executor.executeScript("arguments[0].scrollIntoView(true)", inveteringTitle);

		if (inveteringTitle.isDisplayed() == true) {
			//Provide Quantity
			WebElement antal = wait.until(ExpectedConditions.visibilityOfElementLocated(quantity));
			action.sendKeys(antal, quantityInvest);

			WebElement courtageEtf = driver.findElement(brokerageSlider);
			executor.executeScript("arguments[0].scrollIntoView(true)", courtageEtf);

			//Move courtage slider
			action.moveToElement(courtageEtf).clickAndHold().moveByOffset(50, 0).release().build().perform();
		}

		//Click on button to add product to the listing
		WebElement addListbuttonElement = wait.until(ExpectedConditions.elementToBeClickable(addList));
		addListbuttonElement.click();
	}


	public void placementProposalSell() throws InterruptedException{

		//Select radio button for K�p
		WebElement sellRadioButton= driver.findElement(sell);  
		sellRadioButton.click();

		//Set filter for risk class
		WebElement rightSlider = driver.findElement(riskClassFilters);
		executor.executeScript("arguments[0].scrollIntoView(true)", rightSlider);

		WebElement targetDrop = driver.findElement(highestRiskClasses);

		action = new Actions(driver);

		//Move risk slider and select range 1 to 7
		action.dragAndDrop(rightSlider, targetDrop).build().perform();

		wait.until(ExpectedConditions.visibilityOfElementLocated(labelTitlePlacering));

		//Click product type drop-down
		driver.findElement(productTypeDropdown).click();	
	}

	public void addStructuredProductForSell(String productNameSP, String quantityInvest){

		//Select 'Structure Product' from drop-down
		String productTypeName = "Strukturerade produkter";
		driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+productTypeName+"']")).click();

		//Un-check check-box for selecting secondary market
		driver.findElement(currentInstrument).click();

		//Click on product name drop-down
		driver.findElement(productNameDropdown).click();

		//Select any product from drop-down
		driver.findElement(By.xpath("//div[@title='"+productNameSP+"']")).click();

		//Wait until investering section loads
		WebElement inveteringTitle =  wait.until(ExpectedConditions.visibilityOfElementLocated(labelTitleInvestering));
		executor.executeScript("arguments[0].scrollIntoView(true)", inveteringTitle);

		if (inveteringTitle.isDisplayed() == true) {
			//Provide Quantity
			WebElement antal = wait.until(ExpectedConditions.visibilityOfElementLocated(quantity));
			action.sendKeys(antal, quantityInvest);

			WebElement courtage = driver.findElement(brokerageSlider);
			executor.executeScript("arguments[0].scrollIntoView(true)", courtage);

			//Move courtage slider
			action.moveToElement(courtage).clickAndHold().moveByOffset(150, 0).release().build().perform();
		}

		//Click on button to add product to the listing
		driver.findElement(addList).click();
	}

	public void addObligationerForSell(String productNameBond, String quantityInvest){
		//Click product type drop-down
		driver.findElement(productTypeDropdown).click();

		//Select 'Bonds' from drop-down
		String productTypeName = "Obligationer";
		driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+productTypeName+"']")).click();

		//Un_check check-box for selecting secondary market
		driver.findElement(currentInstrument).click();

		//Click on product name drop-down
		driver.findElement(productNameDropdown).click();

		//Select any product from drop-down
		driver.findElement(By.xpath("//div[@title='"+productNameBond+"']")).click();    

		//Wait until investering section loads
		WebElement inveteringTitle =  wait.until(ExpectedConditions.visibilityOfElementLocated(labelTitleInvestering));
		executor.executeScript("arguments[0].scrollIntoView(true)", inveteringTitle);

		if (inveteringTitle.isDisplayed() == true) {
			//Provide Quantity
			WebElement antal = wait.until(ExpectedConditions.visibilityOfElementLocated(quantity));
			action.sendKeys(antal, quantityInvest);

			WebElement courtageBond = driver.findElement(brokerageSlider);
			executor.executeScript("arguments[0].scrollIntoView(true)", courtageBond);

			//Move courtage slider
			action.moveToElement(courtageBond).clickAndHold().moveByOffset(150, 0).release().build().perform();
		}

		//Click on button to add product to the listing
		driver.findElement(addList).click();
	}

	public void addAktierForSell(String productNameAktier, String quantityInvest){
		//Click product type drop-down
		driver.findElement(productTypeDropdown).click(); 

		//Select 'Shares' from drop-down
		String productTypeName ="Aktier";
		driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+productTypeName+"']")).click();

		//Click on product name drop-down
		driver.findElement(productNameDropdownFund).click();

		//Select any product from drop-down
		driver.findElement(By.xpath("//div[@title='"+productNameAktier+"']")).click();

		//Wait until investering section loads
		WebElement inveteringTitle =  wait.until(ExpectedConditions.visibilityOfElementLocated(labelTitleInvestering));
		executor.executeScript("arguments[0].scrollIntoView(true)", inveteringTitle);

		if (inveteringTitle.isDisplayed() == true) {
			//Provide Quantity
			WebElement antal = wait.until(ExpectedConditions.visibilityOfElementLocated(quantity));
			action.sendKeys(antal, quantityInvest);

			WebElement courtageAktier = driver.findElement(brokerageSlider);
			executor.executeScript("arguments[0].scrollIntoView(true)", courtageAktier);

			//Move courtage slider
			action.moveToElement(courtageAktier).clickAndHold().moveByOffset(150, 0).release().build().perform();
		}

		//Click on button to add product to the listing
		driver.findElement(addList).click();
	}

	public void addFonderForSell(String productNameFund){

		//Click product type drop-down
		driver.findElement(productTypeDropdown).click();

		//Select 'Funds' from drop-down
		String productTypeName ="Fonder";
		driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+productTypeName+"']")).click();

		//Click on product name drop-down
		driver.findElement(productNameDropdownFund).click();

		//Select any product from drop-down
		driver.findElement(By.xpath("//div[@title='"+productNameFund+"']")).click();

		//Wait until investering section loads
		WebElement inveteringTitle =  wait.until(ExpectedConditions.visibilityOfElementLocated(labelTitleInvestering));
		executor.executeScript("arguments[0].scrollIntoView(true)", inveteringTitle);

		if (inveteringTitle.isDisplayed() == true) {
			//Provide Percentage of funds to sell
			wait.until(ExpectedConditions.visibilityOfElementLocated(percentHoldings));

			WebElement courtageFund = driver.findElement(brokerageSlider);
			executor.executeScript("arguments[0].scrollIntoView(true)", courtageFund);

			//Move courtage slider
			action.moveToElement(courtageFund).clickAndHold().moveByOffset(150, 0).release().build().perform();
		}

		//Click on button to add product to the listing
		driver.findElement(addList).click();
	}

	public void addEtfForSell(String productNameEtf, String quantityInvest){
		//Click product type drop-down
		driver.findElement(productTypeDropdown).click();

		//Select 'ETF' from drop-down
		String productTypeName = "ETF:er";
		driver.findElement(By.xpath("//div[@class='Select-dropdown-option' and @title='"+productTypeName+"']")).click();

		//Click on product name drop-down
		driver.findElement(productNameDropdownFund).click();

		//Select any product from drop-down
		driver.findElement(By.xpath("//div[@title='"+productNameEtf+"']")).click();

		//Wait until investering section loads
		WebElement inveteringTitle =  wait.until(ExpectedConditions.visibilityOfElementLocated(labelTitleInvestering));
		executor.executeScript("arguments[0].scrollIntoView(true)", inveteringTitle);

		if (inveteringTitle.isDisplayed() == true) {
			//Provide Quantity
			WebElement antal = wait.until(ExpectedConditions.visibilityOfElementLocated(quantity));
			action.sendKeys(antal, quantityInvest);

			WebElement courtageEtf = driver.findElement(brokerageSlider);
			executor.executeScript("arguments[0].scrollIntoView(true)", courtageEtf);

			//Move courtage slider
			action.moveToElement(courtageEtf).clickAndHold().moveByOffset(50, 0).release().build().perform();
		}

		//Click on button to add product to the listing
		driver.findElement(addList).click();
	}


	public void monthlySavings(String fundName, String quantityInvest) throws InterruptedException{

		//Select radio button for Monthly Savings
		WebElement monthlySavingRadioButton = driver.findElement(monthlySavings);  
		monthlySavingRadioButton.click();

		//Click on new monthly saving button
		WebElement newMonthlySavingButton = driver.findElement(newMonthlySavings);  
		newMonthlySavingButton.click();

		//Set filter for risk class
		WebElement rightSlider = driver.findElement(riskClassFilters);
		executor.executeScript("arguments[0].scrollIntoView(true)", rightSlider);

		WebElement targetDrop = driver.findElement(highestRiskClasses);

		action = new Actions(driver);

		//Move risk slider and select range 1 to 7
		action.dragAndDrop(rightSlider, targetDrop).build().perform();

		wait.until(ExpectedConditions.visibilityOfElementLocated(labelTitlePlacering));

		//Click product type drop-down
		driver.findElement(productTypeDropdown).click();

		//Click on product name drop-down
		driver.findElement(productNameDropdownFund).click();

		//Select any product from drop-down
		driver.findElement(By.xpath("//div[@title='"+fundName+"']")).click();

		//Provide Amount to invest
		WebElement antalfunds = wait.until(ExpectedConditions.visibilityOfElementLocated(quantity));
		antalfunds.sendKeys(quantityInvest);

		WebElement courtageFund = driver.findElement(brokerageSlider);
		executor.executeScript("arguments[0].scrollIntoView(true)", courtageFund);

		//Move courtage slider
		action.moveToElement(courtageFund).clickAndHold().moveByOffset(150, 0).release().build().perform();

		//Click on button to add product to the listing
		driver.findElement(addList).click();
	}

	public AdvisorAssessment moveOnToNextPage(){
		//Click on Gavidare button
		WebElement moveOn = wait.until(ExpectedConditions.elementToBeClickable(gavidareButton));
		executor.executeScript("arguments[0].click()", moveOn);
		return new AdvisorAssessment(driver);
	}	
}
