package se.nordfk.cob.pages.risk;

import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.nordfk.cob.pages.base.BasePage;
import se.nordfk.cob.pages.riskprofile.RiskProfile;

/* Description:- When choosing a risk class for your savings,
 * it is important to keep in mind that you are also choosing
 * the average return you can expect. For diversified savings
 * over longer periods of time, the connection normally applies
 * that lower risk leads to lower returns and that higher returns
 * require higher risk. Note that the relationship is weak for
 * individual securities and / or shorter time periods .
 */

public class Risk extends BasePage implements RiskLocators{  
	WebDriverWait wait = new WebDriverWait(driver, 30);
	JavascriptExecutor executor = (JavascriptExecutor)driver;
	Random randomNumber = new Random();

	//calls super-driver and the constructor from BasePage class
	public Risk(WebDriver driver){
		super(driver);
	}

	By titleRisk = By.xpath(risk);
	By gavidareButton = By.xpath(moveOnButton);

	//Method to select Desired Risk CLass

	public void riskProfile(String riskValue){
		//wait until page loads
		wait.until(ExpectedConditions.visibilityOfElementLocated(titleRisk));

		//Select risk
		WebElement risk = driver.findElement(By.xpath("//div[.='"+riskValue+"']"));
		executor.executeScript("arguments[0].scrollIntoView(true);", risk);
		risk.click();
	}

	public RiskProfile moveOnToNextPage()
	{
		//Click on Gavidare button
		WebElement moveOn = wait.until(ExpectedConditions.elementToBeClickable(gavidareButton));
		executor.executeScript("arguments[0].click();", moveOn);
		return new RiskProfile(driver);
	}	
}