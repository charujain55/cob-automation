package se.nordfk.cob.pages.risk;

public interface RiskLocators {
	String risk= "//h3[text()='V\u00E4lj din \u00F6nskade riskklass']";
	String moveOnButton = "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']";
}
