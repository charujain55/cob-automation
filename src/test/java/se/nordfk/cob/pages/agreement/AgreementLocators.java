package se.nordfk.cob.pages.agreement;

public interface AgreementLocators {

	String evaluateDiscretionaryManagement = "//div[text()='Diskretion\u00E4r f\u00F6rvaltning']";
	String resourceToProcureAndMediateInsurance = "//div[text()='Tryggande & Pensionssparande']";
	String analyzeAndSuggestBasicProtection = "//div[text()='Genomg\u00E5ng av grundskydd']";
	String arrangeHealthInsurance = "//div[text()='Sjukv\u00E5rdsf\u00F6rs\u00E4kring']";
	String provideAdviceAndServiceForInvestments = "//div[text()='R\u00E5dgivning']";
	String provideAdviceAndServiceBasedOnEmployees = "//div[text()='R\u00E5dgivning till anst\u00E4llda']";
	String administrationOfSavingsAndInvestments = "//div[text()='Administration']";
	String webPortalForCustomerAndEmployees = "//div[text()='Onlinetj\u00E4nst']";
	String developmentOfInvestmentPolicy = "//div[text()='Placeringspolicy']";
	String investmentAdviceRegardingCompanyLiquidationAndSuccession = "//div[text()='Vilande bolag/Succession']";
	String suggestInvestmentCompanySurplusLiquidity = "//div[text()='\u00D6verskottslikviditet i bolag']";
	String assistAdviceOnNoninsuranceInvestment = "//div[text()='Investeringsr\u00E5dgivning']";
	String affiliatedRepresentativeEntitledToCompensation = "//div[text()='Ers\u00E4ttning r\u00E5dgivning']";
	String affiliatedAgentAndCustomerEnterIntoAgreement = "//div[text()='Annan ers\u00E4ttning']";
	String describeFormOfCompensation = "uppdragsavtal_annaners_text";
	String moveOnButton = "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']";
}
