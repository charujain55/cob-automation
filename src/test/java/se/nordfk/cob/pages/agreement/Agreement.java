package se.nordfk.cob.pages.agreement;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.nordfk.cob.pages.advancepurchase.AdvancePurchaseInfo;
import se.nordfk.cob.pages.base.BasePage;

/*The Client hereby instructs the Advisor to act as 
 *an investment advisor to the Client in investment matters.The 
 *assignment is performed in accordance with good advisory practice
 *in accordance with the requirements of the Swedish Securities Market Act
 */

public class Agreement extends BasePage implements AgreementLocators{

	WebDriverWait wait = new WebDriverWait(driver, 30);    
	JavascriptExecutor executor = (JavascriptExecutor)driver;

	
	//calls super-driver and the constructor from BasePage class
	public Agreement(WebDriver driver){
		super(driver);
	}

	By discretionaryManagement = By.xpath(evaluateDiscretionaryManagement);
	By guaranteePensionSavings = By.xpath(resourceToProcureAndMediateInsurance);
	By reviewBasicProtection = By.xpath(analyzeAndSuggestBasicProtection);
	By healthInsurance = By.xpath(arrangeHealthInsurance);
	By advice = By.xpath(provideAdviceAndServiceForInvestments);
	By adviceToEmployees = By.xpath(provideAdviceAndServiceBasedOnEmployees);
	By administration = By.xpath(administrationOfSavingsAndInvestments);
	By onlineService = By.xpath(webPortalForCustomerAndEmployees);
	By placementPolicy = By.xpath(developmentOfInvestmentPolicy);
	By succession = By.xpath(investmentAdviceRegardingCompanyLiquidationAndSuccession);
	By excessLiquidityInCompanies = By.xpath(suggestInvestmentCompanySurplusLiquidity);
	By investmentAdvice = By.xpath(assistAdviceOnNoninsuranceInvestment);
	By compensationAdvice = By.xpath(affiliatedRepresentativeEntitledToCompensation);
	By otherCompensation = By.xpath(affiliatedAgentAndCustomerEnterIntoAgreement);
	By formOfCompensation = By.name(describeFormOfCompensation);
	By gavidareButton = By.xpath(moveOnButton);

	//Method to click Agreement options

	public void selectCheckBoxes(String s1){
		WebElement Discretionary = wait.until(ExpectedConditions.visibilityOfElementLocated(discretionaryManagement));
		executor.executeScript("arguments[0].scrollIntoView();", Discretionary);

		//Click on check-box 'Diskretion�r f�rvaltning'
		Discretionary.click();

		//Click on check-box 'Tryggande & Pensionssparande'
		driver.findElement(guaranteePensionSavings).click();

		//Click on check-box 'Genomg�ng av grundskydd'
		driver.findElement(reviewBasicProtection).click();

		//Click on check-box 'Sjukv�rdsf�rs�kring'
		driver.findElement(healthInsurance).click();

		//Click on check-box 'R�dgivning'
		driver.findElement(advice).click();

		//Click on check-box 'R�dgivning till anst�llda'
		driver.findElement(adviceToEmployees).click();

		//Click on check-box 'Administration'
		driver.findElement(administration).click();

		//Click on check-box 'Onlinetj�nst'
		driver.findElement(onlineService).click();

		//Click on check-box 'Placeringspolicy'
		driver.findElement(placementPolicy).click();

		//Click on check-box 'Vilande bolag/Succession'
		driver.findElement(succession).click();

		//Click on check-box '�verskottslikviditet i bolag'
		driver.findElement(excessLiquidityInCompanies).click();

		//Click on check-box 'Investeringsr�dgivning'
		driver.findElement(investmentAdvice).click();

		//Scroll window to bottom
		executor.executeScript("window.scrollTo(0,document.body.scrollHeight)");

		//Click on check-box 'Ers�ttning r�dgivning'
		driver.findElement(compensationAdvice).click();

		//Click on check-box 'Annan ers�ttning'
		driver.findElement(otherCompensation).click();

		//Fill data in text-box 'Var god beskriv ers�ttningsformen'
		driver.findElement(formOfCompensation).sendKeys(s1);
	}
	
	public AdvancePurchaseInfo moveOnToNextPage()
	{
		//Click on Gavidare button
		WebElement moveOn = wait.until(ExpectedConditions.elementToBeClickable(gavidareButton));
		executor.executeScript("arguments[0].click()", moveOn);
		return new AdvancePurchaseInfo(driver);

	}

}
