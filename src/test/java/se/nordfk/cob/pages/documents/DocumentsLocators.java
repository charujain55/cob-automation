package se.nordfk.cob.pages.documents;

public interface DocumentsLocators{
	String otherDocuments = "//h2[text()='\u00D6vriga dokument']";
	String fileDropbox = "FileDrop-dropbox";
	String moveOnButton = "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']";
	String signingAndSignature = "//h2[text()='Signering och kundens underskrift']";
	String cobSignAgreement =  "//div[@class='Cob-SignAgreement-fields']/div";
	String signAgreement = "//div[text()='Signera avtal']";
	String signingAgreement = "//h2[text()='Signera avtal']";
	String sendForSigningButton = "//button[@class='Button small filled success']";
}
