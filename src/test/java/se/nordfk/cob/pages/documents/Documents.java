package se.nordfk.cob.pages.documents;

import java.io.IOException;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.nordfk.cob.pages.base.BasePage;

//Add necessary supporting documents
public class Documents extends BasePage implements DocumentsLocators{

	WebDriverWait wait = new WebDriverWait(driver, 30);
	JavascriptExecutor executor = (JavascriptExecutor)driver;

	//calls super-driver and the constructor from BasePage class
	public Documents(WebDriver driver){
		super(driver);
	}

	By titleOtherDocuments = By.xpath(otherDocuments);
	By filesDropbox = By.className(fileDropbox);
	By gavidareButton = By.xpath(moveOnButton);
	By titleSigningAndSignature = By.xpath(signingAndSignature);
	By cobSigningAgreement = By.xpath(cobSignAgreement);
	By signsAgreement = By.xpath(signAgreement);
	By titleSignAgreement = By.xpath(signingAgreement);
	By sendsForSigningButton = By.xpath(sendForSigningButton);


	//Method for Uploading and Signing Documents

	public void otherDocuments() throws InterruptedException, IOException{
		wait.until(ExpectedConditions.visibilityOfElementLocated(titleOtherDocuments));

		//Click on drag & drop area
		driver.findElement(filesDropbox).click();
		Thread.sleep(1000);

		//Upload PDF file
		Runtime.getRuntime().exec("./src/main/resources/fileUpload.exe");

		Thread.sleep(1000);
		WebElement moveOn = wait.until(ExpectedConditions.elementToBeClickable(gavidareButton));
		//Click on move on button
		executor.executeScript("arguments[0].click()", moveOn);

		wait.until(ExpectedConditions.visibilityOfElementLocated(titleSigningAndSignature));
		List<WebElement> signCheckboxes = driver.findElements(cobSigningAgreement);

		for (WebElement checkbox : signCheckboxes){
			String className = checkbox.getAttribute("class");

			//if agreement check-boxes are not checked then mark them checked
			if (!className.contains("checked"))
				checkbox.click();
			else
				continue;
		}

		WebElement signeraAvtal = driver.findElement(signsAgreement);

		//Click on sign agreement button
		executor.executeScript("arguments[0].click();", signeraAvtal);
		wait.until(ExpectedConditions.visibilityOfElementLocated(titleSignAgreement));

		//Click in 'Skicka till signering' button
		driver.findElement(sendsForSigningButton).click();
	}
}
