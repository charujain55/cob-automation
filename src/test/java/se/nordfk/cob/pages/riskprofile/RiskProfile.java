package se.nordfk.cob.pages.riskprofile;

import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.nordfk.cob.pages.base.BasePage;
import se.nordfk.cob.pages.council.Council;

//Selection of desired investment time span and the risk level.
public class RiskProfile extends BasePage implements RiskProfileLocators{     

	WebDriverWait wait = new WebDriverWait(driver, 30);
	JavascriptExecutor executor = (JavascriptExecutor)driver;
	Random randomNumber = new Random();
	public Actions action;


	//calls super-driver and the constructor from BasePage class
	public RiskProfile(WebDriver driver){
		super(driver);
	}

	By titleRiskSavingProfile = By.xpath(savingProfile);
	By checkboxHorizon1 = By.xpath(horizon1Checkbox1);
	By checkboxHorizon2 = By.xpath(horizon2Checkbox2);
	By checkboxHorizon3 = By.xpath(horizon3Checkbox3);
	By checkboxHorizon4 = By.xpath(horizon4Checkbox4);
	By goalOfSavingdropdown1 = By.xpath(goalOfSavingsdropdown1);
	By goalOfSavingdropdown2 = By.xpath(goalOfSavingsdropdown2);
	By goalOfSavingdropdown3 = By.xpath(goalOfSavingsdropdown3);
	By goalOfSavingdropdown4 = By.xpath(goalOfSavingsdropdown4);
	By goalOfSavingOptionOther1 = By.xpath(goalOfSavingsOptionOther1);
	By goalOfSavingOptionOther2 = By.xpath(goalOfSavingsOptionOther2);
	By goalOfSavingOptionOther3 = By.xpath(goalOfSavingsOptionOther3);
	By goalOfSavingOptionOther4 = By.xpath(goalOfSavingsOptionOther4);
	By titleTotalInvestableAssets = By.xpath(totalInvestableAssets);
	By titleTotalInvestedCapital = By.xpath(totalInvestedCapital);
	By riskSliderDistribution = By.xpath(riskSlider);
	By gavidareButton = By.xpath(moveOnButton);

	//Method to select goal of saving
	public void goalofSaving(String horizon1GoalOfSaving, String otherGoalOfSaving1, String horizon2GoalOfSaving, 
			String otherGoalOfSaving2, String horizon3GoalOfSaving, String otherGoalOfSaving3, String horizon4GoalOfSaving, 
			String otherGoalOfSaving4) throws InterruptedException{
		//wait until page loads
		wait.until(ExpectedConditions.visibilityOfElementLocated(titleRiskSavingProfile));

		//if horizon1 checkbox is selected
		WebElement horizon1Checkbox = driver.findElement(checkboxHorizon1);
		if (horizon1Checkbox.isSelected()==true) {
			//then click on goal of saving dropdown
			WebElement horizon1dropdown =  driver.findElement(goalOfSavingdropdown1);
			executor.executeScript("arguments[0].scrollIntoView();", horizon1dropdown);
			horizon1dropdown.click();
			//select option from goal of saving dropdown
			driver.findElement(By.xpath("(//div[@class='Select-dropdown-option' and .='"+horizon1GoalOfSaving+"'])[1]")).click();

			//if goal of saving is 'Other'
			if (horizon1GoalOfSaving.equalsIgnoreCase("Annat")) {
				//Fill data in text-box
				WebElement other = wait.until(ExpectedConditions.visibilityOfElementLocated
						(goalOfSavingOptionOther1));
				other.sendKeys(otherGoalOfSaving1);
			}
		}

		//if horizon2 checkbox is selected
		WebElement horizon2Checkbox = driver.findElement(checkboxHorizon2);
		if (horizon2Checkbox.isSelected()==true) {
			//then click on goal of saving dropdown
			WebElement horizon2Dropdown =  driver.findElement(goalOfSavingdropdown2);
			executor.executeScript("arguments[0].scrollIntoView();", horizon2Dropdown);
			horizon2Dropdown.click();
			//select option from goal of saving dropdown
			driver.findElement(By.xpath("(//div[@class='Select-dropdown-option' and .='"+horizon2GoalOfSaving+"'])[2]")).click();

			//if goal of saving is 'Other'
			if (horizon2GoalOfSaving.equalsIgnoreCase("Annat")) {
				//Fill data in text-box
				WebElement other = wait.until(ExpectedConditions.visibilityOfElementLocated
						(goalOfSavingOptionOther2));
				other.sendKeys(otherGoalOfSaving2);					
			}
		}

		//if horizon3 checkbox is selected
		WebElement horizon3Checkbox = driver.findElement(checkboxHorizon3);
		if (horizon3Checkbox.isSelected()==true) {
			//then click on goal of saving dropdown
			WebElement horizon3Dropdown =  driver.findElement(goalOfSavingdropdown3);
			executor.executeScript("arguments[0].scrollIntoView();", horizon3Dropdown);
			horizon3Dropdown.click();
			//select option from goal of saving dropdown
			driver.findElement(By.xpath("(//div[@class='Select-dropdown-option' and .='"+horizon3GoalOfSaving+"'])[3]")).click();

			//if goal of saving is 'Other'
			if (horizon3GoalOfSaving.equalsIgnoreCase("Annat")) {
				//Fill data in text-box
				WebElement other = wait.until(ExpectedConditions.visibilityOfElementLocated
						(goalOfSavingOptionOther3));
				other.sendKeys(otherGoalOfSaving3);					
			}
		}

		//if horizon4 checkbox is selected
		WebElement horizon4Checkbox = driver.findElement(checkboxHorizon4);
		if (horizon4Checkbox.isSelected()==true) {
			//then click on goal of saving dropdown
			WebElement horizon4Dropdown =  driver.findElement(goalOfSavingdropdown4);
			executor.executeScript("arguments[0].scrollIntoView();", horizon4Dropdown);
			horizon4Dropdown.click();
			//select option from goal of saving dropdown
			driver.findElement(By.xpath("(//div[@class='Select-dropdown-option' and .='"+horizon4GoalOfSaving+"'])[4]")).click();

			//if goal of saving is 'Other'
			if (horizon4GoalOfSaving.equalsIgnoreCase("Annat")) {
				//Fill data in text-box
				WebElement other = wait.until(ExpectedConditions.visibilityOfElementLocated
						(goalOfSavingOptionOther4));
				other.sendKeys(otherGoalOfSaving4);					
			}
		}
	}

	//Method to select risk level
	public void horizonRiskLevel(String horizon1RiskValue, String horizon2RiskValue, String horizon3RiskValue, 
			String horizon4RiskValue) throws InterruptedException{

		//if horizon1 checkbox is selected
		WebElement horizon1Checkbox = driver.findElement(checkboxHorizon1);
		if (horizon1Checkbox.isSelected()==true) {
			//Select risk at horizon1
			driver.findElement(By.xpath("(//div[@class='Checkbox-label' and text()='"+horizon1RiskValue+"'])[1]")).click();
		}

		//if horizon2 checkbox is selected
		WebElement horizon2Checkbox = driver.findElement(checkboxHorizon2);
		if (horizon2Checkbox.isSelected()==true) {
			//Select risk at horizon2
			driver.findElement(By.xpath("(//div[@class='Checkbox-label' and text()='"+horizon2RiskValue+"'])[2]")).click();
		}


		//if horizon3 checkbox is selected
		WebElement horizon3Checkbox = driver.findElement(checkboxHorizon3);
		if (horizon3Checkbox.isSelected()==true) {
			//Select risk at horizon2
			driver.findElement(By.xpath("(//div[@class='Checkbox-label' and text()='"+horizon3RiskValue+"'])[3]")).click();
		}


		//if horizon4 checkbox is selected
		WebElement horizon4Checkbox = driver.findElement(checkboxHorizon4);
		if (horizon4Checkbox.isSelected()==true) {
			//Select risk at horizon2
			driver.findElement(By.xpath("(//div[@class='Checkbox-label' and text()='"+horizon4RiskValue+"'])[4]")).click();
		}


		/*WebElement riskProfil = driver.findElement(By.xpath("(//font/span)[1]"));
		//WebElement totalRisk = driver.findElement(By.xpath("(//font/span)[2]"));
		int riskProfil1 = Integer.parseInt(riskProfil.getText());
		//integer totalRisk1 = Integer.parseInt(totalRisk.getText());

		//Select risk for each horizon
		for (int i =1; i <= 7; i+=2){
			List<WebElement> allRisk = driver.findElements
					(By.xpath("(//div[@class='Cob-OptionsList'])["+i+"]//div[@class='Checkbox-checkbox']"));
			int size = allRisk.size();

			//If risk selection is greater than actual risk
			if (size > riskProfil1)

				//Then select risk with in range of actual risk only
				size = riskProfil1;
			int randomRisk1 =randomNumber.nextInt(size);
			allRisk.get(randomRisk1).click();
		}

		//executor.executeScript("window.scrollTo(document.body.scrollHeight, 0)");
		String summaPlaceringsbaraTillgångar = driver.findElement(titleTotalInvestableAssets).getText();
		String totaltPlaceratKapital = driver.findElement(titleTotalInvestedCapital).getText();

		//Extracting values from Total Investable assets & Total invested capital 
		String[] summa_placeringsbara_tillgångar1 = summaPlaceringsbaraTillgångar.split(":");
		summaPlaceringsbaraTillgångar = summa_placeringsbara_tillgångar1[1].trim();
		String[] totalt_placerat_kapital1 = totaltPlaceratKapital.split(":");

		totaltPlaceratKapital = totalt_placerat_kapital1[1].trim(); */
	}

	//Method to select early saving
	public void earlySaving(String doYouNeedToSaveEarlyHorizon1, String doYouNeedToSaveEarlyHorizon2, 
			String doYouNeedToSaveEarlyHorizon3, String doYouNeedToSaveEarlyHorizon4, 
			String noOfMonthsHorizon1, String noOfMonthsHorizon2, String noOfMonthsHorizon3, 
			String noOfMonthsHorizon4) throws InterruptedException{

		//if horizon1 checkbox is selected
		WebElement horizon1Checkbox = driver.findElement(checkboxHorizon1);
		if (horizon1Checkbox.isSelected()==true) {
			//Do you need to save early - YES/NO
			driver.findElement(By.xpath("(//div[@class='Checkbox-label' and text()='"+doYouNeedToSaveEarlyHorizon1+"'])[1]")).click();
			//If need to save early - YES 
			if (doYouNeedToSaveEarlyHorizon1.equalsIgnoreCase("Ja")) {
				//then fill no of months
				driver.findElement(By.xpath("(//label[text()='Ange antal m\u00E5nader'])[1]/following-sibling::input")).
				sendKeys(noOfMonthsHorizon1);
			}
		}

		//if horizon2 checkbox is selected
		WebElement horizon2Checkbox = driver.findElement(checkboxHorizon2);
		if (horizon2Checkbox.isSelected()==true) {
			//Do you need to save early - YES/NO
			driver.findElement(By.xpath("(//div[@class='Checkbox-label' and text()='"+doYouNeedToSaveEarlyHorizon2+"'])[2]")).click();
			//If need to save early - YES 
			if (doYouNeedToSaveEarlyHorizon2.equalsIgnoreCase("Ja")) {
				//then fill no of months
				driver.findElement(By.xpath("(//label[text()='Ange antal m\u00E5nader'])[2]/following-sibling::input")).
				sendKeys(noOfMonthsHorizon2);
			}
		}


		//if horizon3 checkbox is selected
		WebElement horizon3Checkbox = driver.findElement(checkboxHorizon3);
		if (horizon3Checkbox.isSelected()==true) {
			//Do you need to save early - YES/NO
			driver.findElement(By.xpath("(//div[@class='Checkbox-label' and text()='"+doYouNeedToSaveEarlyHorizon3+"'])[3]")).click();
			//If need to save early - YES 
			if (doYouNeedToSaveEarlyHorizon3.equalsIgnoreCase("Ja")) {
				//then fill no of months
				driver.findElement(By.xpath("(//label[text()='Ange antal m\u00E5nader'])[3]/following-sibling::input")).
				sendKeys(noOfMonthsHorizon3);
			}
		}


		//if horizon4 checkbox is selected
		WebElement horizon4Checkbox = driver.findElement(checkboxHorizon4);
		if (horizon4Checkbox.isSelected()==true) {
			//Do you need to save early - YES/NO
			driver.findElement(By.xpath("(//div[@class='Checkbox-label' and text()='"+doYouNeedToSaveEarlyHorizon4+"'])[4]")).click();
			//If need to save early - YES 
			if (doYouNeedToSaveEarlyHorizon4.equalsIgnoreCase("Ja")) {
				//then fill no of months
				driver.findElement(By.xpath("(//label[text()='Ange antal m\u00E5nader'])[4]/following-sibling::input")).
				sendKeys(noOfMonthsHorizon4);
			}
		}		
	}


	public Council moveOnToNextPage(){
		//Click on Gavidare button
		WebElement moveOn = wait.until(ExpectedConditions.elementToBeClickable(gavidareButton));
		executor.executeScript("arguments[0].click();", moveOn);
		return new Council(driver);
	}

}
