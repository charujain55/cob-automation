package se.nordfk.cob.pages.riskprofile;

public interface RiskProfileLocators {
	String savingProfile ="//h2[text()='Sparprofil']";
	String horizon1Checkbox1 = "(//input[@type='checkbox'])[1]";
	String horizon2Checkbox2 = "(//input[@type='checkbox'])[2]";
	String horizon3Checkbox3 = "(//input[@type='checkbox'])[3]";
	String horizon4Checkbox4 = "(//input[@type='checkbox'])[4]";
	String goalOfSavingsdropdown1 = "(//div[@class='Select-info'])[1]";
	String goalOfSavingsdropdown2 = "(//div[@class='Select-info'])[2]";
	String goalOfSavingsdropdown3 = "(//div[@class='Select-info'])[3]";
	String goalOfSavingsdropdown4 = "(//div[@class='Select-info'])[4]";
	String goalOfSavingsOptionOther1 = "(//label[text()='V\u00E4nligen beskriv'])[1]/following-sibling::input";
	String goalOfSavingsOptionOther2 = "(//label[text()='V\u00E4nligen beskriv'])[2]/following-sibling::input";
	String goalOfSavingsOptionOther3 = "(//label[text()='V\u00E4nligen beskriv'])[3]/following-sibling::input";
	String goalOfSavingsOptionOther4 = "(//label[text()='V\u00E4nligen beskriv'])[4]/following-sibling::input";
	String totalInvestableAssets = "//h4[contains(text(),'Summa placeringsbara tillg\u00E5ngar: ')]";
	String totalInvestedCapital = "//h4[contains(text(), 'Totalt placerat kapital')]";
	String riskSlider = "(//div[@role='slider'])[4]";
	String moveOnButton = "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']";
}

