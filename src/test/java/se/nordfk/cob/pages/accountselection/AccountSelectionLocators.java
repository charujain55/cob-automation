package se.nordfk.cob.pages.accountselection;

public interface AccountSelectionLocators {
	String accountSelection = "//h2[text()='Kontoval']";
	String newDepotAtNord ="//div[text()='Ny dep\u00E5 hos Nord']";
	String depositFee ="(//div[@class='Checkbox-checkbox'])[10]";
	String adviceFee = "(//div[@class='Checkbox-checkbox'])[11]";
	String moveOnButton = "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']";

}