package se.nordfk.cob.pages.accountselection;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.nordfk.cob.pages.base.BasePage;
import se.nordfk.cob.pages.preregisteredaccount.PreRegisteredAccount;

/*The advice presented by the advisor and the account is created by client.
 * There are 3 types of account:- 1)External existing account, 
 * 2)Existing account at Nord, 3)New account at Nord.
 * When selecting a new account,a investment saving account, Custodian 
 * account, or Custodian insurance account can be created. 
 * And can give consent to payment by direct debit.*/

public class AccountSelection extends BasePage implements AccountSelectionLocators{

	WebDriverWait wait = new WebDriverWait(driver, 30);
	JavascriptExecutor executor = (JavascriptExecutor)driver;

	//calls super-driver and the constructor from BasePage class
	public AccountSelection(WebDriver driver){
		super(driver);
	}

	By titleAccountSelection = By.xpath(accountSelection);
	By newDepotNord = By.xpath(newDepotAtNord);
	By depositFees = By.xpath(depositFee);
	By adviceFees =  By.xpath(adviceFee);
	By gavidareButton = By.xpath(moveOnButton);


	//Method for new Account creation

	public void accountSelect(String typeOfAccount, String typeOfDepot, String powerOfAttorny, 
			String DoYouUnderstandRiskInProductClassA, String DoYouUnderstandRiskInProductClassB){
		//wait until page loads
		wait.until(ExpectedConditions.visibilityOfElementLocated(titleAccountSelection));

		//Select new depot at nordFK
		driver.findElement(newDepotNord).click();

		//Select account selection
		driver.findElement(By.xpath("//div[text()='"+typeOfAccount+"']")).click();

		//Select depot type
		driver.findElement(By.xpath("//div[text()='"+typeOfDepot+"']")).click();

		//Select deposit fee 
		driver.findElement(depositFees).click();

		//Select advice fee 
		driver.findElement(adviceFees).click();

		//Select proxy type
		driver.findElement(By.xpath("//div[text()='"+powerOfAttorny+"']")).click();

		//Select  yes for risk in the products in Product Class A
		driver.findElement(By.xpath("(//div[@class='Cob-OptionsList-checkboxes'])[4]//div[.='"+DoYouUnderstandRiskInProductClassA+"']")).click();

		//Select yes for risk in the products in Product Class B
		driver.findElement(By.xpath("(//div[@class='Cob-OptionsList-checkboxes'])[5]//div[.='"+DoYouUnderstandRiskInProductClassB+"']")).click();
	}
	
	public PreRegisteredAccount moveOnToNextPage(){
		//Click on Gavidare button
		WebElement moveOn = wait.until(ExpectedConditions.elementToBeClickable(gavidareButton));
		executor.executeScript("arguments[0].click();", moveOn);
		return new PreRegisteredAccount(driver);
	}	


}
