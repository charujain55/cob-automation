package se.nordfk.cob.pages.advisorassessment;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.nordfk.cob.pages.base.BasePage;
import se.nordfk.cob.pages.proposal.Proposal;

//Advisor's assessment
public class AdvisorAssessment extends BasePage implements AdvisorAssessmentLocators{ 

	WebDriverWait wait = new WebDriverWait(driver, 30);
	JavascriptExecutor executor = (JavascriptExecutor)driver;


	//calls super-driver and the constructor from BasePage class
	public AdvisorAssessment(WebDriver driver){
		super(driver);
	}

	By advisorsAssessment = By.xpath(advisorAssessment);
	By motivationForSelectedProducts = By.xpath(motivationForSelectedProduct);
	By discourage =By.xpath(discouragement);
	By gavidareButton = By.xpath(moveOnButton);

	//Method for Advisor's Assessment
	public void assessment(String isCustomerAbleToBearRisk, String isCustomerJudgedForRiskKnowledge, 
			String isCustomerAdvisedAgainstPlacement, String feedbackAboutCustomerInvestment, String feedbackDiscouragement){
		wait.until(ExpectedConditions.visibilityOfElementLocated(advisorsAssessment));

		//Select yes/no if customer considered to be able to bear the risk 
		driver.findElement(By.xpath("(//div[@class='Checkbox-label' and .='"+isCustomerAbleToBearRisk+"'])[1]")).click();

		//Select yes/no if customer is judged to have sufficient knowledge of the risks
		driver.findElement(By.xpath("(//div[@class='Checkbox-label' and .='"+isCustomerJudgedForRiskKnowledge+"'])[2]")).click();

		//Select Yes if customer been advised against a certain placement
		driver.findElement(By.xpath("(//div[@class='Checkbox-label' and .='"+isCustomerAdvisedAgainstPlacement+"'])[3]")).click();

		if (isCustomerAdvisedAgainstPlacement.equalsIgnoreCase("Ja")) {
			//Fill data in text-box for motivation for selected product / location
			driver.findElement(motivationForSelectedProducts).sendKeys(feedbackAboutCustomerInvestment);	
		}

		//Fill data in text-box for discouragement
		driver.findElement(discourage).sendKeys(feedbackDiscouragement);
	}

	public Proposal moveOnToNextPage(){
		//Click on Gavidare button
		WebElement moveOn = wait.until(ExpectedConditions.elementToBeClickable(gavidareButton));
		executor.executeScript("arguments[0].click()", moveOn);
		return new Proposal(driver);
	}
}