package se.nordfk.cob.pages.advisorassessment;

public interface AdvisorAssessmentLocators {
	String advisorAssessment = "//h2[text()='R\u00E5dgivarens bed\u00F6mning']";
	String motivationForSelectedProduct = "(//textarea)[1]";
	String discouragement = "(//textarea)[2]";
	String moveOnButton = "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']";
}