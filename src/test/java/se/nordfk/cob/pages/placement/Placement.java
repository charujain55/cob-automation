package se.nordfk.cob.pages.placement;



import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.nordfk.cob.pages.base.BasePage;
import se.nordfk.cob.pages.documents.Documents;

//Analyze placement proposal
public class Placement extends BasePage implements PlacementLocators{
	WebDriverWait wait = new WebDriverWait(driver, 30);
	JavascriptExecutor executor = (JavascriptExecutor)driver;

	//calls super-driver and the constructor from BasePage class
	public Placement(WebDriver driver){
		super(driver);
	}

	By titleFinancialInstrument = By.xpath(financialInstruments);
	By gavidareButton =By.xpath(moveOnButton);

	public void proposal(){
		wait.until(ExpectedConditions.visibilityOfElementLocated(titleFinancialInstrument));
	}

	public Documents moveOnToNextPage(){
		//Click on move on button
		WebElement moveOn = wait.until(ExpectedConditions.elementToBeClickable(gavidareButton));
		executor.executeScript("arguments[0].click()", moveOn);
		return new Documents(driver);
	}

}
