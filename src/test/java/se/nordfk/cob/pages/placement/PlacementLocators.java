package se.nordfk.cob.pages.placement;

public interface PlacementLocators{
	String financialInstruments = "//div[text()='Finansiella instrument']";
	String moveOnButton = "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']";
}
