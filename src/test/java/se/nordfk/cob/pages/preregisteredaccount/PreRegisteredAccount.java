package se.nordfk.cob.pages.preregisteredaccount;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import se.nordfk.cob.pages.base.BasePage;
import se.nordfk.cob.pages.placementproposal.PlacementProposal;

public class PreRegisteredAccount extends BasePage implements PreRegisteredAccountLocators{

	WebDriverWait wait = new WebDriverWait(driver, 30);
	JavascriptExecutor executor = (JavascriptExecutor)driver;
	Actions action = new Actions(driver);

	//calls super-driver and the constructor from BasePage class
	public PreRegisteredAccount(WebDriver driver){
		super(driver);
	}
	By titlePreRegisteredAccount = By.xpath(accountSelection);
	By preRegisterAccount = By.className(preRegisteredAccount);
	By banks = By.xpath(bank);
	By clearingNumbers = By.xpath(clearingNumber);
	By accountNumbers = By.xpath(accountNumber);
	By gavidareButton = By.xpath(moveOnButton);


	//Method for Account Selection
	public void accountSelection(String doYouWantToAddNewBankAccount, String bankName, String clearingNumber, 
			String accountNumber){
		//wait for bank account page header
		wait.until(ExpectedConditions.visibilityOfElementLocated(titlePreRegisteredAccount));
		
		//wait until all external bank account lists
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("account")));
		executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");

		//Select Yes/No for bank account creation
		driver.findElement(By.xpath("(//div[@class='Checkbox-label' and text()='"+doYouWantToAddNewBankAccount+"'])")).click();
		//If yes selected for bank account
		if (doYouWantToAddNewBankAccount.equalsIgnoreCase("Ja")) {
			
			//Click on bank name dropdown
			driver.findElement(preRegisterAccount).click();
		
			//Select Bank from drop-down menu
			driver.findElement(By.xpath("//div[@title='"+bankName+"']")).click();

			//Enter clearing number
			driver.findElement(clearingNumbers).sendKeys(clearingNumber);

			//Enter associated bank account number
			driver.findElement(accountNumbers).sendKeys(accountNumber);		
		}
		
	}

	public PlacementProposal moveOnToNextPage(){
		//Click on Gavidare button
		WebElement moveOn = wait.until(ExpectedConditions.elementToBeClickable(gavidareButton));
		executor.executeScript("arguments[0].click();", moveOn);
		return new PlacementProposal(driver);
	}
}





