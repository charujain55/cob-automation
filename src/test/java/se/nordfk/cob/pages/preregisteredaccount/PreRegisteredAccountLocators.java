package se.nordfk.cob.pages.preregisteredaccount;

public interface PreRegisteredAccountLocators {
	String accountSelection = "//h2[text()='\u00D6versikt: f\u00F6ranm\u00E4lda bankkonton']";
	String preRegisteredAccount = "Select-info";
	String bank = "//input[@placeholder='S\u00F6k']";
	String clearingNumber = "//label[text()='Clearingnummer']/following-sibling::input";
	String accountNumber = "//label[text()='Kontonummer']/following-sibling::input";
	String moveOnButton = "//div[@class='Button-container']//following-sibling::div[.='G\u00E5 vidare']";
}
