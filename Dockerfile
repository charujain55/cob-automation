FROM markhobson/maven-chrome:jdk-16

#Change the Work Directory
WORKDIR /root

#Copy the Content inside the Image
COPY ./src ./src
COPY ./pom.xml ./pom.xml
COPY ./testng.xml ./testng.xml

CMD mvn clean test

